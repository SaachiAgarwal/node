#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -e

starttime=$(date +%s)

if [ ! -d ~/.hfc-key-store/ ]; then
	mkdir ~/.hfc-key-store/
fi
rm -rf ~/.hfc-key-store/*
cp $PWD/creds/* ~/.hfc-key-store/
# launch network; create channel and join peer to channel
cd ../basic-network
#./start.sh

# Now launch the CLI container in order to install, instantiate chaincode
# and prime the ledger with our 10 cars
#docker-compose -f ./docker-compose.yml up -d cli

FOLDER="gsa""$1"
CONTRACT="gsaOffer""$1"


docker exec -e "CORE_PEER_LOCALMSPID=hqMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hq.gsa.com/users/Admin@hq.gsa.com/msp" cli peer chaincode install -n $CONTRACT -v 1.0 -p github.com/hyperledger/fabric/examples/chaincode/go/$FOLDER
docker exec -e "CORE_PEER_LOCALMSPID=hqMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hq.gsa.com/users/Admin@hq.gsa.com/msp" cli peer chaincode instantiate -o orderer.gsa.com:7050 -C mychannel -n $CONTRACT -v 1.0 -c '{"Args":[""]}' -P "OR ('hqMSP.member','kcfMSP.member')"
sleep 10
docker exec -e "CORE_PEER_LOCALMSPID=hqMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hq.gsa.com/users/Admin@hq.gsa.com/msp" cli peer chaincode invoke -o orderer.gsa.com:7050 -C mychannel -n $CONTRACT -c '{"function":"initLedger","Args":[""]}'

docker exec -e "CORE_PEER_LOCALMSPID=hqMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hq.gsa.com/users/Admin@hq.gsa.com/msp" cli peer chaincode query -C mychannel -n $CONTRACT -c '{"function":"queryAllOffers","Args":[""]}'

printf "\nTotal execution time : $(($(date +%s) - starttime)) secs ...\n\n"
