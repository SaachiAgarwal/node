var express = require('express');
var orders = express.Router();
var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var logger=require('../controllers/logger');
var orderController=require('../controllers/orderController');
var emailController=require('../controllers/emailController');
var token;
var bcrypt = require('bcrypt');
var config=require('../config/config');
var flash = require('express-flash');
orders.use(flash());

orders.use(cors());
var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";
var resMsg=require('../config/resMessage');
orders.use(function(req, res, next) {
    var token = req.body.token || req.headers['token'];
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, function(err,dt) {
            console.log(err);
            if (err) {
                res.status(200).json({"error":5,"data":resMsg.tokenError});
            } else {
                //console.log(dt);
                req.session.usrData=dt;
                next();
            }
        })
    } else { 
        res.status(403).json({"error":1,"data":resMsg.emptyToken});
    }
});
orders.post('/update_order',function(req,res){
    orderController.check_duplicate_order(req.session.usrData.company_key_id,req.body.order_number,req.body.order_id,function(result){
        if(!result.error)
        {
            if(result.duplicate){
                res.status(200).json({"error":1,"data":"Order number already exists"});
            }else{
                req.body.order_description=(req.body.order_description===undefined ||req.body.order_description==='')?'':req.body.order_description;
                req.body.order_expected_date=(req.body.order_expected_date===undefined ||req.body.order_expected_date==='')?null:req.body.order_expected_date;
                var sql="CALL "+config.dbname+".usp_update_order (?,?,?,?,?,?,?,?)";
                database.connection.getConnection(function(err, connection) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(500).json({"error":1,"data":"Internal server error"});
                    } else {
                        connection.query(sql,[req.body.order_id,req.body.order_number,req.session.usrData.company_key_id,req.body.supplier_company_key_id,req.body.order_date,req.body.order_expected_date,req.body.order_description,req.session.usrData.user_id],function(err, rows) {
                            if(err){
                                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                                res.status(200).json({"error":1,"data":"Sql error"})
                            }else{
                                res.status(200).json({"error":0,"data":"Order updated successfully"})
                            }
                            connection.release();
                        });
                    }
                });
            }
        }else{
            res.json(result);
        }
    });
});
orders.post('/delete_order',function(req,res){
    database.connection.getConnection(function(err, connection) {
        var appData={};
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_delete_order ("+req.body.order_id+","+req.session.usrData.user_id+")";
            connection.query(sql, function(err, rows) {
                //console.log(rows);
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                } else {
                    //console.log(rows);
                    if(rows==="undefined")
                    {
                        res.status(404).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.json({"error":0,"data":"Order deleted"});
                    }
                }
            });
            connection.release();
        }
    });
});
orders.post('/delete_order_product',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @b_result=0; CALL "+config.dbname+".usp_delete_order_product ("+req.body.order_product_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
            connection.query(sql, function(err, rows) {          
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Product deleted"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});
orders.post('/fetch_orders',function(req,res){
    var from_date=req.body.from_date===undefined?"":req.body.from_date;
    var to_date=req.body.to_date===undefined?"":req.body.to_date;
    var search_text=req.body.search_text===undefined?"":req.body.search_text;
    var page_size=req.body.page_size===undefined?10:req.body.page_size;
    var page_number=req.body.page_number===undefined?1:req.body.page_number;
    var sql="CALL "+config.dbname+".usp_get_incoming_orders_temp ("+req.session.usrData.company_key_id+",'"+from_date+"','"+to_date+"','"+search_text+"',"+page_size+","+page_number+",@total_recordcount);SELECT @total_recordcount as total_recordcount";
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error}":1,"data":"Internal server error"});
        } else {
            connection.query(sql, function(err, rows, fields) {
                if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        appData["error"]=1;
                        appData["data"]="Sql error";
                        res.json(appData);
                }else {
                    if(rows==="undefined")
                      {  appData["error"]=1;
                        appData["data"]="No such record";
                        res.json(appData);}
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
    //console.log(from_date);
});
orders.post('/brand_orders',function(req,res){
    
    var from_date=req.body.from_date===undefined?"":req.body.from_date;
    var to_date=req.body.to_date===undefined?"":req.body.to_date;
    var search_text=req.body.search_text===undefined?"":req.body.search_text;
    var page_size=req.body.page_size===undefined?10:req.body.page_size;
    var page_number=req.body.page_number===undefined?1:req.body.page_number;
    var brand_id=req.body.brand_id===undefined?"":req.body.brand_id;
   /* console.log("brand_id");

    console.log(brand_id);
    console.log('requesting brand_orders ', req);*/
    var sql="CALL "+config.dbname+".usp_get_outgoing_orders('"+brand_id+"','"+from_date+"','"+to_date+"','"+search_text+"','',"+page_size+","+page_number+",@total_recordcount);SELECT @total_recordcount as total_recordcount";
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error}":1,"data":"Internal server error"});
        } else {
            connection.query(sql, function(err, rows, fields) {
                if (err) {
                    console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        appData["error"]=1;
                        appData["data"]="Sql error";
                        res.json(appData);
                }else {
                    if(rows==="undefined")
                      {  appData["error"]=1;
                        appData["data"]="No such record";
                        res.json(appData);}
                    else
                    {
                         res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
   // console.log(from_date);
});
orders.post('/link_order',function(req,res){
    var sql="SET @b_result = 0; CALL "+config.dbname+".usp_link_order("+req.body.order_id+","+req.session.usrData.company_key_id+","+req.body.linkedorder_id+","+req.body.linkedcompany_key_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            connection.query(sql, function(err, rows, fields) {
                logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                }else {
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Product linked successfully"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});
orders.post('/unlink_order',function(req,res){
    var sql="CALL "+config.dbname+".usp_unlink_order("+req.body.order_id+","+req.session.usrData.company_key_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            connection.query(sql, function(err, rows, fields) {
                logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                 if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                }else {
                    if(rows[1][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Product unlinked successfully"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});

orders.get('/view_order',function(req,res){
    var dataObj={};
    
 /*   console.log(req.query.order_id);
    console.log(req.body.order_id);*/

    var sql="CALL "+config.dbname+".usp_view_order("+req.query.order_id+")";
    //console.log(sql);
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql, function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                }else {
                    if(rows===undefined ||rows[0].length===0)
                    {  
                        res.json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        dataObj.orderDetail=rows[0][0];
                        var sql="CALL "+config.dbname+".usp_get_order_products("+req.query.order_id+")";
                        connection.query(sql, function(err, rows, fields) {
                            if (err) {
                                //console.log("----------");
                                console.log(err);
                                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                                    res.json({"error":1,"data":"Sql error"});
                            }else {
                                if(rows===undefined)
                                {  
                                    res.json({"error":1,"data":"No such record"});
                                }
                                else
                                {
                                    dataObj.products=rows[0];
                                    res.json(dataObj);
                                }
                            }
                        });
                    }
                }
            });
            connection.release();
        }
    });      
});
orders.post('/place_order',function(req,res){
    var sql="CALL "+config.dbname+".usp_place_order("+req.body.order_id+","+req.session.usrData.company_key_id+","+req.session.usrData.user_id+")";
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql, function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                }else {
                    if(rows.affectedRows===1)
                    {
                        res.json({"error":0,"data":"Order placed successfully"});  
                        let data={};
                        data.user_action=true;
                        data.usrData=req.session.usrData;
                        data.company_key_id=req.session.usrData.company_key_id;
                        data.recv_company_key_id=(req.body.recv_company_key_id===undefined)?1:req.body.recv_company_key_id;
                        data.action="placed";
                        data.subject="Order placed : order number-"+req.body.order_number;
                        data.procedure=".usp_get_order_emailusers"
                        data.id=req.body.order_id;
                        data.order_number=req.body.order_number;
                        data.user_type="Suppliers";
                        emailController.create_mail(data,function(result){
                            //console.log('result',result);
                        })
                        /*console.log('data in cancel_ordera',data);
                        console.log('data1',data);*/
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"});
                    }
                }
            });
            connection.release();
        }
    });      
});
orders.post('/incoming_orders',function(req,res){
    var appData={};

    let db_param=req.body.db_param===undefined?'':req.body.db_param;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    let page_size=req.body.page_size===undefined?10:req.body.page_size;
    let page_number=req.body.page_number===undefined?1:req.body.page_number;
    //console.log(db_param);
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_incoming_orders (?,?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            //console.log(sql);
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,db_param,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
});

//download incoming orders along with products 
orders.post('/download_incoming_orders',function(req,res){
    var appData={};

    let db_param=req.body.db_param===undefined?'':req.body.db_param;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    //console.log(db_param);
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_incoming_orders_download (?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            //console.log(sql);
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,db_param],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
});

orders.post('/outgoing_orders',function(req,res){
    var appData={};
    let db_param=req.body.db_param===undefined?'':req.body.db_param;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    let page_size=req.body.page_size===undefined?10:req.body.page_size;
    let page_number=req.body.page_number===undefined?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_outgoing_orders (?,?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            console.log(sql);
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,db_param,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
                connection.release();
            });
            
        }
    });
});

orders.post('/download_outgoing_orders',function(req,res){
    var appData={};
    let db_param=req.body.db_param===undefined?'':req.body.db_param;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_outgoing_orders_download (?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            console.log(sql);
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,db_param],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
                connection.release();
            });
            
        }
    });
});

///////////////////////// New Design

orders.post('/update_order/product',function(req,res){
    var glm=req.body.glm===undefined?'':req.body.glm;
    var mpg=req.body.mpg===undefined?'':req.body.mpg;
    var blendpercentage=req.body.blend_percentage===undefined?100:req.body.blend_percentage;
    var product_description=req.body.product_description===undefined?'':req.body.product_description;
    req.checkBody('order_product_id').exists().withMessage('Order product id cant be blank');
    req.checkBody('product_name').exists().withMessage('Product name cant be blank');
    req.checkBody('product_id').exists().withMessage('product id cant be blank');
    req.checkBody('product_qty').exists().withMessage('Product quantity cant be blank');
    req.checkBody('product_uom').exists().withMessage('Product uom cant be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{

        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
                //var sql="SET @b_result= 0; CALL "+config.dbname+".usp_update_order_product("+req.body.order_product_id+","+req.session.usrData.company_key_id+",'"+req.body.product_name+"','"+product_description+"','"+req.body.product_id+"',"+req.body.product_qty+",'"+req.body.product_uom+"','"+glm+"','"+mpg+"',"+req.session.usrData.user_id+",@b_result); select @b_result";
                var sql="SET @b_result= 0; CALL "+config.dbname+".usp_update_order_product(?,?,?,?,?,?,?,?,?,?,?,@b_result); select @b_result";
                console.log(sql);
                connection.query(sql,[req.body.order_product_id,req.session.usrData.company_key_id,req.body.product_name,product_description,req.body.product_id,req.body.product_qty,req.body.product_uom,glm,mpg,blendpercentage,req.session.usrData.user_id],function(err, rows) {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.json({"error":1,"data":"Sql error"});
                    } else {
                        if(rows[2][0]['@b_result']===1)
                        { 
                            res.json({"error":0,"data":"Product updated successfully"});
                        }
                        else
                        {
                            res.json({"error":1,"data":"No such record"});   
                        }
                    }
                });
                connection.release();
            }
        });
    }
});

///////////////    New Design

orders.post('/create_order',function(req,res){
    req.checkBody('order_number').exists().withMessage('Order No. cant be blank');
    req.checkBody('order_date').exists().withMessage('Order date can not be blank');
    req.checkBody('supplier_company_key_id').exists().withMessage('Supplier company cant be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        
        var order_description=(req.body.order_description===undefined)?' ':req.body.order_description;
        var status=(req.body.status===undefined)?'Open':req.body.status;
        console.log("order_description===="+order_description+"==");
        var order_id='';
        orderController.check_duplicate_order(req.session.usrData.company_key_id,req.body.order_number,order_id,function(result){
            if(!result.error)
            {
                if(result.duplicate){
                    res.json({"error":1,"data":"Order number already exists"});
                }else{
                    req.body.order_expected_date=(req.body.order_expected_date===undefined)?null:req.body.order_expected_date;
                    //var sql="SET @order_id = 0; CALL "+config.dbname+".usp_create_order(\""+req.body.order_number+"\","+req.session.usrData.company_key_id+","+req.body.supplier_company_key_id+",\""+req.body.order_date+"\",\""+order_description+"\",\""+status+"\","+req.session.usrData.user_id+",@order_id); SELECT @order_id as order_id";
                    var sql="SET @order_id = 0; CALL "+config.dbname+".usp_create_order(?,?,?,?,?,?,?,?,@order_id); SELECT @order_id as order_id";
                    var appData={};
                    console.log(typeof req.body.order_expected_date);
                    database.connection.getConnection(function(err, connection) {
                        if (err) {
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                            res.status(200).json({"error":1,"data":"Internal server error"});
                        } else {
                            connection.query(sql,[req.body.order_number,req.session.usrData.company_key_id,req.body.supplier_company_key_id,req.body.order_date,req.body.order_expected_date,order_description,status,req.session.usrData.user_id], function(err, rows) {
                                logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));    
                                if (!err) {
                                    res.status(201).json(rows[2][0]);
                                } else {
                                    console.log(err);
                                    res.status(200).json({"error":1,"data":"Sql error"});
                                }
                                connection.release();
                            });
                        }
                    });
                }
            }
            else{
                res.json({"error":1,"data":"Internal server error"});
            }
        });
    }  
});

///////////// New Design

orders.post('/add_product_order',function(req,res){
    req.checkBody('order_id').exists().withMessage('Order Id cant be blank');
    req.checkBody('product_name').exists().withMessage('Product name can not be blank');
    req.checkBody('product_qty').exists().withMessage('Product quantity cant be blank');
    req.checkBody('product_uom').exists().withMessage('Product uom cant be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"error":req.validationErrors()});
    }else{
        orderController.add_product(req.session.usrData.company_key_id,req.session.usrData.user_id,req.body,function(result){
            res.json(result);
        });
    }
});
orders.post('/cancel_order',function(req,res){
    if(req.body.order_id===undefined)
        res.status(200).json({"error":1,"data":"Order id cant be blank"});
    else{
        var cancel_reason=(req.body.cancel_reason===undefined)?'':req.body.cancel_reason;
        var sql="SET @b_result = 0; CALL "+config.dbname+".usp_cancel_order (?,?,?,@b_result);SELECT @b_result";
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":"Internal server error"});
            } else {
                connection.query(sql,[req.body.order_id,cancel_reason,req.session.usrData.user_id],function(err, rows) {
                    if(err)
                    {
                        res.status(200).json({"error":1,"data":"Sql error"});
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    }    
                    else if(rows[rows.length-1][0]!==undefined && rows[rows.length-1][0]["@b_result"]===1){
                        res.status(200).json({"error":0,"data":"Order cancelled"});
                        let data={};
                        data.user_action=true;
                        data.usrData=req.session.usrData;
                        data.company_key_id=req.session.usrData.company_key_id;
                        data.recv_company_key_id=(req.body.recv_company_key_id===undefined)?1:req.body.recv_company_key_id;
                        data.action="cancelled";
                        data.subject="Order cancelled : order number-"+req.body.order_number;
                        data.procedure=".usp_get_order_emailusers"
                        data.id=req.body.order_id;
                        data.order_number=req.body.order_number;
                        data.cancel_reason=req.body.cancel_reason;
                        data.user_type="Suppliers";
                        emailController.create_mail(data,function(result){
                            //console.log('result',result);
                        })
                   /*     console.log('data in cancel_ordera',data);
                        console.log('data1',data);*/
                    }else{
                        res.status(200).json({"error":1,"data":"Failed cancelling order"});
                    }
                    connection.release();
                });
            }
        });
    }
});
orders.post('/incoming_buyer_order',function(req,res){
    if(req.body.buyer_company_key_id===undefined){
         res.status(200).json({"error":1,"data":"Please send buyer company key id"});
    }else{
        let from_date=req.body.from_date===undefined?"":req.body.from_date;
        let to_date=req.body.to_date===undefined?"":req.body.to_date;
        let search_text=req.body.search_text===undefined?"":req.body.search_text;
        let page_size=req.body.page_size===undefined?10:req.body.page_size;
        let page_number=req.body.page_number===undefined?1:req.body.page_number;
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
                var sql="CALL "+config.dbname+".usp_get_buyer_orders (?,?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
                console.log(sql);
                connection.query(sql,[req.session.usrData.company_key_id,req.body.buyer_company_key_id,from_date,to_date,search_text,page_size,page_number], function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(500).json({"error":1,"data":"Sql error"});
                    }else {
                        if(rows==="undefined")
                        {
                            res.status(400).json({"error":1,"data":"No such record"});
                        }
                        else
                        {
                            res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                        }
                    }
                });
                connection.release();
            }
        });
    }
});
orders.post('/list_linking_orders',function(req,res){
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    let page_size=req.body.page_size===undefined?10:req.body.page_size;
    let page_number=req.body.page_number===undefined?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            //console.log("qqqqqqqqqq");
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
          /*   console.log("company_key_id");
             console.log(req.session.usrData.company_key_id);*/
            var sql="CALL "+config.dbname+".usp_get_linking_orders (?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            //console.log(sql);
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                 /*   console.log("company_key_id");
                    console.log(req.session.usrData.company_key_id);*/
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
});

module.exports = orders;
