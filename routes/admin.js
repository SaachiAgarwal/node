var express = require('express');
const xlsx = require("node-xlsx");
const fs = require('fs')
var admin = express.Router();
var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');
var upload = require('express-fileupload');
const path= require('path');

var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var orderController=require('../controllers/orderController');
var token;
var bcrypt = require('bcrypt');
var config=require('../config/config');
var flash = require('express-flash');
var emailController=require('../controllers/emailController')
var logger=require('../controllers/logger');
admin.use(flash());
admin.use(cors());
admin.use(upload());
var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";
var resMsg=require('../config/resMessage');
admin.use(function(req, res, next) {
    var token = req.body.token || req.headers['token'];
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, function(err,dt) {
            console.log(err);
            if (err) {
                res.status(200).json({"error":5,"data":resMsg.tokenError});
            } else {
                req.session.usrData=dt;
                //if(!dt.open_api && dt.is_super_user)
                if(!dt.open_api)
                    next();
                else
                    res.status(200).json({"error":1,"data":"You are not allowed for admin activity"});
            }
        })
    } else {
        res.status(403).json({"error":1,"data":resMsg.emptyToken});
    }
});
admin.get('/check_stock_uploaded',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_is_stock_uploaded (?,@b_result); SELECT @b_result as b_result"; 
            connection.query(sql,[req.session.usrData.company_key_id],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[1]===undefined)
                      {  
                        res.status(200).json({"error":1,"data":resMsg.noRecord});}
                    else
                    {
                        res.status(200).json({"error":0,"data":{"isUploaded":rows[1][0]['b_result']}});
                    }
                }
            });
            connection.release();
        }
    });
});
admin.get('/get_all_products',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_all_products()"; 
            connection.query(sql,[req.session.usrData.company_key_id],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0]});
                }
            });
            connection.release();
        }
    });
});
admin.get('/get_all_companies',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_all_companies()"; 
            connection.query(sql,[req.session.usrData.company_key_id],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0]});
                }
            });
            connection.release();
        }
    });
});
admin.post('/list_companies',function(req,res){
    let search_text=(req.body.search_text===undefined)?'':req.body.search_text;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_company_listing(?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;"; 
            connection.query(sql,[search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                }
            });
            connection.release();
        }
    });
});

admin.post('/list_companies_by_companyid',function(req,res){
    let company_key_id=(req.body.company_key_id===undefined)?'':req.body.company_key_id;
    let search_text=(req.body.search_text===undefined)?'':req.body.search_text;
    let page_size=(req.body.page_size===undefined)?5:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {

            var sql="CALL "+config.dbname+".usp_get_parent_assign_companylist(?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";

            connection.query(sql,[company_key_id,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                   
                    res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                }
            });
            connection.release();
        }
    });
});

admin.get('/company_detail',function(req,res){
    if(req.query.company_key_id===undefined)
        res.status(200).json({"error":1,"data":"Please send company key id"});
    else{
        emailController.make_data(req.query.company_key_id).then(function(result){
            //res.status(200).json({"error":0,"data":result});
           // console.log('result',result);
            if(result.company_type_name==="Integrated Player"){
                get_role(req.query.company_key_id).then((rslt)=>{
                    result.roles=rslt;
                    res.status(200).json({"error":0,"data":result});
                }).catch((err)=>{
                    res.status(200).json({"error":0,"data":result});
                })
            }else{
               res.status(200).json({"error":0,"data":result}); 
            }
            
        }).catch(function(err){
            res.status(200).json({"error":1,"data":err});
        });
    }
});
admin.post('/delete_company',function(req,res){
    if(req.body.company_key_id===undefined || req.body.company_key_id==='')
    {
        res.status(200).json({"error":1,"data":"Please send company key id"});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_delete_company(?,?)"; 
                connection.query(sql,[req.body.company_key_id,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else {     
                        res.status(200).json({"error":0,"data":resMsg.compDeleted});
                    }
                });
                connection.release();
            }
        });
    }    
});
admin.post('/list_users',function(req,res){
    let search_text=(req.body.search_text===undefined)?'':req.body.search_text;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_user_listing(?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount"; 
            connection.query(sql,[search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                }
            });
            connection.release();
        }
    });
});
admin.post('/check_duplicate_company',function(req,res){
    req.checkBody('company_name').exists().withMessage('Company name can not be blank');
    req.checkBody('company_key_id').exists().withMessage('Company key id can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_is_companyname_duplicate(?,?,@b_result); SELECT @b_result as b_result"; 
                connection.query(sql,[req.body.company_name,req.body.company_key_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        if(rows[rows.length-1]!==undefined)
                            res.status(200).json({"error":0,"data":rows[rows.length-1][0]});
                        else
                            res.status(200).json({"error":1,"data":resMsg.error})
                    }
                });
                connection.release();
            }
        });
    }    
});
admin.post('/list_company_bu',function(req,res){
    let search_text=(req.body.search_text===undefined)?'':req.body.search_text;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_companybu_listing(?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;"; 
            connection.query(sql,[search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                }
            });
            connection.release();
        }
    });
});

admin.post('/create_company',function(req,res){

    req.checkBody('company_name').exists().withMessage('Company name can not be blank');
    req.checkBody('company_type_id').exists().withMessage('Company type id can not be blank');
    if(req.validationErrors()){
        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        let is_external=req.body.is_external===undefined?'':req.body.is_external;
        let parentcompany_id=req.body.parent_company_id?0:req.body.parent_company_id;
        let linked_forest_key_id=req.body.linked_forest_key_id===undefined?0:req.body.linked_forest_key_id;
	let product_id=req.body.product_id===undefined?0:req.body.product_id;
	let conversion_factor=req.body.conversion_factor===''?2.39:req.body.conversion_factor;
        let company_role_id=req.body.company_role_id===undefined?'':req.body.company_role_id;
        let company_address=req.body.company_address===undefined?'':req.body.company_address;
        let country=req.body.country===undefined?'':req.body.country;
        let state=req.body.state===undefined?'':req.body.state;
        let city=req.body.city===undefined?'':req.body.city;
        let zip=req.body.zip===undefined?'':req.body.zip;
        let email_address=req.body.email_address===undefined?'':req.body.email_address;
        let phone_number=req.body.phone_number===undefined?'':req.body.phone_number;
        let latitude=req.body.latitude===undefined?'':req.body.latitude;
        let longitude=req.body.longitude===undefined?'':req.body.longitude;
  console.log(typeof (company_role_id));

        console.log(typeof (req.body.company_role_id));
        database.connection.getConnection(function(err, connection) {
            if (err) {
		console.log("in here 2")
                console.log(err);
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
console.log("in here 3")

                   var sql="CALL "+config.dbname+".usp_create_company(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@companykey_id,@trans_error,@err_message);SELECT @companykey_id as company_key_id ,@trans_error as trans_error,@err_message as err_message;"; 
                connection.query(sql,[req.body.company_name, req.body.parent_company_id,product_id,conversion_factor,req.body.company_type_id,company_role_id,company_address,country,state,city,zip,email_address,phone_number,latitude,longitude,is_external,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        //console.log(rows);
                        //let error_message = err_message;
                        //console.log(error_message);
                        let msg=rows[rows.length-1][0]['trans_error']===1?resMsg.compExists:resMsg.error;
                        if(rows[rows.length-1][0]['trans_error']!==0){
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows[rows.length-1]));
                            console.log("__________________________________")
                            console.log(rows[rows.length-1][0]['err_message']);
                            res.status(200).json({"error":1,"data":rows[rows.length-1][0]['err_message'] ,"company_key_id":rows[rows.length-1][0]['company_key_id']  });
                        }else{
                            res.status(200).json({"error":0,"data":rows[rows.length-1]})
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});





admin.post('/update_company',function(req,res){
    req.checkBody('company_name').exists().withMessage('Company name can not be blank');
    req.checkBody('company_key_id').exists().withMessage('Company name can not be blank');
    req.checkBody('company_type_id').exists().withMessage('Company type id can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        let parentcompany_id=req.body.parent_company_id==undefined?'':req.body.parent_company_id;
 	//let linked_forest_key_id=req.body.linked_forest_key_id===undefined?0:req.body.linked_forest_key_id;
	let product_id=req.body.product_id===undefined?0:req.body.product_id;	
	let conversion_factor=req.body.conversion_factor===''?2.39:req.body.conversion_factor;
        let company_role_id=req.body.company_role_id===undefined?'':req.body.company_role_id;
        let company_address=req.body.company_address===undefined?'':req.body.company_address;
        let country=req.body.country===undefined?'':req.body.country;
        let state=req.body.state===undefined?'':req.body.state;
        let city=req.body.city===undefined?'':req.body.city;
        let zip=req.body.zip===undefined?'':req.body.zip;
        let email_address=req.body.email_address===undefined?'':req.body.email_address;
        let phone_number=req.body.phone_number===undefined?'':req.body.phone_number;
        let latitude=req.body.latitude===undefined?0:req.body.latitude;
        let longitude=req.body.longitude===undefined?0:req.body.longitude;

               database.connection.getConnection(function(err, connection) {
            if (err) {
                console.log(err);
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_update_company(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@trans_error,@err_message);SELECT @trans_error as trans_error,@err_message as err_message;"; 
                connection.query(sql,[req.body.company_key_id,req.body.company_name,req.body.parent_company_id,product_id,conversion_factor,req.body.company_type_id,company_role_id,company_address,country,state,city,zip,email_address,phone_number,latitude,longitude,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        let msg=rows[rows.length-1][0]['trans_error']===1?resMsg.compExists:resMsg.error;
                        if(rows[rows.length-1][0]['trans_error']!==0){
                            console.log(err);
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows[rows.length-1]));
                            res.status(200).json({"error":1,"data":msg})
                        }else{

                            res.status(200).json({"error":0,"data":resMsg.compUpdated})
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});


admin.post('/update_external_company_to_normal',function(req,res){
  req.checkBody('company_key_id').exists().withMessage('Company name can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_update_external_company_to_normal(?,?,@b_result);SELECT @b_result as b_result;"; 
                connection.query(sql,[req.body.company_key_id,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else{
                        console.log(rows[rows.length-1][0]['b_result']);
                            res.status(200).json({"error":0,"company_key_id":rows[rows.length-1][0]['b_result']})
                        }                       
                    
                });
                connection.release();
            }
        });
    }
});

admin.post('/create_company_bu',function(req,res){
    req.checkBody('company_bu_name').exists().withMessage('Company bu name can not be blank');
    req.checkBody('company_id').exists().withMessage('Company id can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        let company_bu_address=req.body.company_bu_address===undefined?'':req.body.company_bu_address;
        let country=req.body.country===undefined?'':req.body.country;
        let state=req.body.state===undefined?'':req.body.state;
        let city=req.body.city===undefined?'':req.body.city;
        let zip=req.body.zip===undefined?'':req.body.zip;
        let email_address=req.body.email_address===undefined?'':req.body.email_address;
        let phone_number=req.body.phone_number===undefined?'':req.body.phone_number;
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_create_company_bu(?,?,?,?,?,?,?,?,?,?,@b_result,@err_message);SELECT @b_result as b_result,@err_message as err_message;"; 
                connection.query(sql,[req.body.company_id,req.body.company_bu_name,company_bu_address,country,state,city,zip,email_address,phone_number,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        console.log(rows);
                        let msg=rows[rows.length-1][0]['b_result']===1?resMsg.compBuExists:resMsg.error;
                        if(rows[rows.length-1][0]['b_result'] ===undefined ||rows[rows.length-1][0]['b_result']===1){
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows[rows.length-1]));
                            res.status(200).json({"error":1,"data":msg})
                        }else{
                            res.status(200).json({"error":0,"data":resMsg.compBUCreated});
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});
admin.post('/update_company_bu',function(req,res){
    req.checkBody('company_bu_name').exists().withMessage('Company bu name can not be blank');
    req.checkBody('company_bu_id').exists().withMessage('Company bu id can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        let company_bu_address=req.body.company_bu_address===undefined?'':req.body.company_bu_address;
        let country=req.body.country===undefined?'':req.body.country;
        let state=req.body.state===undefined?'':req.body.state;
        let city=req.body.city===undefined?'':req.body.city;
        let zip=req.body.zip===undefined?'':req.body.zip;
        let email_address=req.body.email_address===undefined?'':req.body.email_address;
        let phone_number=req.body.phone_number===undefined?'':req.body.phone_number;
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                console.log(req.body.company_bu_id);
                var sql="CALL "+config.dbname+".usp_update_company_bu(?,?,?,?,?,?,?,?,?,?,?,@b_result,@err_message);SELECT @b_result as b_result,@err_message as err_message;"; 
                connection.query(sql,[req.body.company_bu_id,req.body.company_id,req.body.company_bu_name,company_bu_address,country,state,city,zip,email_address,phone_number,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        console.log(rows);
                        let msg=rows[rows.length-1][0]['b_result']===1?resMsg.compBuExists:resMsg.error;
                        if(rows[rows.length-1][0]['b_result'] ===undefined ||rows[rows.length-1][0]['b_result']===1){
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows[rows.length-1]));
                            res.status(200).json({"error":1,"data":msg})
                        }else{
                            res.status(200).json({"error":0,"data":resMsg.compBuUpdated})
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});
admin.post('/delete_company_bu',function(req,res){
    if(req.body.company_bu_id===undefined || req.body.company_bu_id==='')
    {
        res.status(200).json({"error":1,"data":"Please send company bu id"});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_delete_company_bu(?,?,@b_result);SELECT @b_result as b_result"; 
                connection.query(sql,[req.body.company_bu_id,req.session.usrData.user_id],function(err, rows, fields) {
                    console.log(rows);
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else if(rows[rows.length-1][0]!==undefined && rows[rows.length-1][0]['b_result']===null) {     
                        res.status(200).json({"error":0,"data":resMsg.compBuDeleted});
                    }else{
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                        res.status(200).json({"error":1,"data":resMsg.error});
                    }
                });
                connection.release();
            }
        });
    }    
});
admin.get('/defined_process_loss',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_defined_process_loss()"; 
            connection.query(sql,function(err, rows, fields) {
                console.log(rows);
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else{
                    res.status(200).json({"error":0,"data":rows[0]});
                }
            });
            connection.release();
        }
    });      
});
admin.post('/defined_process_loss',function(req,res){
    /*req.checkBody('process_loss_id').exists().withMessage('Process loss id can not be blank');
    req.checkBody('process_loss').exists().withMessage('Process loss can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_update_defined_process_loss(?,?)"; 
                connection.query(sql,[req.body.process_loss_id,req.body.process_loss], function(err, rows, fields) {
                    console.log(rows);
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else{
                        res.status(200).json({"error":0,"data":resMsg.prLossUpdated});
                    }
                });
                connection.release();
            }
        });
    }    */      
    orderController.update_process_loss(req.body,function(data){
        res.json(data);
    })
});
admin.post('/list_products',function(req,res){
    let search_text=(req.body.search_text===undefined)?'':req.body.search_text;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_product_listing(?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;"; 
            connection.query(sql,[search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                }
            });
            connection.release();
        }
    });
});
admin.post('/create_product',function(req,res){
    req.checkBody('product_type_id').exists().withMessage('Product type id can not be blank');
    req.checkBody('product_name').exists().withMessage('Product name can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        let product_description=req.body.product_description===undefined?'':req.body.product_description;
        let product_uom=req.body.product_uom===undefined?'':req.body.product_uom;
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_create_product(?,?,?,?,?,@b_result,@err_message);SELECT @b_result as b_result,@err_message as err_message;"; 
                connection.query(sql,[req.body.product_type_id,req.body.product_name,product_description,product_uom,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        console.log(rows);
                        let msg=rows[rows.length-1][0]['b_result']===1?resMsg.prNameExist:resMsg.error;
                        if(rows[rows.length-1][0]['b_result'] ===undefined ||rows[rows.length-1][0]['b_result']===1){
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows[rows.length-1]));
                            res.status(200).json({"error":1,"data":msg})
                        }else{
                            res.status(200).json({"error":0,"data":resMsg.prCreated,"product_id":rows[rows.length-1][0]['b_result']})
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});
admin.post('/update_product',function(req,res){
    req.checkBody('product_id').exists().withMessage('Product id can not be blank');
    req.checkBody('product_type_id').exists().withMessage('Product type id can not be blank');
    req.checkBody('product_name').exists().withMessage('Product name can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        let product_description=req.body.product_description===undefined?'':req.body.product_description;
        let product_uom=req.body.product_uom===undefined?'':req.body.product_uom;
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_update_product(?,?,?,?,?,?,@b_result,@err_message);SELECT @b_result as b_result,@err_message as err_message;"; 
                connection.query(sql,[req.body.product_id,req.body.product_type_id,req.body.product_name,product_description,product_uom,req.session.usrData.user_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        console.log(rows);
                        let msg=rows[rows.length-1][0]['b_result']===1?resMsg.prNameExist:resMsg.error;
                        if(rows[rows.length-1][0]['b_result'] ===undefined ||rows[rows.length-1][0]['b_result']===1){
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows[rows.length-1]));
                            res.status(200).json({"error":1,"data":msg})
                        }else{
                            res.status(200).json({"error":0,"data":resMsg.prUpdated,"product_id":rows[rows.length-1][0]['b_result']})
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});
admin.post('/delete_product',function(req,res){
    if(req.body.product_id===undefined || req.body.product_id==='')
    {
        res.status(200).json({"error":1,"data":"Please send product id"});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_delete_product(?,?,@b_result);SELECT @b_result as b_result"; 
                connection.query(sql,[req.body.product_id,req.session.usrData.user_id],function(err, rows, fields) {
                    console.log(rows);
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else if(rows[rows.length-1][0]!==undefined && rows[rows.length-1][0]['b_result']===1) {     
                        res.status(200).json({"error":0,"data":resMsg.prDeleted});
                    }else{
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                        res.status(200).json({"error":1,"data":resMsg.error});
                    }
                });
                connection.release();
            }
        });
    }    
});
admin.get('/user_details',function(req,res){
    if(req.query.user_id===undefined || req.query.user_id==='')
    {
        res.status(200).json({"error":1,"data":"Please send user id"});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_get_user_details(?)"; 
                connection.query(sql,[req.query.user_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else if(rows[0]!==undefined && rows[0].length !==0) {  
                        let data={};
                        data=rows[0][0];
                        data.user_id=req.query.user_id;   
                        res.status(200).json({"error":0,"data":data});
                    }else{
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                        res.status(200).json({"error":1,"data":resMsg.userNotFound});
                    }
                });
                connection.release();
            }
        });
    }
});
admin.post('/list_users',function(req,res){
    let search_text=(req.body.search_text===undefined)?'':req.body.search_text;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_user_listing(?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;"; 
            connection.query(sql,[search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                }
            });
            connection.release();
        }
    });
});
admin.post('/delete_user',function(req,res){
    if(req.body.user_id===undefined)
        res.status(200).json({"error":1,"data":"Please send user id"});
    else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_delete_user(?,@b_result);SELECT @b_result as b_result;"; 
                connection.query(sql,[req.body.user_id],function(err, rows, fields) {
                    console.log('rows',rows);
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        res.status(200).json({"error":0,"data":resMsg.userDeleted});
                    }
                });
                connection.release();
            }
        });
    }
});
admin.post('/update_user',function(req,res){
    req.checkBody('user_id').exists().withMessage('User id can not be blank');
    req.checkBody('first_name').exists().withMessage('First name can not be blank');
    req.checkBody('last_name').exists().withMessage('Last name can not be blank');
    req.checkBody('company_key_id').exists().withMessage('company_key_id can not be blank');
    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_update_user(?,?,?,?,?,?,?,@b_result);SELECT @b_result as b_result;"; 
                connection.query(sql,[req.body.user_id,req.body.first_name,req.body.last_name,req.body.phone_number,req.body.company_key_id,req.body.email,req.body.notify_usersemail],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else { 
                        console.log(rows);
                        if(rows[rows.length-1][0]!==undefined && rows[rows.length-1][0]['b_result']===1){
                            res.status(200).json({"error":0,"data":resMsg.userUpdated})
                        }else{
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                            res.status(200).json({"error":0,"data":resMsg.error})
                        }                       
                    }
                });
                connection.release();
            }
        });
    }
});
admin.get('/product_details',function(req,res){
    if(req.query.product_id===undefined || req.query.product_id==='')
    {
        res.status(200).json({"error":1,"data":"Please send product id"});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_get_product_detail(?)"; 
                connection.query(sql,[req.query.product_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else if(rows[0]!==undefined && rows[0].length !==0) {  
                        let data={};
                        data=rows[0][0];
                        data.product_id=req.query.product_id;   
                        res.status(200).json({"error":0,"data":data});
                    }else{
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                        res.status(200).json({"error":1,"data":resMsg.prNotFound});
                    }
                });
                connection.release();
            }
        });
    }
});
admin.get('/company_bu_details',function(req,res){
    if(req.query.company_bu_id===undefined || req.query.company_bu_id==='')
    {
        res.status(200).json({"error":1,"data":"Please send product id"});
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":resMsg.serverError});
            } else {
                var sql="CALL "+config.dbname+".usp_get_company_bu_detail(?)"; 
                connection.query(sql,[req.query.company_bu_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":resMsg.sqlError});
                    }else if(rows[0]!==undefined && rows[0].length !==0) {     
                        res.status(200).json({"error":0,"data":rows[0][0]});
                    }else{
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                        res.status(200).json({"error":1,"data":resMsg.compBuNotFound});
                    }
                });
                connection.release();
            }
        });
    }

});
admin.get('/company_role',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            var sql="CALL "+config.dbname+".usp_get_company_roles()"; 
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else if(rows[0]!==undefined && rows[0].length !==0) {     
                    res.status(200).json({"error":0,"data":rows[0]});
                }else{
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(rows));
                    res.status(200).json({"error":1,"data":resMsg.noRecord});
                }
            });
            connection.release();
        }
    });
});
admin.post('/admin_get_documents',function(req,res){
    console.log(req.query);
    console.log(req.body);
    console.log(req.query.page_number);
    let search_text=(req.body.search_text===undefined)?"":req.body.search_text;
    let from_date=(req.body.from_date===undefined)?"":req.body.from_date;
    let to_date=(req.body.to_date===undefined)?"":req.body.to_date;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_shipment_documents_by_companykey(?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            connection.query(sql,[req.body.company_key_id,from_date,to_date,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    { console.log(page_number);
                       res.status(200).json({"error":0,"documents":rows[0] ,"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
})
admin.post('/approve_document',function(req,res){    
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            console.log(req.body.shipment_document_id);           
            var sql="CALL "+config.dbname+".usp_approve_shipment_document(?,?)";
            connection.query(sql,[req.body.shipment_document_id,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    res.status(200).json({"error":0,"data":"Approved"});
                }
            });
            connection.release();
        }
    });
})

admin.post('/reject_document',function(req,res){    
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            console.log(req.body.shipment_document_id);           
            var sql="CALL "+config.dbname+".usp_reject_shipment_document(?,?,?)";
            connection.query(sql,[req.body.shipment_document_id,req.body.reject_remarks,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    res.status(200).json({"error":0,"data":"Rejected Document"});
                }
            });
            connection.release();
        }
    });
})

admin.post('/upload_linked_forest',function(req,res){
    var document_filename ; 
        /*console.log(req.body.shipmentid);
        console.log(req.body.document_filename);*/
        var documentname;
     /*   console.log('type',req.body.type);
      console.log('files',req.files);
      console.log('upfile',req.files.upfile);*/
      document_filename = req.body.document_filename;
console.log("input file")
console.log(req.files)
console.log(req.files.upfile)

      if(req.files.upfile){
console.log("input file")
console.log(req.files)
console.log(req.files.upfile)
        var file = req.files.upfile,
          documentname = file.name,
          type = file.mimetype;
        var uploadpath =path.join( __dirname ,'../forest_documents/') + document_filename;
        file.mv(uploadpath,function(err){
          if(err){
    /*        console.log(err);
            console.log("File Upload Failed",documentname,err);*/
            res.send("Error Occured!")
          }
          else {
           /* console.log("File Uploaded",documentname);
            console.log(uploadpath);     
            console.log(documentname); */  
            var sql="CALL "+config.dbname+".usp_create_linked_forest_document(?,?,?,?,@bresult); SELECT @bresult as bresult";    
        database.connection.getConnection(function(err, connection) {
            if (err) {
                console.log(err);
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":"Internal server error"});
            } else {
                connection.query(sql,[req.body.company_key_id,documentname,req.body.document_filename,req.session.usrData.user_id], function(err, rows, fields) {
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":"Sql error"});
                    }else { 
                        if (rows[1][0].bresult == 2)
                        {    fs.unlinkSync(uploadpath);
                             res.status(200).json({"error":1,"data":"Error: Document name already exists"});
                        }
                        
                        else {
                            res.status(200).json({"error":0,"data":"Document uploaded"});
                        }
                    }
                });
                connection.release();
            }
        });
    
          }
        });
      }
      else {
        res.send("No File selected !");
        res.end();
      };
    })
    
admin.post('/download_linked_forest_file',function(req,res){
var document_filename = req.body.file_name;
try{

const file = path.join( __dirname ,'../forest_documents/') + document_filename;
/*console.log(file);*/
   res.status(200).download(file); // Set disposition and send it.
}
catch (err) {
    console.log(err);
  }

})


admin.post('/get_forest_file',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_forest_documents_by_companykeyid("+req.body.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	console.log("_________________________________________");
                	console.log(rows[0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  console.log("&&&&&&&&&&&&&&&&&&&&&&&")
               			 console.log(rows[0]);
                        res.status(200).json({"error":0,"documents":rows[0]});
                    }
                    else
                    {console.log("**********************")
                    	console.log(rows[0]);
                       res.status(200).json({"error":0,"documents":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})


admin.post('/delete_linked_forest_document',function(req,res){
const file = path.join( __dirname ,'../forest_documents/') + req.body.document_file_name;
//console.log(file);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_linked_forest_document(?,?)"; 
           connection.query(sql,[req.body.linked_forest_document_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	   fs.unlinkSync(file);                                
                       res.status(200).json({"error":0,"data":"Document deleted"});
                    
                }
            });
            connection.release();
        }
    });
})

/*admin.post('/get_linked_forest_documents',function(req,res){       
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_linked_forest_by_companykey_id(?)";
            connection.query(sql,[req.body.company_key_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    { console.log(page_number);
                       res.status(200).json({"error":0,"Document":rows[0] });
                    }
                }
            });
            connection.release();
        }
    });
})
*/
admin.post('/upload_linked_forest_certificate',function(req,res){
    var certificate_filename ; 
        /*console.log(req.body.shipmentid);
        console.log(req.body.certificate_filename);*/
        var certificatename;
     /*   console.log('type',req.body.type);
      console.log('files',req.files);
      console.log('upfile',req.files.upfile);*/
      certificate_filename = req.body.certificate_filename;
      if(req.files.upfile){
        var file = req.files.upfile,
          certificatename = file.name,
          type = file.mimetype;
        var uploadpath =path.join( __dirname ,'../forest_documents/') + certificate_filename;
        file.mv(uploadpath,function(err){
          if(err){
    /*        console.log(err);
            console.log("File Upload Failed",certificatename,err);*/
            res.send("Error Occured!")
          }
          else {
           /* console.log("File Uploaded",certificatename);
            console.log(uploadpath);     
            console.log(certificatename); */  
            var sql="CALL "+config.dbname+".usp_create_linked_forest_certificate(?,?,?,?,@bresult); SELECT @bresult as bresult";    
        database.connection.getConnection(function(err, connection) {
            if (err) {
                console.log(err);
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":"Internal server error"});
            } else {
                connection.query(sql,[req.body.company_key_id,certificatename,req.body.certificate_filename,req.session.usrData.user_id], function(err, rows, fields) {
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":"Sql error"});
                    }else { 
                        if (rows[1][0].bresult == 2)
                        {    fs.unlinkSync(uploadpath);
                             res.status(200).json({"error":1,"data":"Error: Certificate name already exists"});
                        }
                        
                        else {
                            res.status(200).json({"error":0,"data":"Certificate uploaded"});
                        }
                    }
                });
                connection.release();
            }
        });
    
          }
        });
      }
      else {
        res.send("No File selected !");
        res.end();
      };
    })
    
admin.post('/delete_linked_forest_certificate',function(req,res){
const file = path.join( __dirname ,'../forest_documents/') + req.body.certificate_file_name;
//console.log(file);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_linked_forest_certificate(?,?)"; 
           connection.query(sql,[req.body.linked_forest_certificate_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	   fs.unlinkSync(file);                                
                       res.status(200).json({"error":0,"data":"Certificate deleted"});
                    
                }
            });
            connection.release();
        }
    });
})

admin.post('/get_linked_forest_certificates',function(req,res){       
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_linked_forest_certificate_by_companykey_id(?)";
            connection.query(sql,[req.body.company_key_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    { console.log(page_number);
                       res.status(200).json({"error":0,"certificate":rows[0] });
                    }
                }
            });
            connection.release();
        }
    });
})





//Get export rebate documents 

admin.post('/get_export_documents_by_company_keyid',function(req,res){
    let search_text=(req.body.search_text===undefined)?"":req.body.search_text;
    let from_date=(req.body.from_date===undefined)?"":req.body.from_date;
    let to_date=(req.body.to_date===undefined)?"":req.body.to_date;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_export_documents_by_companykey(?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            connection.query(sql,[req.body.company_key_id,from_date,to_date,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    { console.log(page_number);
                       res.status(200).json({"error":0,"documents":rows[0] ,"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
})


admin.post('/get__all_export_documents',function(req,res){
    let search_text=(req.body.search_text===undefined)?"":req.body.search_text;
    let from_date=(req.body.from_date===undefined)?"":req.body.from_date;
    let to_date=(req.body.to_date===undefined)?"":req.body.to_date;
    let page_size=(req.body.page_size===undefined)?10:req.body.page_size;
    let page_number=(req.body.page_number===undefined)?1:req.body.page_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_all_export_documents(?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            connection.query(sql,[from_date,to_date,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    { console.log(page_number);
                       res.status(200).json({"error":0,"documents":rows[0] ,"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
})

admin.post('/approve_export_document',function(req,res){    
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            console.log(req.body.shipment_document_id);           
            var sql="CALL "+config.dbname+".usp_approve_export_rebate(?,?)";
            connection.query(sql,[req.body.shipment_id,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    res.status(200).json({"error":0,"data":"Approved"});
                }
            });
            connection.release();
        }
    });
})

admin.post('/reject_export_document',function(req,res){    
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            console.log(req.body.shipment_document_id);           
            var sql="CALL "+config.dbname+".usp_reject_export_rebate(?,?,?)";
            connection.query(sql,[req.body.shipment_id,req.body.reject_remarks,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    res.status(200).json({"error":0,"data":"Rejected Document"});
                }
            });
            connection.release();
        }
    });
})

admin.post('/get_fibre_qty',function(req,res){       
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_calc_fibre_consumption(?)";
            connection.query(sql,[req.body.shipment_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    { 
                       res.status(200).json({"error":0,"Quantity":rows[0] });
                    }
                }
            });
            connection.release();
        }
    });
})


/*admin.get('/admin_reports',function(req,res){    
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            //console.log(req.body.shipment_document_id);           
            var sql="CALL "+config.dbname+".usp_admin_user_data()";

           connection.query(sql,function(err, rows, fields) {
            
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {

                var sql2 = "CALL "+config.dbname+".usp_product_report()";
               connection.query(sql2,function(err, rows2, fields2) {         

                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {

                
               //console.log(rows2);
                const userdata =  json2Array(rows[0], fields[0])
                const productdata = productArray(rows2[0], fields2[0])
                
                 const buffer =  xlsx.build([{ name: "user_data", data: userdata } , {name: "product_data", data: productdata}]);
                   
                     fs.writeFile('demo.xlsx', buffer, (err) => {
                    if (err) throw err
                    console.log('Done...');

                 })
/*
let contentType = req.headers['content-type'] || 'application/octet';
    let fileName = req.headers['demo.xlsx'];
                 const fileId =  files.create(fileName, contentType, buffer);
        res.status(201).json({fileId: fileId});*/
/*        try{

 const file =   path.join( __dirname ,'../') + 'demo.xlsx';
console.log(file);

   res.status(200).download(file); // Set disposition and send it.
}
catch (err) {
    console.log(err);
  }

               //res.status(200).json({"error":0,"data":buffer});
         

                }
            });           
            
                }
            });
            connection.release();
        }
    });
})*/



const json2Array =  function (result, fields) {
    //console.log(result);
    let out = [];
    let temp = [];
    // Create headers array
    fields.forEach(item => {
       // console.log(item)
        temp.push(item.name)
    });
    // temp array works as column headers in .xlsx file
     out.push(temp)
    
    result.forEach(item => {
        out.push([item.s_no,item.company_name ,item.company_type_name, item.name, item.email,item.phone_number,item.company_address , item.country , item.state , item.city , item.zip])
    })
    return out;
}

const productArray =  function (result, fields) {
    //console.log(result);
    let out = [];
    let temp = [];
    // Create headers array
     fields.forEach(item => {
        //console.log(item)
        temp.push(item.name)
    });
    // temp array works as column headers in .xlsx file
    out.push(temp)
    
    result.forEach(item => {

        //console.log(item);
        out.push([item.s_no,item.product_name ,item.product_type_name, item.product_qty , item.product_uom])
    })
   return out;
}

const stockArray =  function (result, fields) {
    //console.log(result);
    let out = [];
    let temp = [];
    // Create headers array
     fields.forEach(item => {
        //console.log(item)
        temp.push(item.name)
    });
    // temp array works as column headers in .xlsx file
    out.push(temp)
    
    result.forEach(item => {

        //console.log(item);
        out.push([item.Invoice_number,item.product_name ,item.company_name, item.product_type_name ,item.product_qty, item.product_uom , item.lot_number , item.blend_percentage])
    })
   return out;
}

const orderShipmentArray =  function (result, fields) {
    //console.log(result);
    let out = [];
    let temp = [];
    // Create headers array
     fields.forEach(item => {
        //console.log(item)
        temp.push(item.name)
    });
    // temp array works as column headers in .xlsx file
    out.push(temp)
    
    result.forEach(item => {

        //console.log(item);
        out.push([item.order_number,item.buyer_company_name ,item.order_date, item.order_expected_date, item.order_description ,item.order_status, item.supplier_company_name , item.Invoice_number ,item.product_name, item.product_qty_received ,item.product_uom  , item.product_description])
    })
   return out;
}

admin.get('/admin_reports',function(req,res){    
   
    let from_date=(req.body.from_date===undefined)?"":req.body.from_date;
    let to_date=(req.body.to_date===undefined)?"":req.body.to_date;
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
          async.waterfall([
                function(callback) {
                  var sql="CALL "+config.dbname+".usp_admin_user_data()";   
                  connection.query(sql,function(err, rows, fields) {
                        if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                }
                        else{

                            const userdata = json2Array(rows[0], fields[0])
                            callback(null,userdata)
                        }
                            
                    })            
                },
                function(userdata,callback ) {
               var sql="CALL "+config.dbname+".usp_dashboard_get_products_summary";   
                  connection.query(sql,function(err, rows, fields) {
                        if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                }
                        else{
                            
                             const productdata = productArray(rows[0], fields[0])
                            callback(null,userdata,productdata)
                        }
                            
                    })
                },
                function(userdata,productdata, callback ) {
               var sql="CALL "+config.dbname+".usp_dashboard_get_all_stock_products(?,?)";
            connection.query(sql,[from_date,to_date],function(err, rows, fields) {
        
                        if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                }
                        else{
                            
                             const stockdata = stockArray(rows[0], fields[0])
                            callback(null,userdata,productdata,stockdata)
                        }
                            
                    })
                },
                function(userdata,productdata,stockdata,callback ) {
                	console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{")
               var sql="CALL "+config.dbname+".usp_dashboard_get_orders_shipment_summary(?,?)";
            connection.query(sql,[from_date,to_date],function(err, rows, fields) {        
                        if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                }
                        else{
                            
                             const order_shipment_data = orderShipmentArray(rows[0], fields[0])
                            callback(null,userdata,productdata,stockdata , order_shipment_data)
                        }
                            
                    })
                }                
        

            ], function (err, userdata , productdata , stockdata , order_shipment_data) {
                if(err)
                    {  console.log(err);
                    res.json({"error":1,"data":err});
                }
                else{

                   //console.log(stockdata);
                   console.log(order_shipment_data)
                    const buffer = xlsx.build([{ name: "user_data", data: userdata } , {name: "product_data", data: productdata} , {name: "stock_data", data: stockdata} , 
                    							{name: "order_shipment_data", data: order_shipment_data}]);
                    fs.writeFile('demo.xlsx', buffer, (err) => {
                    if (err) throw err
                    console.log('Done...');
                    try{

                    const file =   path.join( __dirname ,'../') + 'demo.xlsx';
                    /*console.log(file);*/
                       res.status(200).download(file); // Set disposition and send it.
                    }
                    catch (err) {
                        console.log(err);
                      }
                 })
                }
            });
          connection.release();
        }
    });
})
function get_role(company_key_id){
    return new Promise((resolve,reject)=>{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(Date()+JSON.stringify(err));
            } else {
                var sql="CALL "+config.dbname+".usp_get_company_role_ids(?)";
                connection.query(sql,[company_key_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(Date()+JSON.stringify(err));
                        reject(err);
                    }else if(rows[0].length!==0){
                        resolve(rows[0]);
                    }else{
                        reject(err);
                    }
                });
                connection.release();
            }
        });
    })
}

module.exports = admin;
function get_role(company_key_id){
    return new Promise((resolve,reject)=>{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(Date()+JSON.stringify(err));
            } else { 
                var sql="CALL "+config.dbname+".usp_get_company_role_ids(?)";
                connection.query(sql,[company_key_id],function(err, rows, fields) {
                    if (err) {
                        logger.error(Date()+JSON.stringify(err));
                        reject(err);
                    }else if(rows[0].length!==0){
                        resolve(rows[0]);
                    }else{
                        reject(err);
                    }
                });
                connection.release();
            }
        });
    })
}