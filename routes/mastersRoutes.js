var express = require('express');
const xlsx = require("node-xlsx");
var masters = express.Router();
const fs = require('fs');
const path= require('path');
var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');
var admin=require('../routes/admin');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var logger=require('../controllers/logger');
var orderController=require('../controllers/orderController');
var token;
var bcrypt = require('bcrypt');
var config=require('../config/config');
var flash = require('express-flash');
masters.use(flash());

masters.use(cors());

var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";
var resMsg=require('../config/resMessage');
masters.use(function(req, res, next) {
    var token = req.body.token || req.headers['token'];
    console.log("token="+token);
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, function(err,dt) {
            console.log(err);
            if (err) {
                res.status(200).json({"error":5,"data":resMsg.tokenError});
            } else {
                console.log(dt);
                req.session.usrData=dt;
                next();
            }
        })
    } else { 
        res.status(403).json({"error":1,"data":resMsg.emptyToken});
    }
});

//Use the below route for selecting the supplier type. This needs to be done while creating an order from IP. After selecting the type , only the crelevant suppliers will be shown 
masters.get('/get_supplier_type',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
        	console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
              connection.query("CALL "+config.dbname+".usp_get_supplier_type("+req.session.usrData.company_key_id+")",function(err, rows, fields) {
                if (err) {
                	console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});


masters.get('/get_supplier_companies',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));            
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_supplier_companies("+req.session.usrData.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
});
masters.get('/get_all_brands',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));            
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_all_brands()";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log("get all brands");
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
});
masters.get('/get_buyer_companies',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
        	console.log(req.body);
        	console.log(req.query);
            console.log(req.query.company_type_id);
            var sql="CALL "+config.dbname+".usp_get_buyer_companies("+req.session.usrData.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    
                    if(rows==="undefined" || rows[0].length===0)
                    {  console.log("__________________________________________");
                    	console.log(req.session.usrData);
                        res.status(200).json({"error":0,"data":[]});
                    }
                    else
                    {  
                        res.status(200).json({"error":0,"data":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
});
masters.get('/get_product_category',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));     
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            connection.query("CALL "+config.dbname+".usp_get_product_category",function(err, rows, fields) {
                if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        appData["error"]=1;
                        appData["data"]="Sql error";
                        res.json(appData);
                } else {
                    if(rows==="undefined")
                      {  appData["error"]=1;
                        appData["data"]="No such records";
                        res.json(appData);}
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});
masters.get('/get_product_type',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            
            appData["error"] = 1;
            appData["data"] = "Internal Server Error";
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query("CALL "+config.dbname+".usp_get_product_type",function(err, rows, fields) {
                if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_company_bu',function(req,res){
    var company_id=(req.query.buyer_company_key_id===undefined)?req.session.usrData.company_key_id:req.query.buyer_company_key_id;
    console.log("***********************");
    console.log('company_id',company_id);
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            console.log(company_id);
            connection.query("CALL "+config.dbname+".usp_get_company_bu (?)",[company_id], function(err, rows, fields) {
                if (err) {
                    console.log("_____________________________");
                    console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        appData["error"]=1;
                        appData["data"]="Sql error";
                        res.json(appData);
                } else {
                    if(rows==="undefined")
                      {  appData["error"]=1;
                        appData["data"]="No such records";
                        res.json(appData);}
                    else
                    {   console.log(rows[0]);
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});


masters.get('/get_products',function(req,res){

var company_key_id=(req.body.supplier_company_key_id===undefined)?req.session.usrData.company_key_id:req.body.supplier_company_key_id;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
              
            connection.query("CALL "+config.dbname+".usp_get_products (?)",[company_key_id], function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});


masters.post('/get_products_by_role_id',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
        	console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
             var sql="CALL "+config.dbname+".usp_get_products_by_role_id (?,?)"; 
            connection.query(sql,[req.body.company_role_id,req.session.usrData.company_key_id],function(err, rows, fields) {
                if (err) {
                	console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_product_by_type_id',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
         
  connection.query("CALL "+config.dbname+".usp_get_products_by_type_id("+req.query.product_type_id+")",function(err, rows, fields) {

                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});


masters.get('/get_ext_products',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
            connection.query("CALL "+config.dbname+".usp_get_products("+req.query.company_key_id+")",function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_shipment_products',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
            console.log("____________________________");
            console.log(req.session.usrData);
            console.log(req.session.usrData.company_key_id);
            connection.query("CALL "+config.dbname+".usp_get_shipment_products("+req.session.usrData.company_key_id+")",function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_shipment_products_catalogue',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
            console.log("____________________________");
            console.log(req.session.usrData);
            console.log(req.session.usrData.company_key_id);
            connection.query("CALL "+config.dbname+".usp_get_shipment_products_catalogue("+req.session.usrData.company_key_id+")",function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        //res.json(rows[0]);
                    const productdata = getproductArray(rows[0], fields[0])  
                    const buffer = xlsx.build([{ name: "product_data", data: productdata } ]);
                    fs.writeFile('shipment_product_catalogue.xlsx', buffer, (err) => {
                    if (err) throw err
                    console.log('Done...');
                    try{

                    const file =   path.join( __dirname ,'../') + 'shipment_product_catalogue.xlsx';
                    /*console.log(file);*/
                       res.status(200).download(file); // Set disposition and send it.
                    }
                    catch (err) {
                        console.log(err);
                      }
                 })
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_order_products_catalogue',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
            console.log("____________________________");
            console.log(req.session.usrData);
            console.log(req.session.usrData.company_key_id);
            connection.query("CALL "+config.dbname+".usp_get_order_products_catalogue("+req.session.usrData.company_key_id+")",function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        //res.json(rows[0]);
                    const productdata = getproductArray(rows[0], fields[0])  
                    const buffer = xlsx.build([{ name: "product_data", data: productdata } ]);
                    fs.writeFile('order_product_catalogue.xlsx', buffer, (err) => {
                    if (err) throw err
                    console.log('Done...');
                    try{

                    const file =   path.join( __dirname ,'../') + 'order_product_catalogue.xlsx';
                    /*console.log(file);*/
                       res.status(200).download(file); // Set disposition and send it.
                    }
                    catch (err) {
                        console.log(err);
                      }
                 })
                    }
                }
            });
            connection.release();
        }
    });
});


masters.get('/get_supplier_companies_catalogue',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));            
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_supplier_companies("+req.session.usrData.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        const suppliercompanydata = getcompanyArray(rows[0], fields[0])  
                    const buffer = xlsx.build([{ name: "supplier_company_data", data: suppliercompanydata } ]);
                    fs.writeFile('supplier_company_data.xlsx', buffer, (err) => {
                    if (err) throw err
                    console.log('Done...');
                    try{

                    const file =   path.join( __dirname ,'../') + 'supplier_company_data.xlsx';
                    /*console.log(file);*/
                       res.status(200).download(file); // Set disposition and send it.
                    }
                    catch (err) {
                        console.log(err);
                      }
                 })
                    }
                }
            });
            connection.release();
        }
    });
});
masters.get('/get_buyer_companies_catalogue',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
        	console.log(req.body);
        	console.log(req.query);
            console.log(req.query.company_type_id);
            var sql="CALL "+config.dbname+".usp_get_buyer_companies("+req.session.usrData.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    
                    if(rows==="undefined" || rows[0].length===0)
                    {  console.log("__________________________________________");
                    	console.log(req.session.usrData);
                        res.status(200).json({"error":0,"data":[]});
                    }
                    else
                    {  
                       const buyercompanydata = getcompanyArray(rows[0], fields[0])  
                    const buffer = xlsx.build([{ name: "buyer_company_data", data: buyercompanydata } ]);
                    fs.writeFile('buyer_company_data.xlsx', buffer, (err) => {
                    if (err) throw err
                    console.log('Done...');
                    try{

                    const file =   path.join( __dirname ,'../') + 'buyer_company_data.xlsx';
                    /*console.log(file);*/
                       res.status(200).download(file); // Set disposition and send it.
                    }
                    catch (err) {
                        console.log(err);
                      }
                 })
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_uom',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
            let sql="CALL "+config.dbname+".usp_get_uom(?,?)";
            connection.query(sql,[req.session.usrData.company_key_id,req.query.type],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.status(200).json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_neworders_count',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));            
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_neworders_count("+req.session.usrData.company_key_id+",@count);SELECT @count as count";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"count":rows[rows.length-1][0]['count']});
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_newshipment_count',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));            
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_newshipment_count("+req.session.usrData.company_key_id+",@count);SELECT @count as count";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"count":rows[rows.length-1][0]['count']});
                    }
                }
            });
            connection.release();
        }
    });
});
masters.post('/update_order_read',function(req,res){
     
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_update_order_read(?)";
            connection.query(sql,[req.body.order_id],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                     res.status(200).json({"error":1,"data":"Sql error"})
                } else {
                   res.status(200).json({"error":0,"data":"Count updated successfully"})
                }
            });
            connection.release();
        }
    });
});

masters.post('/update_shipment_read',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_update_shipment_read(?)";
            connection.query(sql,[req.body.shipment_id],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                     res.status(200).json({"error":1,"data":"Sql error"})
                } else {
                   res.status(200).json({"error":0,"data":"Count updated successfully"})
                }
            });
            connection.release();
        }
    });
});


masters.get('/actual_process_loss',function(req,res){
    let search_text=(req.query.search_text===undefined)?"":req.query.search_text;
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
    let page_size=(req.query.page_size===undefined)?10:req.query.page_size;
    let page_number=(req.query.page_number===undefined)?1:req.query.page_number;
    var sql="CALL "+config.dbname+".usp_get_actual_process_loss(?,?,?,?,?,@total_recordcount); SELECT @total_recordcount as total_recordcount";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[search_text,from_date,to_date,page_size,page_number], function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                    if(rows===undefined)
                      {  
                        res.status(200).json({"error":1,"data":"No such records"});}
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
});

masters.get('/get_birla_fibre',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            
            appData["error"] = 1;
            appData["data"] = "Internal Server Error";
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query("CALL "+config.dbname+".usp_get_birla_fibre",function(err, rows, fields) {
                if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});



const getproductArray =  function (result, fields) {
    //console.log(result);
    let out = [];
    let temp = [];
    // Create headers array
     fields.forEach(item => {
        //console.log(item)
        temp.push(item.name)
    });
    // temp array works as column headers in .xlsx file
    out.push(temp)
    
    result.forEach(item => {

        //console.log(item);
        out.push([item.product_name ,item.product_type_name])
    })
   return out;
}

const getcompanyArray =  function (result, fields) {
    //console.log(result);
    let out = [];
    let temp = [];
    // Create headers array
     fields.forEach(item => {
     	console.log(item.name)
       if (item.name == 'company_name' ||item.name == 'company_type_name' ){
        temp.push(item.name)
    }
    });
    // temp array works as column headers in .xlsx file
    out.push(temp)
    
    result.forEach(item => {

        //console.log(item);
        out.push([item.company_name ,item.company_type_name])
    })
   return out;
}

module.exports = masters