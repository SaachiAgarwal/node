var express = require('express');
var users = express.Router();
var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');
var config=require('../config/config');
var async = require('async');
var crypto = require('crypto');
var token;
var bcrypt = require('bcrypt');
var logger=require('../controllers/logger');
var flash = require('express-flash');
users.use(flash());
var emailController=require('../controllers/emailController')
users.use(cors());

var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";
users.post('/register', function(req, res) {
    req.checkBody('first_name').exists().withMessage('First Name can not be blank');
    req.checkBody('last_name').exists().withMessage('Last Name can not be blank');
    req.checkBody('email').exists().withMessage('Email can not be blank').isEmail().withMessage('Invalid email');
    req.checkBody('password').exists().withMessage('Password can not be blank');
    req.checkBody('company_key_id').exists().withMessage('Company key id is blank');
    var today = new Date();
    var salt = bcrypt.genSaltSync(10);
    console.log(salt);
    if(req.validationErrors())
    {
        res.status(200).json(req.validationErrors());
    }else{
        var hash = bcrypt.hashSync(req.body.password, salt);
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(Date()+JSON.stringify(err));
                res.status(201).json({"error":1,"data":"Internal Server Error"});
            } else {
                var sql="SET @b_result = 0; CALL "+config.dbname+".usp_create_user(?,?,?,?,?,?,?,@b_result);SELECT @b_result as b_result";
                connection.query(sql,[req.body.first_name,req.body.last_name,req.body.email,req.body.phone_number,hash,req.body.company_key_id, req.body.notify_usersemail],function(err,rows){
                    if(err)
                    {
                        res.status(200).json({"error":1,"data":"Sql error"});
                        logger.error(Date()+JSON.stringify(err));
                    }    
                    else{
                        logger.debug(Date()+JSON.stringify(rows));
                        if(rows[2][0]['b_result']===2){
                            console.log(rows);
                            res.status(200).json({"error":1,"data":"User already exists"});
                        }
                        else if(rows[2][0]['b_result']===1)

                        {
                            res.status(200).json({"error":0,"data":"User registered"});
                            let data={}
                            data.companyData={};
                            data.companyData.recv_email=req.body.email;
                            data.email=req.body.email;
                            data.phone_number=req.body.phone_number;
                            data.password=req.body.password;
                            data.user_create=true;
                            data.first_name=req.body.first_name;
                            data.subject="Account created";
                             emailController.send_email(data,function(result){
                            //console.log('result',result);
                        })
                        }
                        else
                            res.status(200).json({"error":1,"data":"Some problem occured"});
                        connection.release();
                    }
                });
            }
        });
    }
});
users.post('/forgot', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/forgot');
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport('SMTP', {
        service: 'SendGrid',
        auth: {
          user: '!!! YOUR SENDGRID USERNAME !!!',
          pass: '!!! YOUR SENDGRID PASSWORD !!!'
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Node.js Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});


users.get('/getUsers', function(req, res) {
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query('SELECT *FROM users', function(err, rows, fields) {
                if (!err) {
                    res.status(200).json({"error":0,"data":rows[0]});
                } else {
                    logger.error(Date()+JSON.stringify(err));
                    res.status(204).json({"error":1,"data":"No data found"});
                }
            });
            connection.release();
        }
    });
});
users.get('/get_company_type',function(req,res){
var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query("CALL "+config.dbname+".usp_get_company_types()",function(err, rows, fields) {
                if (err) {
                    logger.error(Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});
users.get('/get_company',function(req,res){
    var appData={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query("CALL "+config.dbname+".usp_get_companies_by_companytype("+req.query.company_type_id+")", function(err, rows, fields) {
                if (err) {
                    logger.error(Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {  
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});
users.post('/test',function(req,res){
    let data={};
    data.pass_reset="reset password";
    data.companyData={};
    data.companyData.recv_email=req.body.email;
    data.subject="Reset password"
    data.link="<a href=https://google.co.in>click here</a>";
    emailController.send_email(data,function(result){
        res.json(result);
    })
});
users.post('/forgot_password',function(req,res){
    let data={};
    req.checkBody('email').exists().withMessage('Email can not be blank');
    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{ 
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(Date()+JSON.stringify(err));
                res.status(201).json({"error":1,"data":"Internal Server Error"});
            }else{
                async.waterfall([
                    function(callback) {
                        connection.query('SELECT password,first_name FROM users WHERE email = ?', [req.body.email], function(err, rows, fields){
                            logger.error(Date()+JSON.stringify(err));
                            if(err || rows.length===0 || rows.length===undefined)
                                callback("User id not found",null);
                            else
                                callback(null,rows[0])
                        })            
                    },
                    function(arg2, callback) {
                        console.log('arg2',arg2);
                        let str=Math.random().toString(36).substring(4, 11);
                        data.pass_reset="reset password";
                        data.companyData={};
                        data.companyData.recv_email=req.body.email;
                        data.password=str;
                        data.first_name=arg2.first_name;
                        data.subject="Password reset information"
                        let sql="UPDATE grasim.users SET password=? WHERE email=?";
                        let salt = bcrypt.genSaltSync(10);
                        console.log(salt);
                        let hash = bcrypt.hashSync(str, salt);
                        console.log(sql);
                        connection.query(sql,[hash,req.body.email],function(err,rows,fields){
                            logger.error(Date()+JSON.stringify(err));
                            if(err)
                                callback("Some problem occured",null);
                            else
                            {
                                callback(null,{"error":0,"data":"User updated successfully"});
                            }
                        });
                    }
                ], function (err, result) {
                    connection.release();
                    if(err)
                        res.status(200).json({"error":1,"data":err});
                    else
                    {
                        emailController.send_email(data,function(result){
                            if(result.error===1)
                                res.status(200).json({"error":1,"data":"Unable to send email! Please try again"});
                            else
                                res.status(200).json({"error":0,"data":"Please check your email"});
                        })
                    }
                });
            }
        }); 
    }
})
users.post('/change_password',function(req,res){
    req.checkBody('user_id').exists().withMessage('User id can not be blank');
    req.checkBody('old_password').exists().withMessage('Old password can not be blank');
    req.checkBody('new_password').exists().withMessage('New password can not be blank');
    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{ 
        database.connection.getConnection(function(err, connection) {
            if (err) {
                res.status(201).json({"error":1,"data":"Internal Server Error"});
            }else{
                async.waterfall([
                    function(callback) {
                        connection.query('SELECT password FROM users WHERE user_id = ?', [req.body.user_id], function(err, rows, fields){
                            if(err || rows.length===0 || rows.length===undefined)
                                callback("User id not found",null);
                            else
                                callback(null,rows[0])
                        })            
                    },
                    function(arg1,callback) {
                        if (bcrypt.compareSync(req.body.old_password, arg1.password)) {
                            callback(null,arg1);
                        } else {
                            callback("Wrong password",null)
                        }
                    },
                    function(arg2, callback) {
console.log("changing password");
                        let sql="UPDATE grasim.users SET password=? WHERE user_id=?";
                        let salt = bcrypt.genSaltSync(10);
                        console.log(salt);
                        let hash = bcrypt.hashSync(req.body.new_password, salt);
                        console.log(sql);
                        connection.query(sql,[hash,req.body.user_id],function(err,rows,fields){
                            if(err)
                                callback("Unable to update password",null);
                            else
                            {
                                callback(null,{"error":0,"data":"User updated successfully"});
                            }
                        });
                    }
                ], function (err, result) {
                    connection.release();
                    if(err)
                        res.json({"error":1,"data":err});
                    else
                        res.json(result);
                });
            }
        }); 
    }       
})
module.exports = users;
