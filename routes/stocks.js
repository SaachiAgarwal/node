var express = require('express');
var cors = require('cors');
var bodyParser = require("body-parser");
var session=require("express-session");
var shipment = express.Router();
var jwt = require('jsonwebtoken');
var async = require('async');
var database = require('../Database/database');
var shipmentController=require('../controllers/shipmentController');
var logger=require('../controllers/logger');
var emailController=require('../controllers/emailController');
shipment.use(cors());
var config=require('../config/config');
var token;
var fs = require('fs');
var bcrypt = require('bcrypt');
var audit=require('../controllers/audit');
var flash = require('express-flash');

const path       = require('path');
shipment.use(flash());

shipment.use(cors());

var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";
var session={};
var resMsg=require('../config/resMessage');
shipment.use(function(req, res, next) {
    var token = req.body.token || req.headers['token'];
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, function(err,dt) {
            console.log(err);
            if (err) {
                res.status(200).json({"error":5,"data":resMsg.tokenError});
            } else {
                req.session.usrData=dt;
                next();
            }
        })
    } else { 
        res.status(403).json({"error":1,"data":resMsg.emptyToken});
    }
});
shipment.post('/create_shipment',function(req,res){
    req.checkBody('invoice_number').exists().withMessage('Invoice No. cant be blank');
    req.checkBody('invoice_date').exists().withMessage('Invoice date cant be blank');
 /*   req.checkBody('shipment_transport_mode').exists().withMessage('Transport mode cant be blank'); */ 
    /*req.checkBody('buyer_company_key_id').exists().withMessage('Buyer company key cant be blank');*/
   /* req.checkBody('buyer_company_bu_id').exists().withMessage('Buyer company business unit id is blank');
    req.checkBody('seller_company_bu_id').exists().withMessage('Seller company business unit id is blank');*/

    if(req.validationErrors()){
        res.status(201).json({"error":1,"data":req.validationErrors()});
    }else{
        var shipment_id=""; 
        var shipment_product_id="";
        if(req.body.shipment_id!==undefined)
        shipment_id=req.body.shipment_id;   
        shipment_product_id = req.body.shipment_product_id;
        shipmentController.check_duplicate_shipment(req.session.usrData.company_key_id,req.body.invoice_number,req.body.shipment_id,function(result){
            if(!result.error)
            {
                if(result.duplicate){
                    res.status(200).json({"error":1,"data":"Invoice number already exists"});
                }else{
		    var linked_forest_key_id=req.body.linked_forest_key_id===undefined?'':req.body.linked_forest_key_id;
                    var shipment_description=(req.body.shipment_description===undefined)?'':req.body.shipment_description;
                    var sql="SET @shipment_id = 0; CALL "+config.dbname+".usp_create_shipment(?,?,?,?,?,?,?,?,?,?,@shipment_id); SELECT @shipment_id as shipment_id";
                    var appData={};
                    database.connection.getConnection(function(err, connection) {
                        if (err) {
                            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                            res.status(200).json({"error":1,"data":"Internal server error"});
                        } else {
                                    connection.query(sql,[req.body.invoice_number,req.body.invoice_date,req.body.shipment_transport_mode,shipment_description,req.body.buyer_company_key_id,req.session.usrData.company_key_id,req.body.buyer_company_bu_id,req.body.seller_company_bu_id,linked_forest_key_id,req.session.usrData.user_id],function(err, rows) {
                                console.log(err);
                                if (!err) {
                                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                                    res.status(200).json({"error":0,"data":rows[2][0]});
                                }else {
                                	console.log(err);
                                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                                    res.status(400).json({"error":1,"data":"Sql error"});
                                }
                            });
                            connection.release();
                        }
                    }); 
                }
            }else{
                res.status(200).json({"error":1,"data":"Internal server error"});
            }
        });
    }
});

shipment.post('/check_duplicate_invoice',function(req,res){
    shipmentController.check_duplicate_shipment(req.session.usrData.company_key_id,req.body.invoice_number,req.body.shipment_id,function(result){
        if(!result.error)
        {
            if(result.duplicate){
                res.json({"msg":"Invoice number already exists"});
            }else{                
              res.status(200).json({"error":0,"data":"Not duplicate"});  
            }
        }else{
            res.json(result);
        }
    });
});

shipment.post('/update_shipment',function(req,res){
var linked_forest_key_id=req.body.linked_forest_key_id===undefined?'':req.body.linked_forest_key_id;
    shipmentController.check_duplicate_shipment(req.session.usrData.company_key_id,req.body.invoice_number,req.body.shipment_id,function(result){
        if(!result.error)
        {
            if(result.duplicate){
                res.json({"msg":"Invoice number already exists"});
            }else{                
                var sql="SET @b_result=0;CALL "+config.dbname+".usp_update_shipment(?,?,?,?,?,?,?,?,?,?,?,@b_result); SELECT @b_result as b_result";
                database.connection.getConnection(function(err, connection) {
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(500).json({"error":1,"data":"Internal server error"});
                    } else {
                        connection.query(sql,[req.body.shipment_id,req.body.invoice_number,req.body.invoice_date,req.body.shipment_transport_mode,req.body.shipment_description,req.body.buyer_company_key_id,req.session.usrData.company_key_id,req.body.buyer_company_bu_id,req.body.seller_company_bu_id,linked_forest_key_id,req.session.usrData.user_id], function(err, rows) {
                            if (!err) {
                            res.status(201).json({"error":0,"data":" Shipment updated successfully!"});
                            } else {
                                console.log(err);
                                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                                res.status(200).json({"error":1,"data":"Sql error"});
                            }
                        });
                        connection.release();
                    }
                });
            }
        }else{
            res.json(result);
        }
    });
});

shipment.post('/incoming_shipment',function(req,res){
    var procedure=".usp_get_incoming_shipments" ;   
    shipmentController.get_shipment(req.session.usrData.company_key_id,req.body,procedure ,function(result) {
        res.status(200).json(result);
    }); 
});



shipment.post('/download_incoming_shipment',function(req,res){
    var appData={};

    let db_param=req.body.db_param===undefined?'':req.body.db_param;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    //console.log(db_param);
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_incoming_shipments_download (?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            //console.log(sql);
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,db_param],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });
});


shipment.post('/outgoing_shipment',function(req,res){
    var procedure=".usp_get_outgoing_shipments" ;   
    shipmentController.get_shipment(req.session.usrData.company_key_id,req.body,procedure ,function(result) {
        res.status(200).json(result);
    }); 
});




shipment.post('/download_outgoing_shipment',function(req,res){
    var appData={};
    let db_param=req.body.db_param===undefined?'':req.body.db_param;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            appData["error"] = 1;
            appData["data"] = "Internal server error";
            res.status(500).json(appData);
        } else {
            var sql="CALL "+config.dbname+".usp_get_outgoing_shipments_download (?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,db_param],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(500).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined" || rows[0]===undefined)
                    {
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                    }
                }
                connection.release();
            });
            
        }
    });
});

shipment.get('/view_shipment',function(req,res){
    /*var sql="CALL "+config.dbname+".usp_view_shipment("+req.query.shipment_id+")";
    var dataObj={};
    database.connection.getConnection(function(err, connection) {
        if (err) {
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql, function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    res.json({"error":1,"data":"Sql error"});
                }else {
                    if(rows===undefined ||rows[0].length===0)
                    {  
                        res.json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        dataObj.shipmentDetail=rows[0][0];
                        var sql="CALL "+config.dbname+".usp_get_shipment_products_by_shipmentid("+req.query.shipment_id+")";
                        connection.query(sql, function(err, rows, fields) {
                            if (err) {
                                    res.json({"error":1,"data":"Sql error"});
                            }else {
                                if(rows===undefined)
                                {  
                                    res.json({"error":1,"data":"No such record"});
                                }
                                else
                                {
                                    dataObj.products=rows[0];
                                    res.status(200).json(dataObj);
                                }
                            }
                        });
                    }
                    connection.release();
                }
            });
        }
    });      */
    shipmentController.view_shipment(req.query.shipment_id,function(result){
        /*console.log(result);*/
       /* if(result.error)
            res.status(200).json({"error":1,result});
        else*/
            res.status(200).json(result);
    })

});

shipment.post('/view_stocks',function(req,res){
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let page_size=req.body.page_size===undefined?10:req.body.page_size;
    let page_number=req.body.page_number===undefined?1:req.body.page_number;
    var sql="CALL "+config.dbname+".usp_get_stock_lots(?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                    
                    if(rows===undefined ||rows[0]===undefined)
                      {  
                        res.json({"error":1,"data":"No such records"});}
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],'total_recordcount':rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });      
});


shipment.post('/test_Sh',function(req,res){
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
     var db_param = req.body.db_param===undefined?"":req.db_param;
    let page_size=req.body.page_size===undefined?10:req.body.page_size;
    let page_number=req.body.page_number===undefined?1:req.body.page_number;
    var sql="CALL "+config.dbname+".usp_get_outgoing_shipments(2,'','','','',10,2,@total_recordcount);SELECT @total_recordcount as total_recordcount";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           // connection.query(sql,[2,'','','','',10,2],function(err, rows, fields) {
            console.log(sql);
                 connection.query(sql,function(err, rows, fields) {
                   
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                    
                    if(rows===undefined ||rows[0]===undefined)
                      {  
                        res.json({"error":1,"data":"No such records"});}
                    else
                    {

                    	//console.log(arraySort(rows[0], 'created_date'));
                    /*	var sort2 = rows[0].data.sort(function(a, b){return a.created_date - b.created_date}); 
                    	console.log(sort2)*/
                        res.status(200).json({"error":0,"data":rows[0],'total_recordcount':rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });      
});

shipment.post('/auto_order',function(req,res){
   let order_number=req.body.order_number===undefined?'':req.body.order_number;
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
            console.log(order_number);
            var sql="CALL "+config.dbname+".usp_generate_shipment_auto_order(?,?,?,@b_result,@auto_order_number);SELECT @b_result as b_result;SELECT @auto_order_number as auto_order_number;"; 
            connection.query(sql,[req.body.shipment_id,order_number,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    /*console.log("auto order");
                    console.log(rows);*/
                    res.status(200).json({"error":0,"auto_order_number": rows[rows.length-1][0]['auto_order_number']});
                }
            });
            connection.release();
        }
    });
});


shipment.post('/add_shipment_product',function(req,res){
    req.checkBody('shipment_id').exists().withMessage('shipment id cant be blank');
   // req.checkBody('product_lot_number').exists().withMessage('Lot number cant be blank');
    req.checkBody('product_id').exists().withMessage('Product id cant be blank');
    var shipment_product_id=(req.body.shipment_product_id===undefined)?'':req.body.shipment_product_id;
    var product_uom=(req.body.product_uom===undefined)?'':req.body.product_uom;
    var product_qty=(req.body.product_qty===undefined)?'':req.body.product_qty;
    var product_description=(req.body.product_description===undefined)?'':req.body.product_description;
    var blendpercentage=(req.body.blend_percentage===undefined)?100:req.body.blend_percentage;
    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{

        shipmentController.check_duplicate_lot(req.body.shipment_id,req.body.product_lot_number,shipment_product_id,function(result){
            /*console.log('route ',result);*/
            if(!result.error)
            {
                if(result.duplicate){
                    res.status(200).json({"error":1,"data":"Lot number already exists"});
                }else{
                    /*console.log('route add_product')*/
                    shipmentController.add_product(req.body.shipment_id,req.body.product_lot_number,req.body.product_id,product_description,product_qty,product_uom,blendpercentage,req.session.usrData.user_id,function(result){
                        res.status(200).json(result);
                    });
                }
            }else{
                logger.error(req.session.usrData.email+Date()+JSON.stringify(result));
                res.status(200).json({"error":1,"data":"Internal server error"});
            }        
        });
    }
});

shipment.post('/update_shipment_product',function(req,res){
    req.checkBody('shipment_id').exists().withMessage('shipment id cant be blank');
    req.checkBody('product_lot_number').exists().withMessage('Lot number cant be blank');
    req.checkBody('product_id').exists().withMessage('Product id cant be blank');
    req.checkBody('shipment_product_id').exists().withMessage('Shipment product id cant be blank');
    var product_uom=(req.body.product_uom===undefined)?'':req.body.product_uom;
    var product_qty=(req.body.product_qty===undefined)?'':req.body.product_qty;
    var product_description=(req.body.product_description===undefined)?'':req.body.product_description;
    var blendpercentage=(req.body.blend_percentage===undefined)?100:req.body.blend_percentage;

    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{
        shipmentController.check_duplicate_lot(req.body.shipment_id,req.body.product_lot_number,req.body.shipment_product_id,function(result){
            //console.log('route ',result);
            if(!result.error)
            {
                if(result.duplicate){
                    res.status(200).json({"error":1,"data":"Lot number already exists"});
                }else{
                    
                    shipmentController.update_product(req.body.shipment_id,req.body.shipment_product_id,req.body.product_lot_number,req.body.product_id,product_description,product_qty,product_uom,blendpercentage,req.session.usrData.user_id,function(result){
                        result.shipment_id=req.body.shipment_id;
                        result.shipment_product_id=req.body.shipment_product_id;
                        res.status(200).json(result);
                    });
                }
            }else{
                logger.error(req.session.usrData.email+Date()+JSON.stringify(result));
                res.status(200).json({"error":1,"data":"Internal server error"});
            }        
        });
    }
});

shipment.post('/add_shipment_product/lots',function(req,res){
    req.checkBody('shipment_product_id').exists().withMessage('Shipment product id cant be blank');
    req.checkBody('stock_lot_id').exists().withMessage('Stock lot id cant be blank');
    req.checkBody('consume_product_qty').exists().withMessage('Consume product qty cant be blank'); 
    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
                var sql="SET @shipment_productlot_id=0; CALL "+config.dbname+".usp_add_shipment_product_lots (?,?,?,?,?,@shipment_productlot_id); SELECT @shipment_productlot_id as shipment_productlot_id";
                //console.log(sql);
                connection.query(sql,[req.body.shipment_product_id,req.session.usrData.company_key_id,req.body.stock_lot_id,req.body.consume_product_qty,req.session.usrData.user_id], function(err, rows) {          
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.json({"error":1,"data":"Sql error"});
                    } else 
                    { 
                        logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                        res.json({"error":0,"data":rows[2][0]}); 
                    }
                });
                connection.release();
            }
        });
    }
});
shipment.post('/delete_shipment',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @b_result=0; CALL "+config.dbname+".usp_delete_shipment ("+req.body.shipment_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
            //console.log(sql);
            connection.query(sql, function(err, rows) {          
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Shipment deleted"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});

shipment.post('/delete_shipment_product',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @b_result=0; CALL "+config.dbname+".usp_delete_shipment_product ("+req.body.shipment_product_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
            //console.log(sql);
            connection.query(sql, function(err, rows) {  
                //console.log(rows);        
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Product deleted"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});
shipment.post('/delete_shipment_product/lot',function(req,res){
    //console.log('here');
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @b_result=0; CALL "+config.dbname+".usp_delete_shipment_product_lot ("+req.body.shipment_product_lot_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
            //console.log(sql);
            connection.query(sql, function(err, rows) {  
                //console.log(rows);        
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Product lot deleted"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});
shipment.post('/link_shipment',function(req,res){
    var sql="SET @b_result = 0; CALL "+config.dbname+".usp_link_shipment(?,?,?,@b_result); SELECT @b_result";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.body.shipment_id,req.body.order_id,req.session.usrData.user_id],function(err, rows, fields) {
                logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Shipment linked successfully"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"No such record"}); 
                    }
                }
            });
            connection.release();
        }
    });
});
shipment.post('/unlink_shipment',function(req,res){
    var sql="CALL "+config.dbname+".usp_unlink_shipment(?,?,?)";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.body.shipment_id,req.body.order_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined")
                    {  
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":"Shipment unlinked successfully"});
                    }
                }
            });
            connection.release();
        }
    });
});
shipment.get('/shipment_product_lot',function(req,res){
    var sql="CALL "+config.dbname+".usp_get_shipment_product_lots(?)";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.query.shipment_product_id], function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined")
                    {  
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
});

shipment.get('/stocklot_by_id',function(req,res){
    
    var sql="CALL "+config.dbname+".usp_get_stocklot_by_id(?)";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.query.consume_stock_lot_id], function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {
                    if(rows==="undefined")
                    {  
                        res.status(200).json({"error":1,"data":"No such record"});
                    }
                    else
                    {   
                        res.status(200).json({"error":0,"data":rows[0][0]});
                    }
                }
            });
            connection.release();
        }
    });
});

shipment.post('/update_shipment_product/lot',function(req,res){
    req.checkBody('shipment_product_lot_id').exists().withMessage('Shipment product lot id cant be blank');
    req.checkBody('consume_product_qty').exists().withMessage('Consume quantity cant be blank');
    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{    
        //console.log('route add_product')
        shipmentController.update_product_lot(req.body.shipment_product_lot_id,req.body.consume_product_qty,req.session.usrData.user_id,function(result){
            res.status(200).json(result);
        }); 
    }
})

shipment.post('/update_shipment_product_receive_qty',function(req,res){
    req.checkBody('shipment_product_id').exists().withMessage('Shipment product id cant be blank');
    req.checkBody('product_receiveqty').exists().withMessage('Product receive quantity cant be blank');
   
    if(req.validationErrors()){
        res.status(200).json(req.validationErrors());
    }else{    
        //console.log('in product receive route');
         var sql="CALL "+config.dbname+".usp_update_shipment_product_receive_qty (?,?,?,?,@b_result); SELECT @b_result as b_result";
         database.connection.getConnection(function(err, connection) {
         if (err) {     console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(500).json({"error":1,"data":"Internal server error"});
                    }
        else {
                        connection.query(sql,[req.body.shipment_product_id,req.body.product_receiveqty,req.body.product_receiveddescription,req.session.usrData.user_id],function(err, rows) {
                            if(err){
                                console.log(err);
                                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                                res.status(200).json({"error":1,"data":"Sql error"})
                            }else{
                                //console.log(rows);
                                res.status(200).json({"error":0,"data":"Shipment product updated successfully"});
                            }
                            connection.release();
                        });
                    }

           })
    }
})

shipment.post('/cancel_shipment',function(req,res){
    if(req.body.shipment_id===undefined)
        res.status(200).json({"error":1,"data":"Shipment id cant be blank"});
    else{
        var cancel_reason=(req.body.cancel_reason===undefined)?'':req.body.cancel_reason;
        var sql="SET @b_result = 0; CALL "+config.dbname+".usp_cancel_shipment (?,?,?,@b_result);SELECT @b_result";
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":"Internal server error"});
            } else {
                connection.query(sql,[req.body.shipment_id,cancel_reason,req.session.usrData.user_id],function(err, rows) {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(err)
                    {    
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":"Sql error"});
                    }
                    else if(rows[2][0]["@b_result"]===1){
                        res.status(200).json({"error":0,"data":"Shipment cancelled"});
                        let data={};
                        data.user_action=true;
                        data.usrData=req.session.usrData;
                        data.company_key_id=req.session.usrData.company_key_id;
                        data.recv_company_key_id=req.body.recv_company_key_id;
                        data.action="cancelled";
                        data.subject="Shipment cancelled : invoice number-"+req.body.invoice_number;
                        data.procedure=".usp_get_shipment_emailusers";
                        data.id=req.body.shipment_id;
                        data.invoice_number=req.body.invoice_number;
                        data.cancel_reason=req.body.cancel_reason;
                        data.user_type="Buyers";
                        emailController.create_mail(data,function(result){
                            //console.log('result',result);
                        })
                        /*console.log('data in cancel_ordera',data);
                        console.log('data1',data);*/
                    }else{
                        res.status(200).json({"error":1,"data":"Failed cancelling shipment"});
                    }
                    connection.release();
                });
            }
        });
    }
});



shipment.post('/reject_shipment',function(req,res){
	var cancel_reason=(req.body.cancel_reason===undefined)?'':req.body.cancel_reason;
    if(req.body.shipment_id===undefined)
        res.status(200).json({"error":1,"data":"Shipment id cant be blank"});
    else{
        var reject_reason=(req.body.reject_reason===undefined)?'':req.body.reject_reason;
        var sql="SET @b_result = 0; CALL "+config.dbname+".usp_reject_shipment (?,?,?,@b_result);SELECT @b_result";
        database.connection.getConnection(function(err, connection) {
            if (err) {
                console.log(err);
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(200).json({"error":1,"data":"Internal server error"});
            } else {
                connection.query(sql,[req.body.shipment_id,cancel_reason,req.session.usrData.user_id],function(err, rows) {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(err)
                    {    console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.status(200).json({"error":1,"data":"Sql error"});
                    }
                    else if(rows[2][0]["@b_result"]===1){
                        res.status(200).json({"error":0,"data":"Shipment rejected"});
                        let data={};
                        data.user_action=true;
                        data.usrData=req.session.usrData;
                        data.company_key_id=req.session.usrData.company_key_id;
                        data.recv_company_key_id=req.body.recv_company_key_id;
                        data.action="Rejected";
                        data.subject="Shipment rejected : invoice number-"+req.body.invoice_number;
                        data.procedure=".usp_get_shipment_emailusers";
                        data.id=req.body.shipment_id;
                        data.invoice_number=req.body.invoice_number;
                        data.reject_reason=req.body.reject_reason;
                        data.user_type="Buyers";
                        emailController.create_mail(data,function(result){
                            //console.log('result',result);
                        })
                        /*console.log('data in cancel_ordera',data);
                        console.log('data1',data);*/
                    }else{
                        res.status(200).json({"error":1,"data":"Failed rejecting shipment"});
                    }
                    connection.release();
                });
            }
        });
    }
});



shipment.post('/receive_shipment',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @b_result=0; CALL "+config.dbname+".usp_receive_shipment ("+req.body.shipment_id+","+req.session.usrData.user_id+",@b_result); SELECT @b_result";
            //console.log(sql);
            connection.query(sql, function(err, rows) {          
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[2][0]['@b_result']===1)
                    { 
                       res.json({"error":0,"data":"Shipment received"});
                    }
                    else
                    {
                        res.json({"error":1,"data":"Problem in receiving shipment"}); 
                    }
                }
            });
            connection.release();
        }
    });
});
shipment.post('/validate_shipment_qty',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_validate_shipment_qty (?,?,?,@b_error,@b_message,@shipment_product_qty,@expected_consume_product_qty,@consume_productqty); SELECT @b_error as b_error,@b_message as b_message,@shipment_product_qty as shipment_product_qty,@expected_consume_product_qty as expected_consume_product_qty,@consume_productqty as consume_productqty";
            //console.log(sql);
            connection.query(sql,[req.body.shipment_id,req.body.order_id,req.session.usrData.company_key_id],function(err, rows) {          
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[1]!==undefined && rows[rows.length-1][0]['b_error']!==undefined)
                    { 
                       res.status(200).json({"error":0,"data":rows[rows.length-1][0]});
                    }
                    else
                    {
                        res.json({"error":1,"data":"Something went wrong"}); 
                    }
                }
            });
            connection.release();
        }
    });
});

shipment.post('/auto_order_validate_shipment_qty',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
        	console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_validate_auto_order_shipment(?,?,@b_error,@b_message,@shipment_product_qty,@consume_productqty); SELECT @b_error as b_error,@b_message as b_message,@shipment_product_qty as shipment_product_qty,@consume_productqty as consume_productqty";
            //console.log(sql);
            connection.query(sql,[req.body.shipment_id,req.session.usrData.company_key_id],function(err, rows) {          
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                    logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
                    if(rows[1]!==undefined && rows[rows.length-1][0]['b_error']!==undefined)
                    { 
                       res.status(200).json({"error":0,"data":rows[rows.length-1][0]});
                    }
                    else
                    {
                        res.json({"error":1,"data":"Something went wrong"}); 
                    }
                }
            });
            connection.release();
        }
    });
});

/*shipment.post('/generate_pulp_shipment',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @c_result = 0; CALL "+config.dbname+".usp_generate_pulp_shipment(?,?,?,@c_result); SELECT @c_result"; 
            //console.log(sql);
            connection.query(sql,[req.body.shipment_id,req.session.usrData.company_key_id,req.session.usrData.user_id],function(err, rows) {          
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                     res.status(200).json({"error":0,"data":"Shipment placed successfully"});
                }
            });
            connection.release();
        }
    });
});*/

shipment.post('/generate_forest_shipment',function(req,res){
	console.log("*****************************************************************************")
console.log("*****************************************************************************")
console.log(req.body.linked_forest_key_id)
	console.log("*****************************************************************************")

    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(500).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="SET @c_result = 0; CALL "+config.dbname+".usp_generate_forest_shipment(?,?,?,?,@c_result); SELECT @c_result"; 
            //console.log(sql);
            connection.query(sql,[req.body.shipment_id,req.body.linked_forest_key_id,req.session.usrData.company_key_id,req.session.usrData.user_id],function(err, rows) {          
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.json({"error":1,"data":"Sql error"});
                } else 
                {
                     res.status(200).json({"error":0,"data":"Shipment placed successfully"});
                }
            });
            connection.release();
        }
    });
});

//shipment.post('/place_shipment',audit.traceshipment,audit.register_shipment,audit.place_shipment,audit.save_auditto_db,shipmentController.send_email);
shipment.post('/update_blockchain_new',audit.traceorder,audit.register_shipment_update);
shipment.post('/update_blockchain',audit.traceshipment,audit.register_shipment);//,shipmentController.send_email);
shipment.post('/place_shipment',audit.traceshipment,audit.register_shipment,audit.place_shipment,shipmentController.send_email);
shipment.post('/geo_data', audit.getshipmentid, audit.tracegeo);
shipment.post('/get_trace_for_export_rebate' , audit.trace_exportgeo);
shipment.post('/forest_company_keyid', audit.getshipmentid, audit.forest_company);
shipment.post('/test',audit.traceshipment);//,shipmentController.send_email);
/*shipment.post('/geo_data', function(req, res){
    console.log("1212323");
  audit.traceshipment
});*/
//,shipmentController.send_email);
/*shipment.post('/place_shipment',function(req,res,next){

    async.waterfall([
                
                        function(callback){
                        console.log('aaaaaaa');
                         audit.traceshipment(req,res,next,function(err,data){

                             callback(null,'Done'); 
                        });
                         //next();
                         
                }, function(result,callback) {
                     console.log('bbbbb');
                       audit.register_shipment(req,res,next,function(err,data){

                             callback(null,'Done'); 
                        });
                      
                },function(result,callback) {
                    console.log('ccccc');
                    audit.place_shipment(req,res,next,function(err,data){

                             callback(null,'Done'); 
                        });
                      
                      
                },function(result,callback) {
                      console.log('dddd');
                       audit.save_auditto_db(req,res,next,function(err,data){

                             callback(null,'Done'); 
                        });
                      
                },function(result,callback) {
                      console.log('eeeee');
                      audit.send_email(req,res,next,function(err,data){

                             callback(null,'Done'); 
                        });
                      
                }

                ], function (err, result) {
                if(err)
                {   console.log('in next',err); res.status(200).json(err); 
                    if(!err.error)
                    next();
                }
                else
                    {console.log('sssssssss');
                    res.status(200).json({"error":0,"data":"Shipment placed successfully"});
                        ;
                        //console.log(req.body);
                    }

            });


});*/

shipment.post('/auditdb' , audit.save_auditto_db);
shipment.post('/audittrail',audit.audittrail);

shipment.get('/check_shipment_lot',function(req,res,next){
    if(req.query.shipment_id===undefined || req.query.shipment_id=='')
        res.status(200).json({"error":1,"data":"Please send shipment id"});
    else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
                var sql="CALL "+config.dbname+".usp_is_shipment_lotnumber_blank (?,@b_result);SELECT @b_result as b_result;";
                connection.query(sql,[req.query.shipment_id],function(err, rows) {          
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.json({"error":1,"data":"Sql error"});
                    } else 
                    {
                        if(rows[1]!==undefined && rows[rows.length-1][0]['b_result']!==undefined)
                        { 
                           res.status(200).json({"error":0,"data":{'blank_lot':rows[rows.length-1][0]['b_result']}});
                        }
                        else
                        {
                            res.json({"error":1,"data":"Something went wrong"}); 
                        }
                    }
                });
                connection.release();
            }
        });
    }

})
shipment.get('/shipment_lot',function(req,res,next){
  
    //console.log(req.query.shipment_id);
    if(req.query.shipment_id===undefined || req.query.shipment_id=='')
        res.status(200).json({"error":1,"data":"Please send shipment id"});
    else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
                var sql="CALL "+config.dbname+".usp_get_shipment_product_lots_by_shipmentid (?)";
                //console.log("here");
                connection.query(sql,[req.query.shipment_id],function(err, rows) {          
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.json({"error":1,"data":"Sql error"});
                    } else 
                    {
                        if(rows[0]!==undefined)
                        { 
                           res.status(200).json({"error":0,"data":rows[0]});
                        }
                        else
                        {
                            res.json({"error":1,"data":"Something went wrong"}); 
                        }
                    }
                });
                connection.release();
            }
        });
    }
})

shipment.get('/validate_stock',function(req,res){
    if(req.query.shipment_id===undefined || req.query.shipment_id==='')
        res.status(200).jsonn({"error":1,"data":"Please send shipment id"});
    else{
        database.connection.getConnection(function(err, connection) {
            if (err) {
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
                var sql="CALL "+config.dbname+".usp_validate_stock_availability (?,@b_error);SELECT @b_error";
                connection.query(sql,[req.query.shipment_id],function(err, rows) {          
                    if (err) {
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.json({"error":1,"data":"Sql error"});
                    } else 
                    {
                        if(rows[rows.length-1][0]!==undefined)
                        { 
                           res.status(200).json({"error":0,"data":rows[rows.length-1][0]});
                        }
                        else
                        {
                            res.json({"error":1,"data":"Something went wrong"}); 
                        }
                    }
                });
                connection.release();
            }
        });
    }
})

var upload = require('express-fileupload');

shipment.use(upload()); // configure middleware
shipment.get('/upload',function(req,res){
    /*console.log(req.session)
    if(req.session.usrData===undefined)
        res.status(200).json({"error":1,"data":"Plese login"});
    else*/
        res.sendFile(path.join(__dirname,'../views/index.html'));
    
})


shipment.post('/update_audit_display',function(req,res){
     
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_update_audit_display_preference(?,?,?,?,?,?,?,?,?,?,?,?,@b_result); SELECT @b_result as b_result";
            connection.query(sql,[req.session.usrData.company_key_id,req.body.sd_sellercol,req.body.sd_buyercol,req.body.sd_invoice_numbercol,req.body.sd_MoTcol, req.body.sd_shipment_datecol,req.body.sd_received_datecol,req.body.pd_productcol,req.body.pd_quantitycol,req.body.pd_unitcol,req.body.pd_descriptioncol,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                     res.status(200).json({"error":1,"data":"Sql error"})
                } else {
                   res.status(200).json({"error":0,"data":"Data updated successfully"})
                }
            });
            connection.release();
        }
    });
});

shipment.get('/get_audit_display' , audit.audit_display);


shipment.post('/upload',function(req,res){
var document_filename ; 
    /*console.log(req.body.shipmentid);
    console.log(req.body.document_filename);*/
    var documentname;
 /*   console.log('type',req.body.type);
  console.log('files',req.files);
  console.log('upfile',req.files.upfile);*/
  document_filename = req.body.document_filename;
  if(req.files.upfile){
    var file = req.files.upfile,
      documentname = file.name,
      type = file.mimetype;
    var uploadpath =path.join( __dirname ,'../QA_documents/') + document_filename;
    file.mv(uploadpath,function(err){
      if(err){
/*        console.log(err);
        console.log("File Upload Failed",documentname,err);*/
        res.send("Error Occured!")
      }
      else {
       /* console.log("File Uploaded",documentname);
        console.log(uploadpath);     
        console.log(documentname); */  
        var sql="CALL "+config.dbname+".usp_create_shipment_document(?,?,?,?,@bresult); SELECT @bresult as bresult";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.body.shipmentid,documentname,req.body.document_filename,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                	if (rows[1][0].bresult == 2)
                	{    fs.unlinkSync(uploadpath);
                		 res.status(200).json({"error":1,"data":"Error: Document name already exists"});
                	}
                	
                    else {
                    	res.status(200).json({"error":0,"data":"Document uploaded"});
                    }
                }
            });
            connection.release();
        }
    });

      }
    });
  }
  else {
    res.send("No File selected !");
    res.end();
  };
})



shipment.post('/get_documents',function(req,res){
/*console.log(req.body.shipment_id);*/
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_shipment_documents_by_shipmentid("+req.body.shipment_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	console.log("_________________________________________");
                	console.log(rows[0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  console.log("&&&&&&&&&&&&&&&&&&&&&&&")
               			 console.log(rows[0]);
                        res.status(200).json({"error":0,"documents":rows[0]});
                    }
                    else
                    {console.log("**********************")
                    	console.log(rows[0]);
                       res.status(200).json({"error":0,"documents":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})



shipment.post('/download_document',function(req,res){
var document_filename = req.body.document_file_name;
try{

const file = path.join( __dirname ,'../QA_documents/') + document_filename;
/*console.log(file);*/
   res.status(200).download(file); // Set disposition and send it.
}
catch (err) {
    console.log(err);
  }

})

shipment.post('/delete_document',function(req,res){
const file = path.join( __dirname ,'../QA_documents/') + req.body.document_file_name;
//console.log(file);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_shipment_document(?,?)"; 
           connection.query(sql,[req.body.shipment_documentid,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	   fs.unlinkSync(file);                                
                       res.status(200).json({"error":0,"data":"Document deleted"});
                    
                }
            });
            connection.release();
        }
    });
})


shipment.post('/create_ext_vendor_shipment',function(req,res){
    let seller_companytype_name= 'Finished Fabric Manufacturer';
        var ext_vendor_shipmentid=""; 
        var shipment_product_id="";  
        shipment_product_id = req.body.shipment_product_id;
        var shipment_description=(req.body.shipment_description===undefined)?'':req.body.shipment_description;
        var sql="SET @ext_vendor_shipmentid = 0; CALL "+config.dbname+".usp_create_ext_vendor_shipment(?,?,?,?,?,?,?,?,?,?,?,@ext_vendor_shipmentid); SELECT @ext_vendor_shipmentid as ext_vendor_shipmentid";
        var appData={};
        database.connection.getConnection(function(err, connection) {
        if (err) {
         logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
        res.status(200).json({"error":1,"data":"Internal server error"});
         } else {
         /*	console.log("**************************************");
         	console.log(req.body.invoice_date);*/
         connection.query(sql,[req.body.shipment_id,req.body.invoice_number,req.body.invoice_date,req.body.order_number,req.body.shipment_transport_mode,shipment_description,req.session.usrData.company_key_id,req.body.seller_company_key_id,seller_companytype_name,req.body.shipment_date,req.session.usrData.user_id],function(err, rows) {
         console.log(err);
         if (!err) {
            logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
            res.status(200).json({"error":0,"data":rows[2][0]});
            }else {
                console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(400).json({"error":1,"data":"Sql error"});
                                }
                            });
                            connection.release();
                        }
                    });             
 });

shipment.post('/delete_external_shipment',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_ext_vendor_shipment(?,?,?,@b_result); SELECT @b_result as b_result";
           connection.query(sql,[req.body.ext_vendor_shipment_id,req.body.shipment_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {                                  
                       res.status(200).json({"error":0,"data":"External vendor shipment deleted"});
                    
                }
            });
            connection.release();
        }
    });
})


shipment.post('/view_external_shipment',function(req,res){
console.log("_______________________________________");
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_ext_vendor_shipment("+req.body.shipment_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(rows[0]);
                    console.log("***************************");
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    console.log("_______________________________________");
                    console.log(rows[0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"data":"No such records"});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"documents":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})


shipment.post('/view_external_vendor_by_shipment_id',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_ext_vendor_by_vendor_shipmentid("+req.body.ext_vendor_shipment_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    //console.log(rows[0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"vendors":rows[0][0]});
                    }
                }
            });
            connection.release();
        }
    });
})

shipment.post('/view_external_shipment_product_by_product_id',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_ext_vendor_shipment_product_by_productid("+req.body.ext_vendor_shipment_product_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"documents":rows[0][0]});
                    }
                }
            });
            connection.release();
        }
    });
})

shipment.post('/update_ext_vendor_shipment',function(req,res){
 let seller_companytype_name= 'Finished Fabric Manufacturer';
 var sql="SET @b_result=0;CALL "+config.dbname+".usp_update_ext_vendor_shipment(?,?,?,?,?,?,?,?,?,?,?,@b_result); SELECT @b_result as b_result";
 database.connection.getConnection(function(err, connection) {
 if (err) {
 	console.log(err);
  logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
 res.status(500).json({"error":1,"data":"Internal server error"});
 } else {
 connection.query(sql,[req.body.ext_vendor_shipment_id,req.body.shipment_id,req.body.invoice_number,req.body.invoice_date,req.body.order_number,req.body.shipment_transport_mode,req.body.shipment_description,req.body.seller_company_key_id,seller_companytype_name,req.body.shipment_date,req.session.usrData.user_id], function(err, rows) {
 if (!err) {
 res.status(201).json({"error":0,"data":" External vendor shipment updated successfully!"});
 } else {
 	console.log(err);
  logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
	res.status(200).json({"error":1,"data":"Sql error"});
    }
     });
   connection.release();
                    }
                });         
      });


shipment.post('/add_ext_vendor_shipment_product',function(req,res){
    
 
    var ext_product_uom=(req.body.ext_product_uom===undefined)?'':req.body.ext_product_uom;
    var ext_product_qty=(req.body.ext_product_qty===undefined)?'':req.body.ext_product_qty;
    var ext_product_description=(req.body.ext_product_description===undefined)?'':req.body.ext_product_description;
    var ext_blend_percentage=(req.body.ext_blend_percentage===undefined)?100:req.body.ext_blend_percentage;
    var sql="SET @ext_vendor_shipment_product_id = 0; CALL "+config.dbname+".usp_add_ext_vendor_shipment_product(?,?,?,?,?,?,?,?,@ext_vendor_shipment_product_id); SELECT @ext_vendor_shipment_product_id as ext_vendor_shipment_product_id";
    var appData={};
        database.connection.getConnection(function(err, connection) {
        if (err) {
        console.log(err);
        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
        res.status(200).json({"error":1,"data":"Internal server error"});
         } else {
         connection.query(sql,[req.body.ext_vendor_shipment_id,req.body.shipment_id,req.body.product_id,ext_product_description,ext_product_qty,ext_product_uom,ext_blend_percentage,req.session.usrData.user_id],function(err, rows) {
         console.log(err);
         if (!err) {
         	logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
            res.status(200).json({"error":0,"data":rows[2][0]});
            }else {
              console.log(err);
              logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
              res.status(400).json({"error":1,"data":"Sql error"});
              }});
              connection.release();
              }});   
   });

shipment.post('/view_external_shipment_product',function(req,res){
//console.log(req.body.shipment_id);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_ext_vendor_shipment_product("+req.body.ext_vendor_shipment_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"data":"No such records"});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"Products":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})


shipment.post('/update_ext_vendor_shipment_product',function(req,res){
	//console.log(req.body.ext_vendor_shipment_product_id);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
          /*  console.log(":::::::::::::::::::")
            console.log(req.body.ext_vendor_shipment_product_id);
            console.log(req.body.ext_vendor_shipment_id);
            console.log("88888888888888888888888888");*/
           var sql="CALL "+config.dbname+".usp_update_ext_vendor_shipment_product(?,?,?,?,?,?,?,?,@b_result); SELECT @b_result as b_result";
			 connection.query(sql,[req.body.ext_vendor_shipment_product_id,req.body.ext_vendor_shipment_id,req.body.product_id,req.body.ext_product_description,req.body.ext_product_qty,req.body.ext_product_uom,req.body.ext_blend_percentage,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {                                  
                       res.status(200).json({"error":0,"data":"External vendor shipment product updated"});
                    
                }
            });
            connection.release();
        }
    });
})

shipment.post('/delete_external_shipment_product',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_ext_vendor_shipment_product(?,?,@b_result); SELECT @b_result as b_result";
           connection.query(sql,[req.body.ext_vendor_shipment_product_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {                                  
                       res.status(200).json({"error":0,"data":"External vendor shipment product deleted"});
                    
                }
            });
            connection.release();
        }
    });
})

shipment.get('/get_company_detail',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal Server Error"});
        } else {
            let sql="CALL "+config.dbname+".usp_get_uom(?,?)";
            connection.query(sql,[req.session.usrData.company_key_id,req.query.type],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                } else {
                    if(rows==="undefined")
                    {
                        res.status(200).json({"error":1,"data":"No such records"});
                    }
                    else
                    {
                        res.status(200).json(rows[0]);
                    }
                }
            });
            connection.release();
        }
    });
});


//create shipment to external vendor who is not on the shipment.

shipment.post('/create_external_vendor_company',function(req,res){
 
    database.connection.getConnection(function(err, connection) {
        if (err) {
        	console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
           
            console.log(req.body.company_name);
            var sql="CALL "+config.dbname+".usp_create_external_vendor_company(?,?,?,@vendor_company_key_id);SELECT @vendor_company_key_id as vendor_company_key_id;"; 
            connection.query(sql,[req.body.company_name,req.session.usrData.company_key_id,req.session.usrData.user_id],function(err, rows, fields) {
                if (err) {
                	console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                    
                    res.status(200).json({"error":0,"data":rows[0],"vendor_company_key_id":rows[rows.length-1][0]['vendor_company_key_id']});
                }
            });
            connection.release();
        }
    });
});



shipment.post('/certificate_number',function(req,res,next){  
 
        database.connection.getConnection(function(err, connection) {
            if (err) {
                console.log(err);
                logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                res.status(500).json({"error":1,"data":"Internal server error"});
            } else {
 
                var sql="CALL "+config.dbname+".usp_get_shipment_certificate(?,@certificate_number);SELECT @certificate_number as certificate_number;"; 
                connection.query(sql,[req.body.shipment_id],function(err, rows) {          
                    if (err) {
                        console.log(err);
                        logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                        res.json({"error":1,"data":"Sql error"});
                    } else 
                    { console.log(rows[0]);
                        if(rows[0]!==undefined)
                        { 
                           res.status(200).json({"error":0,"data":rows[rows.length-1][0]['certificate_number']});
                        }
                        else
                        {
                            res.json({"error":1,"data":"Something went wrong"}); 
                        }
                    }
                });
                connection.release();
            }
     });
});


//////


shipment.post('/upload_export_rebate_documents',function(req,res){
var document_filename ; 
var docuemntname;
  document_filename = req.body.document_filename;
  if(req.files.upfile){
    var file = req.files.upfile,
      docuemntname= file.name,
      type = file.mimetype;
    var uploadpath =path.join( __dirname ,'../export_rebate/') + document_filename;
    file.mv(uploadpath,function(err){
      if(err){
/*        console.log(err);
        console.log("File Upload Failed",docuemntname,err);*/
        res.send("Error Occured!")
      }
      else {
       /* console.log("File Uploaded",docuemntname);
        console.log(uploadpath);     
        console.log(docuemntname); */  
        var sql="CALL "+config.dbname+".usp_create_export_rebate_document(?,?,?,?,?,@bresult); SELECT @bresult as bresult";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.body.shipmentid,docuemntname,document_filename,req.body.is_bill,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                	if (rows[1][0].bresult == 2)
                	{    fs.unlinkSync(uploadpath);
                		 res.status(200).json({"error":1,"data":"Error: Document name already exists"});
                	}
                	
                    else {
                    	res.status(200).json({"error":0,"data":"Document uploaded"});
                    }
                }
            });
            connection.release();
        }
    });

      }
    });
  }
  else {
    res.send("No File selected !");
    res.end();
  };
})



shipment.post('/get_export_rebate_documents',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_export_document_by_shipmentid("+req.body.shipment_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	console.log("_________________________________________");
                	console.log(rows[0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  console.log("&&&&&&&&&&&&&&&&&&&&&&&")
               			 console.log(rows[0]);
                        res.status(200).json({"error":0,"documents":rows[0]});
                    }
                    else
                    {console.log("**********************")
                    	console.log(rows[0]);
                       res.status(200).json({"error":0,"documents":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})



shipment.post('/download_export_document',function(req,res){
var document_filename = req.body.document_file_name;
try{

const file = path.join( __dirname ,'../export_rebate/') + document_filename;
/*console.log(file);*/
   res.status(200).download(file); // Set disposition and send it.
}
catch (err) {
    console.log(err);
  }

})

shipment.post('/delete_export_rebate_document',function(req,res){
const file = path.join( __dirname ,'../export_rebate/') + req.body.document_file_name;
//console.log(file);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_export_rebate_document(?,?)"; 
           connection.query(sql,[req.body.export_rebate_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	   fs.unlinkSync(file);                                
                       res.status(200).json({"error":0,"data":"Document deleted"});
                    
                }
            });
            connection.release();
        }
    });
})


shipment.post('/submit_export_rebate_documents',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_submit_export_rebate_document(?,?,?,?)"; 
           connection.query(sql,[req.body.shipment_id, req.body.bill_number,req.body.invoice_number,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	                               
                       res.status(200).json({"error":0,"data":"Documents submitted"});
                    
                }
            });
            connection.release();
        }
    });
})



//////


// TRF routes 

//Create a TRF from VC login 

shipment.post('/create_tracer_trf',function(req,res){
   
        var sql="SET @trf_id = 0; CALL "+config.dbname+".usp_create_trf(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@trf_id); SELECT @trf_id as trf_id";
       
        database.connection.getConnection(function(err, connection) {
        if (err) {
         logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
        res.status(200).json({"error":1,"data":"Internal server error"});
         } else {
         connection.query(sql,[req.body.invoice_number,req.body.birla_fibre,req.body.product_type_id,req.body.fabric_order_number,req.body.brand_name,req.body.sample_batch_number,req.body.garmenter , req.body.finished_fabric_manufacturer , req.body.greige_fabric_manufacturer,req.body.spinner , req.body.claimed_composition,req.body.warpweft,req.body.fabricgsm, req.body.courier_id,req.session.usrData.company_key_id,req.session.usrData.user_id],function(err, rows) {
         console.log(err);
         if (!err) {
            logger.debug(req.session.usrData.email+Date()+JSON.stringify(rows));
            res.status(200).json({"error":0,"data":rows[2][0]});
            }else {
                console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(400).json({"error":1,"data":"Sql error"});
                                }
                            });
                            connection.release();
                        }
                    });             
 });

// Get TRF details by TRF ID 

shipment.post('/get_trf_details_by_trf_id',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_tracer_trf_by_tracer_id("+req.body.trf_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"TRF details":rows[0]});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"TRF details ":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})

shipment.post('/get_trf_details_by_courier_id',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_tracer_trf_by_courier_id("+req.body.courier_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"TRF details":rows[0]});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"TRF details ":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})

//Get list of all the TRF samples submitted by the company 

shipment.post('/get_trf_details_by_company_key_id',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_tracer_trf_by_company_id("+req.body.company_key+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"TRF details":rows[0]});
                    }
                    else
                    {
                       res.status(200).json({"error":0,"TRF details ":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})

shipment.post('/get_trf_all',function(req,res){
    let from_date=req.body.from_date===undefined?'':req.body.from_date;
    let to_date=req.body.to_date===undefined?'':req.body.to_date;
    let search_text=req.body.search_text===undefined?'':req.body.search_text;
    let page_size=req.body.page_size===undefined?10:req.body.page_size;
    let page_number=req.body.page_number===undefined?1:req.body.page_number;
    var sql="CALL "+config.dbname+".usp_get_all_tracer_trf_courier(?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount";
    database.connection.getConnection(function(err, connection) {
        if (err) {
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[from_date,to_date,search_text,page_size,page_number],function(err, rows, fields) {
                if (err) {
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                    
                    if(rows===undefined ||rows[0]===undefined)
                      {  
                        res.json({"error":1,"data":"No such records"});}
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0],'total_recordcount':rows[rows.length-1][0]['total_recordcount']});
                    }
                }
            });
            connection.release();
        }
    });      
});

shipment.post('/delete_trf',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_trf(?,?)"; 
           connection.query(sql,[req.body.trf_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	                                 
                       res.status(200).json({"error":0,"data":"TRF deleted"});
                    
                }
            });
            connection.release();
        }
    });
})

shipment.post('/submit_trf',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_submit_trf(?,?)"; 
           connection.query(sql,[req.body.trf_id, req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	                               
                       res.status(200).json({"error":0,"data":"TRF submitted"});
                    
                }
            });
            connection.release();
        }
    });
})

shipment.post('/shortlist_trf',function(req,res){
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_shortlist_trf(?,?)"; 
           connection.query(sql,[req.body.trf_id, req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	                               
                       res.status(200).json({"error":0,"data":"TRF submitted"});
                    
                }
            });
            connection.release();
        }
    });
})


shipment.post('/upload_trf_image',function(req,res){
var document_filename ; 
   
    var documentname;
  document_filename = req.body.document_filename;
  if(req.files.upfile){
    var file = req.files.upfile,
      documentname = file.name,
      type = file.mimetype;
    var uploadpath =path.join( __dirname ,'../Trf/') + document_filename;
    file.mv(uploadpath,function(err){
      if(err){
/*        console.log(err);
        console.log("File Upload Failed",documentname,err);*/
        res.send("Error Occured!")
      }
      else {
       /* console.log("File Uploaded",documentname);
        console.log(uploadpath);     
        console.log(documentname); */  
        var sql="CALL "+config.dbname+".usp_create_trf_document(?,?,?,?,@bresult); SELECT @bresult as bresult";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.body.trf_id,documentname,req.body.document_filename,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else { 
                	if (rows[1][0].bresult == 2)
                	{    fs.unlinkSync(uploadpath);
                		 res.status(200).json({"error":1,"data":"Error: Document name already exists"});
                	}
                	
                    else {
                    	res.status(200).json({"error":0,"data":"Document uploaded"});
                    }
                }
            });
            connection.release();
        }
    });

      }
    });
  }
  else {
    res.send("No File selected !");
    res.end();
  };
})

shipment.post('/get_trf',function(req,res){

    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_trf_documents_by_trfid("+req.body.trf_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                	console.log("_________________________________________");
                	console.log(rows[0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  console.log("&&&&&&&&&&&&&&&&&&&&&&&")
               			 console.log(rows[0]);
                        res.status(200).json({"error":0,"documents":rows[0]});
                    }
                    else
                    {console.log("**********************")
                    	console.log(rows[0]);
                       res.status(200).json({"error":0,"documents":rows[0]});
                    }
                }
            });
            connection.release();
        }
    });
})



shipment.post('/download_trf_image',function(req,res){
var document_filename = req.body.document_file_name;
try{

const file = path.join( __dirname ,'../Trf/') + document_filename;
/*console.log(file);*/
   res.status(200).download(file); // Set disposition and send it.
}
catch (err) {
    console.log(err);
  }

})

shipment.post('/delete_trf_image',function(req,res){
const file = path.join( __dirname ,'../Trf/') + req.body.document_file_name;
//console.log(file);
    database.connection.getConnection(function(err, connection) {
        if (err) {
           console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
           var sql="CALL "+config.dbname+".usp_delete_trf_document(?,?)"; 
           connection.query(sql,[req.body.trf_document_id,req.session.usrData.user_id], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {     
                	   fs.unlinkSync(file);                                
                       res.status(200).json({"error":0,"data":"Document deleted"});
                    
                }
            });
            connection.release();
        }
    });
})




shipment.post('/fibre_detail', audit.get_fibre_detail);


module.exports = shipment;