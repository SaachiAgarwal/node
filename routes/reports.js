var express = require('express');
var reports = express.Router();
var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');

var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var logger=require('../controllers/logger');
var orderController=require('../controllers/orderController');
var token;
var bcrypt = require('bcrypt');
var config=require('../config/config');
var flash = require('express-flash');
reports.use(flash());

reports.use(cors());

var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";
var resMsg=require('../config/resMessage');
reports.use(function(req, res, next) {
    var token = req.body.token || req.headers['token'];
    //console.log("token="+token);
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, function(err,dt) {
            console.log(err);
            if (err) {
                res.status(200).json({"error":5,"data":resMsg.tokenError});
            } else {
                //console.log(dt);
                req.session.usrData=dt;
                next();
            }
        })
    } else { 
        res.status(403).json({"error":1,"data":resMsg.emptyToken});
    }
});

//Order Reports 

reports.get('/outgoing_orders_summary',function(req,res){
    //console.log(req.session.usrData.company_key_id);
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_outgoing_orders_summary(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"outgoing_orders_summary":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/incoming_orders_summary',function(req,res){    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_incoming_orders_summary(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"incoming_orders_summary":rows[rows.length-1][0]});                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/order_top_supplier',function(req,res){    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;   
    var sql="CALL "+config.dbname+".usp_dashboard_get_top_supplierlist(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"order_top_supplier":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});



reports.get('/order_top_products',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_topmost_ordered_products(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"order_top_products":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/get_all_order_turnaround',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_all_orders_turnaround(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"order_turnaround":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});


reports.get('/get_top_vendor_order_turnaround',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_top_vendor_orders_turnaround(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"top_vendor":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

//Shipment reports 

reports.get('/outgoing_shipment_summary',function(req,res){
    //console.log(req.session.usrData.company_key_id);
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_outgoing_shipment_summary(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"outgoing_shipment_summary":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/incoming_shipment_summary',function(req,res){    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_incoming_shipment_summary(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"incoming_shipment_summary":rows[rows.length-1][0]});                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/shipment_top_buyer',function(req,res){    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;   
    var sql="CALL "+config.dbname+".usp_dashboard_get_top_buyerlist(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"shipment_top_buyer":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});



reports.get('/shipment_top_products',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_topmost_shipped_products(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"shipment_top_products":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});


//Stock Reports 
reports.get('/stock_summary',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_stock_summary(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"stock_summary":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/stock_monthwise',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_monthwise_stock(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"stock_monthwise":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/stock_productwise',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_productwise_stock(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"stock_productwise":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});

reports.get('/stock_supplierwise',function(req,res){
    
    let from_date=(req.query.from_date===undefined)?"":req.query.from_date;
    let to_date=(req.query.to_date===undefined)?"":req.query.to_date;
   
    var sql="CALL "+config.dbname+".usp_dashboard_get_supplierwise_stock(?,?,?)";    
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            connection.query(sql,[req.session.usrData.company_key_id,from_date,to_date], function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":"Sql error"});
                }else {                   
                   
                     res.status(200).json({"error":0,"data":rows[0],"stock_supplierwise":rows[rows.length-1][0]});
                    
                }
            });
            connection.release();
        }
    });
});



module.exports = reports