'use strict';
/* global process */
/*******************************************************************************
 * Copyright (c) 2015 IBM Corp.
 *
 * All rights reserved. 
 *
 *******************************************************************************/


var express = require('express');
var router = express.Router();
var helper = require(__dirname + '/../utils/helper.js')(process.env.creds_filename, console);
var hfc = require('fabric-client');
var path = require('path');
var util = require('util');

var options = {
    		wallet_path: path.join(__dirname, '../config/crypto/prebaked'),
	};	


var database = require('../Database/database');
var cors = require('cors')
var jwt = require('jsonwebtoken');

var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');

var token;
var bcrypt = require('bcrypt');



var flash = require('express-flash');
router.use(flash());

router.use(cors());

var BCRYPT_SALT_ROUNDS = 12;
process.env.SECRET_KEY = "new";

const SOCKET_IO_ENDPOINT= process.env.SOCKET_API_URL || 'http://ec2-52-91-69-35.compute-1.amazonaws.com:3000';
const io = require('socket.io-client')
const socketIo= io(SOCKET_IO_ENDPOINT);


//anything in here gets passed to Pug template engine
function build_bag(req) {
	return {
		e: process.error,							//send any setup errors
		creds_filename: process.env.creds_filename,
		jshash: process.env.cachebust_js,			//js cache busting hash (not important)
		csshash: process.env.cachebust_css,			//css cache busting hash (not important)
		marble_company: process.env.marble_company,
		creds: get_credential_data()
	};
}



//get cred data
function get_credential_data() {
	const channel = helper.getChannelId();
	const first_org = helper.getFirstOrg();
	const first_ca = helper.getFirstCaName(first_org);
	const first_peer = helper.getFirstPeerName(channel);
	const first_orderer = helper.getFirstOrdererName(channel);
	var ret = {
		admin_id: helper.getEnrollObj(first_ca, 0).enrollId,
		admin_secret: helper.getEnrollObj(first_ca, 0).enrollSecret,
		orderer: helper.getOrderersUrl(first_orderer),
		ca: helper.getCasUrl(first_ca),
		peer: helper.getPeersUrl(first_peer),
		chaincode_id: helper.getChaincodeId(),
		channel: helper.getChannelId(),
		chaincode_version: helper.getChaincodeVersion(),
		marble_owners: helper.getMarbleUsernames(),
	};
	for (var i in ret) {
		if (ret[i] == null) ret[i] = '';			//set to blank if not found
	}
	return ret;
}


// Add this function definition below get_credential_data()

function emitMessage(message){
                console.log("SOCKET_IO_ENDPOINT : "+ SOCKET_IO_ENDPOINT);
		console.log("message : "+ message);
		//var socket = socketIo.connect();
                if(socketIo.disconnected){
                                //socket.io queues messages if it is disconnected, and sends them all in next emit
				console.log("disconnected : "+ socketIo.disconnected);

                                socketIo.sendBuffer=[]
                                	
				socketIo.connect();	
                }
                socketIo.emit("send-message", message);
                //socketIo.disconnect();
}


// ============================================================================================================================
// Root
// ============================================================================================================================
router.route('/').get(function (req, res) {
	res.redirect('/home');
});

// ============================================================================================================================
// Login
// ============================================================================================================================
router.route('/login').get(function (req, res) {
	res.render('login', { title: 'Marbles - Login', bag: build_bag(req) });
});

router.route('/login').post(function (req, res) {
	req.session.user = { username: 'Admin' };
	res.redirect('/home');
});

router.route('/logout').get(function (req, res) {
	req.session.destroy();
	res.redirect('/login');
});


// ============================================================================================================================
// Home
// ============================================================================================================================
router.route('/home').get(function (req, res) {
	route_me(req, res);
});

router.route('/create').get(function (req, res) {
	route_me(req, res);
});

function route_me(req, res) {
	if (!req.session.user || !req.session.user.username) {
		res.redirect('/login');
	}
	else {
		res.render('marbles', { title: 'Marbles - Home', bag: build_bag(req) });
	}
}


// ============================================================================================================================
// GSA Offer
// ============================================================================================================================
router.route('/getOffers').get(function (req, res) {
	console.log('in getOffers');

	
	var channel = {};
	var client = null;

	
	const channel1 = helper.getChannelId();
			const first_org = helper.getFirstOrg();
			const first_ca = helper.getFirstCaName(first_org);
			const first_peer = helper.getFirstPeerName(channel1);
			const first_orderer = helper.getFirstOrdererName(channel1);
			const org_name = helper.getOrgsMSPid(first_org);				//lets use the first org we find
			const user_obj = helper.getEnrollObj(first_ca, 0);


	Promise.resolve().then(() => {
    console.log("Create a client and set the wallet location");
    client = new hfc();
    return hfc.newDefaultKeyValueStore({ path: options.wallet_path });
	}).then((wallet) => {
	   
	    client.setStateStore(wallet);
	    return client.getUserContext(user_obj.enrollId, true);
	}).then((user) => {
	    console.log("Check user is enrolled, and set a query URL in the network");
	    if (user === undefined || user.isEnrolled() === false) {
	        console.error("User not defined, or not enrolled - error");
	    }
	    channel = client.newChannel(helper.getChannelId());
	    channel.addPeer(client.newPeer(helper.getPeersUrl(first_peer)));
	    return;
	}).then(() => {
	    console.log("Make query");
	    var transaction_id = client.newTransactionID();
	    console.log("Assigning transaction_id: ", transaction_id._transaction_id);

	    // queryOffer - requires 1 argument, ex: args: ['OFFER4'],
	    // queryAllOffers - requires no arguments , ex: args: [''],
	    const request = {
	        chaincodeId: helper.getChaincodeId(),
	        txId: transaction_id,
	        fcn: 'queryAllOffers',
	        args: ['']
	    };
	    return channel.queryByChaincode(request);
	}).then((query_responses) => {
	    console.log("returned from query");
	    if (!query_responses.length) {
	        console.log("No payloads were returned from query");
	    } else {
	        console.log("Query result count = ", query_responses.length)
	    }
	    if (query_responses[0] instanceof Error) {
	        console.error("error from query = ", query_responses[0]);
	    }
	    console.log("Response is ", query_responses[0].toString());
		emitMessage("GetOffers Successfully");

	    res.send(query_responses[0].toString()); 	
	}).catch((err) => {
	    console.error("Caught Error", err);
	});

});

router.route('/getOffer').post(function (req, res) {
	console.log('in getOffer');

	
	var channel = {};
	var client = null;

	

	const channel1 = helper.getChannelId();
			const first_org = helper.getFirstOrg();
			const first_ca = helper.getFirstCaName(first_org);
			const first_peer = helper.getFirstPeerName(channel1);
			const first_orderer = helper.getFirstOrdererName(channel1);
			const org_name = helper.getOrgsMSPid(first_org);				//lets use the first org we find
			const user_obj = helper.getEnrollObj(first_ca, 0);


	Promise.resolve().then(() => {
    console.log("Create a client and set the wallet location");
    client = new hfc();
    return hfc.newDefaultKeyValueStore({ path: options.wallet_path });
	}).then((wallet) => {
	   
	    client.setStateStore(wallet);
	    return client.getUserContext(user_obj.enrollId, true);
	}).then((user) => {
	    console.log("Check user is enrolled, and set a query URL in the network");
	    if (user === undefined || user.isEnrolled() === false) {
	        console.error("User not defined, or not enrolled - error");
	    }
	    channel = client.newChannel(helper.getChannelId());
	    channel.addPeer(client.newPeer(helper.getPeersUrl(first_peer)));
	    return;
	}).then(() => {
	    console.log("Make query");
	    var transaction_id = client.newTransactionID();
	    console.log("Assigning transaction_id: ", transaction_id._transaction_id);

	    // queryOffer - requires 1 argument, ex: args: ['OFFER4'],
	    // queryAllOffers - requires no arguments , ex: args: [''],
	    const request = {
	        chaincodeId: helper.getChaincodeId(),
	        txId: transaction_id,
	        fcn: 'queryOffer',
	        args: [req.body.Id]
	    };
	    return channel.queryByChaincode(request);
	}).then((query_responses) => {
	    console.log("returned from query");
	    if (!query_responses.length) {
	        console.log("No payloads were returned from query");
	    } else {
	        console.log("Query result count = ", query_responses.length)
	    }
	    if (query_responses[0] instanceof Error) {
	        console.error("error from query = ", query_responses[0]);
	    }
	    console.log("Response is ", query_responses[0].toString());
	    res.send(query_responses[0].toString()); 	
	}).catch((err) => {
	    console.error("Caught Error", err);
	});

});



router.route('/createOffer').post(function (req, res) {
	console.log('in createOffer');

	
	var channel = {};
	var client = null;
	var targets = [];
	var tx_id = null;
	var d = new Date();
	var counter = d.getTime();
 	


	const channel1 = helper.getChannelId();
			const first_org = helper.getFirstOrg();
			const first_ca = helper.getFirstCaName(first_org);
			const first_peer = helper.getFirstPeerName(channel1);
			const first_orderer = helper.getFirstOrdererName(channel1);
			const org_name = helper.getOrgsMSPid(first_org);				//lets use the first org we find
			const user_obj = helper.getEnrollObj(first_ca, 0);


	Promise.resolve().then(() => {
	    console.log("Create a client and set the wallet location");
	    client = new hfc();
	    return hfc.newDefaultKeyValueStore({ path: options.wallet_path });
	}).then((wallet) => {
	    console.log("Set wallet path, and associate user ", user_obj.enrollId, " with application");
	    client.setStateStore(wallet);
	    return client.getUserContext(user_obj.enrollId, true);
	}).then((user) => {
	    console.log("Check user is enrolled, and set a query URL in the network");
	    if (user === undefined || user.isEnrolled() === false) {
	        console.error("User not defined, or not enrolled - error");
	    }
	    channel = client.newChannel(helper.getChannelId());
	    var peerObj = client.newPeer(helper.getPeersUrl(first_peer));
	    channel.addPeer(peerObj);
	    channel.addOrderer(client.newOrderer(helper.getOrderersUrl(first_orderer)));
	    targets.push(peerObj);
	    return;
	}).then(() => {
	    tx_id = client.newTransactionID();
	    console.log("Assigning transaction_id: ", tx_id._transaction_id);
	    // createOffer - requires 5 args, ex: args: ['CAR11', 'Honda', 'Accord', 'Black', 'Tom'],
	    // changeOfferDetails - requires 2 args , ex: args: ['CAR10', 'Barry'],
	    // send proposal to endorser
	    
	    var idGenerated = counter.toString();//JSON.stringify(counter);
	   
	    var request = {
	        targets: targets,
	        chaincodeId: helper.getChaincodeId(),
	        fcn: 'createOffer',
	        //args: [''+counter,req.body.offerID, req.body.assigneeID, req.body.assigneeName, req.body.submissionDate, req.body.vendorID, req.body.vendorName, req.body.contactID, req.body.contactName],
		args: [idGenerated,JSON.stringify(req.body) ],
	        chainId: helper.getChannelId(),
	        txId: tx_id
	    };
		 console.log(request.body);
		console.log(channel.sendTransactionProposal);

	    return channel.sendTransactionProposal(request);
	}).then((results) => {
		console.log('results' + results);

	    var proposalResponses = results[0];
	    var proposal = results[1];
	    var header = results[2];
	    let isProposalGood = false;
	    if (proposalResponses && proposalResponses[0].response &&
	        proposalResponses[0].response.status === 200) {
	        isProposalGood = true;
	        console.log('transaction proposal was good');
	    } else {
	        console.error('transaction proposal was bad');
	    }
	    if (isProposalGood) {
	        console.log(util.format(
	            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
	            proposalResponses[0].response.status, proposalResponses[0].response.message,
	            proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));
	        var request = {
	            proposalResponses: proposalResponses,
	            proposal: proposal,
	            header: header
	        };
	        // set the transaction listener and set a timeout of 30sec
	        // if the transaction did not get committed within the timeout period,
	        // fail the test
	        var transactionID = tx_id.getTransactionID();
	        var eventPromises = [];
	        let eh = client.newEventHub();
	        eh.setPeerAddr(helper.getPeerEventUrl(first_peer));
	        eh.connect();

	        let txPromise = new Promise((resolve, reject) => {
	            let handle = setTimeout(() => {
	                eh.disconnect();
	                reject();
	            }, 30000);

	            eh.registerTxEvent(transactionID, (tx, code) => {
	                clearTimeout(handle);
	                eh.unregisterTxEvent(transactionID);
	                eh.disconnect();

	                if (code !== 'VALID') {
	                    console.error(
	                        'The transaction was invalid, code = ' + code);
	                    reject();
	                } else {
	                    console.log(
	                        'The transaction has been committed on peer ' +
	                        eh._ep._endpoint.addr);
	                    resolve();
	                }
	            });
	        });
	        eventPromises.push(txPromise);
	        var sendPromise = channel.sendTransaction(request);
	        return Promise.all([sendPromise].concat(eventPromises)).then((results) => {
	            console.log(' event promise all complete and testing complete');
	            emitMessage("Offer Created Successfully-->" + proposalResponses[0].response.payload);
	            return results[0]; // the first returned value is from the 'sendPromise' which is from the 'sendTransaction()' call
	        }).catch((err) => {
	            console.error(
	                'Failed to send transaction and get notifications within the timeout period.'
	            );
	            return 'Failed to send transaction and get notifications within the timeout period.';
	        });
	    } else {
	        console.error(
	            'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
	        );
	        return 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...';
	    }
	}, (err) => {
	    console.error('Failed to send proposal due to error: ' + err.stack ? err.stack :
	        err);
	    return 'Failed to send proposal due to error: ' + err.stack ? err.stack :
	        err;
	}).then((response) => {
	    if (response.status === 'SUCCESS') {
	        console.log('Successfully sent transaction to the orderer.');
		
	        //return tx_id.getTransactionID();
	        res.send( ''+counter ); 
	    } else {
	        console.error('Failed to order the transaction. Error code: ' + response.status);
	        return 'Failed to order the transaction. Error code: ' + response.status;
	    }

	}, (err) => {
	    console.error('Failed to send transaction due to error: ' + err.stack ? err
	        .stack : err);
	    return 'Failed to send transaction due to error: ' + err.stack ? err.stack :
	        err;
	});




});

router.route('/updateOffer').put(function (req, res) {
	console.log('in updateOffer');

	
	var channel = {};
	var client = null;
	var targets = [];
	var tx_id = null;
	


	const channel1 = helper.getChannelId();
			const first_org = helper.getFirstOrg();
			const first_ca = helper.getFirstCaName(first_org);
			const first_peer = helper.getFirstPeerName(channel1);
			const first_orderer = helper.getFirstOrdererName(channel1);
			const org_name = helper.getOrgsMSPid(first_org);				//lets use the first org we find
			const user_obj = helper.getEnrollObj(first_ca, 0);


	Promise.resolve().then(() => {
	    console.log("Create a client and set the wallet location");
	    client = new hfc();
	    return hfc.newDefaultKeyValueStore({ path: options.wallet_path });
	}).then((wallet) => {
	    console.log("Set wallet path, and associate user ", user_obj.enrollId, " with application");
	    client.setStateStore(wallet);
	    return client.getUserContext(user_obj.enrollId, true);
	}).then((user) => {
	    console.log("Check user is enrolled, and set a query URL in the network");
	    if (user === undefined || user.isEnrolled() === false) {
	        console.error("User not defined, or not enrolled - error");
	    }
	    channel = client.newChannel(helper.getChannelId());
	    var peerObj = client.newPeer(helper.getPeersUrl(first_peer));
	    channel.addPeer(peerObj);
	    channel.addOrderer(client.newOrderer(helper.getOrderersUrl(first_orderer)));
	    targets.push(peerObj);
	    return;
	}).then(() => {
	    tx_id = client.newTransactionID();
	    console.log("Assigning transaction_id: ", tx_id._transaction_id);
	    // createOffer - requires 5 args, ex: args: ['CAR11', 'Honda', 'Accord', 'Black', 'Tom'],
	    // changeOfferDetails - requires 2 args , ex: args: ['CAR10', 'Barry'],
	    // send proposal to endorser
	   
	    var request = {
	        targets: targets,
	        chaincodeId: helper.getChaincodeId(),
	        fcn: 'changeOfferDetails',
	        //args: [req.body.Id, req.body.offerID, req.body.assigneeID,req.body.assigneeName, req.body.submissionDate, req.body.vendorID, req.body.vendorName, req.body.contactID, req.body.contactName],
		args: [''+req.body.Id, JSON.stringify(req.body)],
	        chainId: helper.getChannelId(),
	        txId: tx_id
	    };
	    return channel.sendTransactionProposal(request);
	}).then((results) => {
	    var proposalResponses = results[0];
	    var proposal = results[1];
	    var header = results[2];
	    let isProposalGood = false;
	    if (proposalResponses && proposalResponses[0].response &&
	        proposalResponses[0].response.status === 200) {
	        isProposalGood = true;
	        console.log('transaction proposal was good');
	    } else {
	        console.error('transaction proposal was bad');
	    }
	    if (isProposalGood) {
	        console.log(util.format(
	            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
	            proposalResponses[0].response.status, proposalResponses[0].response.message,
	            proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));
	        var request = {
	            proposalResponses: proposalResponses,
	            proposal: proposal,
	            header: header
	        };
	        // set the transaction listener and set a timeout of 30sec
	        // if the transaction did not get committed within the timeout period,
	        // fail the test
	        var transactionID = tx_id.getTransactionID();
	        var eventPromises = [];
	        let eh = client.newEventHub();
	        eh.setPeerAddr(helper.getPeerEventUrl(first_peer));
	        eh.connect();

	        let txPromise = new Promise((resolve, reject) => {
	            let handle = setTimeout(() => {
	                eh.disconnect();
	                reject();
	            }, 30000);

	            eh.registerTxEvent(transactionID, (tx, code) => {
	                clearTimeout(handle);
	                eh.unregisterTxEvent(transactionID);
	                eh.disconnect();

	                if (code !== 'VALID') {
	                    console.error(
	                        'The transaction was invalid, code = ' + code);
	                    reject();
	                } else {
	                    console.log(
	                        'The transaction has been committed on peer ' +
	                        eh._ep._endpoint.addr);
	                    resolve();
	                }
	            });
	        });
	        eventPromises.push(txPromise);
	        var sendPromise = channel.sendTransaction(request);
	        return Promise.all([sendPromise].concat(eventPromises)).then((results) => {
	            console.log(' event promise all complete and testing complete');
	            return results[0]; // the first returned value is from the 'sendPromise' which is from the 'sendTransaction()' call
	        }).catch((err) => {
	            console.error(
	                'Failed to send transaction and get notifications within the timeout period.'
	            );
	            return 'Failed to send transaction and get notifications within the timeout period.';
	        });
	    } else {
	        console.error(
	            'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
	        );
	        return 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...';
	    }
	}, (err) => {
	    console.error('Failed to send proposal due to error: ' + err.stack ? err.stack :
	        err);
	    return 'Failed to send proposal due to error: ' + err.stack ? err.stack :
	        err;
	}).then((response) => {
	    if (response.status === 'SUCCESS') {
	        console.log('Successfully sent transaction to the orderer.');
	        //return tx_id.getTransactionID();
	        res.send( tx_id.getTransactionID()); 
	    } else {
	        console.error('Failed to order the transaction. Error code: ' + response.status);
	        return 'Failed to order the transaction. Error code: ' + response.status;
	    }

	}, (err) => {
	    console.error('Failed to send transaction due to error: ' + err.stack ? err
	        .stack : err);
	    return 'Failed to send transaction due to error: ' + err.stack ? err.stack :
	        err;
	});




});

router.route('/deleteOffer').delete(function (req, res) {
	console.log('in updateOffer');

	
	var channel = {};
	var client = null;
	var targets = [];
	var tx_id = null;
	


	const channel1 = helper.getChannelId();
			const first_org = helper.getFirstOrg();
			const first_ca = helper.getFirstCaName(first_org);
			const first_peer = helper.getFirstPeerName(channel1);
			const first_orderer = helper.getFirstOrdererName(channel1);
			const org_name = helper.getOrgsMSPid(first_org);				//lets use the first org we find
			const user_obj = helper.getEnrollObj(first_ca, 0);


	Promise.resolve().then(() => {
	    console.log("Create a client and set the wallet location");
	    client = new hfc();
	    return hfc.newDefaultKeyValueStore({ path: options.wallet_path });
	}).then((wallet) => {
	    console.log("Set wallet path, and associate user ", user_obj.enrollId, " with application");
	    client.setStateStore(wallet);
	    return client.getUserContext(user_obj.enrollId, true);
	}).then((user) => {
	    console.log("Check user is enrolled, and set a query URL in the network");
	    if (user === undefined || user.isEnrolled() === false) {
	        console.error("User not defined, or not enrolled - error");
	    }
	    channel = client.newChannel(helper.getChannelId());
	    var peerObj = client.newPeer(helper.getPeersUrl(first_peer));
	    channel.addPeer(peerObj);
	    channel.addOrderer(client.newOrderer(helper.getOrderersUrl(first_orderer)));
	    targets.push(peerObj);
	    return;
	}).then(() => {
	    tx_id = client.newTransactionID();
	    console.log("Assigning transaction_id: ", tx_id._transaction_id);
	    // createOffer - requires 5 args, ex: args: ['CAR11', 'Honda', 'Accord', 'Black', 'Tom'],
	    // changeOfferDetails - requires 2 args , ex: args: ['CAR10', 'Barry'],
	    // send proposal to endorser
	   
	    var request = {
	        targets: targets,
	        chaincodeId: helper.getChaincodeId(),
	        fcn: 'deleteOffer',
	        args: [req.body.Id],
	        chainId: helper.getChannelId(),
	        txId: tx_id
	    };
	    return channel.sendTransactionProposal(request);
	}).then((results) => {
	    var proposalResponses = results[0];
	    var proposal = results[1];
	    var header = results[2];
	    let isProposalGood = false;
	    if (proposalResponses && proposalResponses[0].response &&
	        proposalResponses[0].response.status === 200) {
	        isProposalGood = true;
	        console.log('transaction proposal was good');
	    } else {
	        console.error('transaction proposal was bad');
	    }
	    if (isProposalGood) {
	        console.log(util.format(
	            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
	            proposalResponses[0].response.status, proposalResponses[0].response.message,
	            proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));
	        var request = {
	            proposalResponses: proposalResponses,
	            proposal: proposal,
	            header: header
	        };
	        // set the transaction listener and set a timeout of 30sec
	        // if the transaction did not get committed within the timeout period,
	        // fail the test
	        var transactionID = tx_id.getTransactionID();
	        var eventPromises = [];
	        let eh = client.newEventHub();
	        eh.setPeerAddr(helper.getPeerEventUrl(first_peer));
	        eh.connect();

	        let txPromise = new Promise((resolve, reject) => {
	            let handle = setTimeout(() => {
	                eh.disconnect();
	                reject();
	            }, 30000);

	            eh.registerTxEvent(transactionID, (tx, code) => {
	                clearTimeout(handle);
	                eh.unregisterTxEvent(transactionID);
	                eh.disconnect();

	                if (code !== 'VALID') {
	                    console.error(
	                        'The transaction was invalid, code = ' + code);
	                    reject();
	                } else {
	                    console.log(
	                        'The transaction has been committed on peer ' +
	                        eh._ep._endpoint.addr);
	                    resolve();
	                }
	            });
	        });
	        eventPromises.push(txPromise);
	        var sendPromise = channel.sendTransaction(request);
	        return Promise.all([sendPromise].concat(eventPromises)).then((results) => {
	            console.log(' event promise all complete and testing complete');
	            return results[0]; // the first returned value is from the 'sendPromise' which is from the 'sendTransaction()' call
	        }).catch((err) => {
	            console.error(
	                'Failed to send transaction and get notifications within the timeout period.'
	            );
	            return 'Failed to send transaction and get notifications within the timeout period.';
	        });
	    } else {
	        console.error(
	            'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
	        );
	        return 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...';
	    }
	}, (err) => {
	    console.error('Failed to send proposal due to error: ' + err.stack ? err.stack :
	        err);
	    return 'Failed to send proposal due to error: ' + err.stack ? err.stack :
	        err;
	}).then((response) => {
	    if (response.status === 'SUCCESS') {
	        console.log('Successfully sent transaction to the orderer.');
	        //return tx_id.getTransactionID();
	        res.send( tx_id.getTransactionID()); 
	    } else {
	        console.error('Failed to order the transaction. Error code: ' + response.status);
	        return 'Failed to order the transaction. Error code: ' + response.status;
	    }

	}, (err) => {
	    console.error('Failed to send transaction due to error: ' + err.stack ? err
	        .stack : err);
	    return 'Failed to send transaction due to error: ' + err.stack ? err.stack :
	        err;
	});




});



module.exports = router;