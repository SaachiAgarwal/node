var groupBy = require('json-groupby')

var path = require('path');
var Fabric_Client = require('fabric-client');
var util = require('util');
var os = require('os');
var fabric_client = new Fabric_Client();
var channel = fabric_client.newChannel('mychannel');
var peer = fabric_client.newPeer('grpc://localhost:8051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);
var shipmentController=require('./shipmentController');
var async=require('async');
var member_user = null;
//var store_path = path.join('/home/parikshit/shipment/abg_project_hyperledger/grasim' , 'hfc-key-store');
console.log('dir',__dirname);
var store_path = path.join(__dirname, '../../../abg_project_hyperledger/grasim' , 'hfc-key-store');
var config=require('../config/config');
var database = require('../Database/database');
var tx_id = null;
var chaincodeId = 'grasim';
var chainId = 'mychannel';
var test = 0;
module.exports={
    make_audit:function(req,res,next){
        if(req.query.asset_id===undefined)
            res.status(200).json({"error":1,"data":"Please send asset id to track"});
        else
        {
            console.log('asset_id',req.query.asset_id);
            req.body.asset_id=req.query.asset_id;
            next();
        }
    },

save_auditto_db : function(req,res,next){

 async.waterfall([
                
                        function(callback){
                        
                        module.exports.auditquery(req.body.asset_id,function(result){
                            if(result.error)
                                callback(result,null);
                            else if(req.body.asset_id===null || req.body.asset_id===undefined)
                                callback({"error":0,"data":"Shipment placed successfully"},null)
                            else{
                                console.log('here333');
                                 callback(null,result);
                             }
                            console.log('here1');
                        });
                }, function(result,callback) {
                        database.connection.getConnection(function(err, connection) {
                        if (err) {
                            console.log(err);
                           /* logger.error(req.session.usrData.email+Date()+JSON.stringify(err))*/;
                           // res.status(200).json({"error":1,"data":"Internal server error"});
                             callback(null,err);
                        } else {
                            console.log(req.body.shipment_id);
                           var sql="CALL "+config.dbname+".usp_add_shipment_trace_json_by_shipmentid(?,?,?,@bresult)";
                            connection.query(sql,[req.body.shipment_id,JSON.stringify(result),'1234' ],function(err, rows, fields) {
                                if (err) {
                                    console.log(err);
                                   /* logger.error(req.session.usrData.email+Date()+JSON.stringify(err));*/
                                      callback(null,err);
                                } else {
                                    console.log("ttttttttt")
                                 /*  res.status(200).json({"error":0,"data":"Count updated successfully"})*/
                                }
                            });
                            connection.release();
                        }
                    });
                    callback(null,'Done');
                      
                }

                ], function (err, result) {
                if(err)
                {   console.log('in next',err); res.status(200).json(err); 
                    if(!err.error)
                    next();
                }
                else
                    {console.log('heresssssssss');
                        res.status(200).json({"error":0,"data":"Audit saved successfully"});
                        next(); console.log('here');
                        //console.log(req.body);
                    }

            });
},


//Trace shipment with order id as input - same as traceshipment just the input parameter is different.

traceorder: function(req,res,next){
console.log("CCCCCCCCCCCCCCCC");
console.log(req.body.asset_id);
console.log(req.body.order_id)
var sql="CALL "+config.dbname+".usp_get_shipment_trace_by_order_id("+req.body.order_id+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        console.log('err in call order trace',err);
        res.status(500).json({"error":0,"data":"Internal server error"});
    } else {
        connection.query(sql,function(err, rows, fields) {
            /*console.log("products");
            console.log(rows);*/
            if (err) {
                console.log('err in call order trace 2nd',err);
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else {
                 console.log("ooooooooooo");
                 console.log(rows[0]);

                    if(rows !=="undefined" && rows[0] !==undefined){
                    /*console.log('here1',req.body.asset_id===rows[0][0].asset_id);*/
                    if(req.body.asset_id===rows[0][0].asset_id)
                    {
                        module.exports.make_data(rows[0],req.body.asset_id,function(result){
                            
                            let asset_id=req.body.asset_id
                            req.body.shipment=result;
                            req.body.isAudit=1;
                            console.log("cccccccccccccccccccccccccccc");
                                 console.log(result);                                   
                            next();

                        })
                    }
                }
                    else {
                        console.log("kkkkkkkkkkkkkk");
                        next();
                    }
                    
                                        
            }
        });
        connection.release();
    }
});    
},


audit_display: function(req,res,next){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_audit_display_preference("+req.session.usrData.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    console.log(rows[0][0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"data":123});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0][0]});
                    }
                }
            });
            connection.release();
        }
    });
},

public_audit_display: function(req,res,next){
    database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":"Internal server error"});
        } else {
            var sql="CALL "+config.dbname+".usp_get_audit_display_preference("+req.body.company_key_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(201).json({"error":1,"data":"Sql error"});
                } else {
                    console.log(rows[0][0]);
                    if(rows==="undefined" || rows[0].length===0)
                    {  
                        res.status(200).json({"error":0,"data":123});
                    }
                    else
                    {
                        res.status(200).json({"error":0,"data":rows[0][0]});
                    }
                }
            });
            connection.release();
        }
    });
},



traceshipment: function(req,res,next){
    console.log("::::::::::::::::::::::::::::::::::::::::::::::");
console.log("BBBBBBBBBBBBBBBBBBBB");
console.log(req.body.asset_id);
console.log(req.body.shipment_id)
var sql="CALL "+config.dbname+".usp_get_shipment_trace("+req.body.shipment_id+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        console.log('err in call shipment trace',err);
        res.status(500).json({"error":0,"data":"Shipment placed successfully"});
    } else {
        connection.query(sql,function(err, rows, fields) {
            /*console.log("products");
            console.log(rows);*/
            if (err) {
                console.log(err);
                console.log('err in call shipment trace 2nd',err);
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else {
                 console.log("ooooooooooo");
                 console.log(rows[0]);

                    if(rows !=="undefined" && rows[0] !==undefined){
                    /*console.log('here1',req.body.asset_id===rows[0][0].asset_id);*/
                    if(req.body.asset_id===rows[0][0].asset_id)
                    {     console.log("____________________________________________________________");
                        module.exports.make_data(rows[0],req.body.asset_id,function(result){
                            
                            let asset_id=req.body.asset_id
                            req.body.shipment=result;
                            req.body.isAudit=1;
                            console.log("cccccccccccccccccccccccccccc");
                                 console.log(result);                                   
                            next();

                        })
                    }
                }
                    else {
                        console.log("kkkkkkkkkkkkkk");
                        next();
                    }
                    
                                        
            }
        });
        connection.release();
    }
});    
},


getshipmentid: function(req,res,next){
console.log("55555555");
console.log("vvvvvvvv");


let body = JSON.stringify(req.body.asset_id);

var sql="CALL "+config.dbname+".usp_get_shipmentIdByAssetId("+body+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        
        res.status(500).json({"error":0,"data":"Shipment placed successfully"});
    } else {
        connection.query(sql,function(err, rows, fields) {
           console.log("here");
            if (err) {
                console.log(err);
               
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else {
                console.log("hhhhhhhhh");
                console.log(rows[0][0]);
               console.log(rows[0][0] == undefined)
               if (rows[0][0] == undefined){
           res.status(200).json({"error":0,"data":"Shipment has not started for this order"})
                      }    
                    else{
                        req.body.shipment_id =rows[0][0].shipment_id;                          
                         next();
                    }

            }
        });
        connection.release();
    }
});    
},




get_companyid: function(req,res,next){
console.log("oooooooosssssssssssssssssssssssssssssssss");
console.log("vvvvvvvv");

console.log(req.query.asset_id);
let body = JSON.stringify(req.query.asset_id);
console.log(body);
var sql="CALL "+config.dbname+".usp_get_company_keyid_by_assetid("+body+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        
        res.status(500).json({"error":0,"data":"Internal server error"});
    } else {
        connection.query(sql,function(err, rows, fields) {
           console.log("here");
            if (err) {
                console.log(err);
               
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else {
                console.log("hhhhhhhhh");
               console.log(rows[0][0] == undefined)
              
                console.log(rows[0][0].company_key_id);
               req.body.company_key_id =rows[0][0].company_key_id;                          
                         next();
                    

            }
        });
        connection.release();
    }
});    
},



tracegeo: function(req,res,next){
console.log("777777");
console.log(req.body.shipment_id);

var sql="CALL "+config.dbname+".usp_get_shipment_trace_for_geo("+req.body.shipment_id+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        console.log(err);
        console.log('err in call shipment trace',err);
        res.status(500).json({"error":0,"data":"Internal server error"});
    } else {
        connection.query(sql,function(err, rows, fields) {
            /*console.log("products");
            console.log(rows);*/
            if (err) {
                console.log(err);
              
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else if (rows[0] == ''){
              res.status(200).json({"error":0,"data":"Shipment has not started for this order"})    

            }
            else {
                 console.log("ooooooooooo");
                console.log(rows[0]);


                    if(rows !=="undefined" && rows[0] !==undefined){

                    /*console.log('here1',req.body.asset_id===rows[0][0].asset_id);*/
                    if(req.body.asset_id===rows[0][0].asset_id )
                    {
                        module.exports.make_data(rows[0],req.body.asset_id,function(result){
                            
                            let asset_id=req.body.asset_id
                            req.body.shipment=result;
                            req.body.isAudit=1;
                            console.log("ttttttttttttttttttttttttttttttttttttttttttttt");
                            console.log(result);
                            var array = result.reverse();
                            console.log(array);
                            var group = groupBy(array, ['seller_company_name']);
	                          var new_array = [];
							    var arrayIndex = 0;
							    var kkk = group
							    for (var key in kkk) {
							      if (kkk.hasOwnProperty(key)) {
							        kkk[key].map((val, index) => {
							          console.log(val)
							          new_array[arrayIndex++] = val
							        })
							      }
							    }
							    var new_traversed = new_array.reverse()

                                 res.status(200).send(new_traversed);      


                            next();

                        })
                    }
                }
                    else {
                        console.log("kkkkkkkkkkkkkk");
                        next();
                    }
                    
                                        
            }
        });
        connection.release();
    }
});    
},



trace_exportgeo: function(req,res,next){
console.log("777777");
console.log(req.body.shipment_id);

var sql="CALL "+config.dbname+".usp_get_shipment_trace_for_geo("+req.body.shipment_id+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        console.log(err);
        console.log('err in call shipment trace',err);
        res.status(500).json({"error":0,"data":"Internal server error"});
    } else {
        connection.query(sql,function(err, rows, fields) {
            /*console.log("products");
            console.log(rows);*/
            if (err) {
                console.log(err);
              
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else if (rows[0] == ''){
              res.status(200).json({"error":0,"data":"Shipment has not started for this order"})    

            }
            else {
                 console.log("ooooooooooo");
                console.log(rows[0]);


                    if(rows !=="undefined" && rows[0] !==undefined){

                    /*console.log('here1',req.body.asset_id===rows[0][0].asset_id);*/
                    if(req.body.asset_id===rows[0][0].asset_id || req.body.shipment_id)
                    {
                        module.exports.make_data(rows[0],req.body.asset_id,function(result){
                            
                            let asset_id=req.body.asset_id
                            req.body.shipment=result;
                            req.body.isAudit=1;
                            console.log("ttttttttttttttttttttttttttttttttttttttttttttt");
                            console.log(result);
                            var array = result.reverse();
                            console.log(array);
                            var group = groupBy(array, ['seller_company_name']);
	                          var new_array = [];
							    var arrayIndex = 0;
							    var kkk = group
							    for (var key in kkk) {
							      if (kkk.hasOwnProperty(key)) {
							        kkk[key].map((val, index) => {
							          console.log(val)
							          new_array[arrayIndex++] = val
							        })
							      }
							    }
							    var new_traversed = new_array.reverse()

                                 res.status(200).send(new_traversed);      


                            next();

                        })
                    }
                }
                    else {
                        console.log("kkkkkkkkkkkkkk");
  		res.status(200).send("No data");    
                        next();
                    }
                    
                                        
            }
        });
        connection.release();
    }
});    
},


forest_company: function(req,res,next){
var sql="CALL "+config.dbname+".usp_get_shipment_trace_for_geo("+req.body.shipment_id+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        console.log(err);
        console.log('err in call shipment trace',err);
        res.status(500).json({"error":0,"data":"Internal server error"});
    } else {
        connection.query(sql,function(err, rows, fields) {
            /*console.log("products");
            console.log(rows);*/
            if (err) {
                console.log(err);
              
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else if (rows[0] == ''){
              res.status(200).json({"error":0,"data":"Shipment has not started for this order"})    

            }
            else {
                 console.log("ooooooooooo");
                console.log(rows[0]);


                    if(rows !=="undefined" && rows[0] !==undefined){

                    /*console.log('here1',req.body.asset_id===rows[0][0].asset_id);*/
                    if(req.body.asset_id===rows[0][0].asset_id)
                    {
                        module.exports.make_data(rows[0],req.body.asset_id,function(result){
                            
                            let asset_id=req.body.asset_id
                            req.body.shipment=result;
                            req.body.isAudit=1;
                            console.log("ttttttttttttttttttttttttttttttttttttttttttttt");
                            console.log(result);
                            var resultData = makeDataforComapanyKey(result);

                            // var array = result.reverse();
                            // console.log(array);
                            // var group = groupBy(array, ['seller_company_name']);
	                        //   var new_array = [];
							//     var arrayIndex = 0;
							//     var kkk = group
							//     for (var key in kkk) {
							//       if (kkk.hasOwnProperty(key)) {
							//         kkk[key].map((val, index) => {
							//           console.log(val)
							//           new_array[arrayIndex++] = val
							//         })
							//       }
							//     }
							//     var new_traversed = new_array.reverse()

                                 res.status(200).send(resultData);      


                            next();

                        })
                    }
                }
                    else {
                        console.log("kkkkkkkkkkkkkk");
                        next();
                    }
                    
                                        
            }
        });
        connection.release();
    }
});    
},





    place_shipment:function(req,res,next){
        /*console.log('req',req.session.usrData);*/
         if(req.body.shipment_id===undefined){
        res.status(200).json({"error":1,"data":"Plese send shipment id"});
    }else{
         async.waterfall([
                
                        function(callback){
                        //req.body.asset_id="ad408d75-d7d0-11e8-b79e-54ee751e298c1";
                        shipmentController.place_shipment(req.body.shipment_id,req.session.usrData.user_id,function(result){
                            if(result.error)
                                callback(result,null);
                            else if(req.body.asset_id===null || req.body.asset_id===undefined)
                                callback({"error":0,"data":"Shipment placed successfully"},null)
                            else{
                                console.log('here333');
                                 callback(null,result);
                             }
                            console.log('here1');
                        });
                }, function(arg1,callback) {
                       console.log('new mine');
                       callback(null,'Done');
                      
                }

                ], function (err, result) {
                if(err)
                {   console.log('in next',err); res.status(200).json(err); 
                    if(!err.error)
                    next();
                }
                else
                    {console.log('heresssssssss');
                        res.status(200).json({"error":0,"data":"Shipment placed successfully"});
                        next(); console.log('here');
                        //console.log(req.body);
                    }

            });
                }
         
            
    },



     register_shipment: function (req,res,next) {
    
        console.log("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");

        console.log('here in register_shipment');
        /*console.log('body',req.body);*/
        Fabric_Client.newDefaultKeyValueStore({ path: store_path
         }).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext('grasim_user', true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded grasim_user from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get grasim_user.... run registerUser.js');
    }


    tx_id = fabric_client.newTransactionID();
       
 var newtype = "docType";
var newvalue = "Shipment" ;
req.body[newtype] = newvalue; 
 var newtype2 = "tx_id";
var newvalue2 =  tx_id._transaction_id;
req.body[newtype2] = newvalue2;
     
    var body = JSON.stringify(req.body);
    console.log("__________________________________________________________________________________");
    console.log(body);

    var asset_id = req.body.asset_id;
    console.log('req.body.asset_id;',req.body.asset_id);
  
    console.log("Assigning transaction_id: ", tx_id._transaction_id);
       
    var request = {     
                chaincodeId,
                txId: tx_id,
                fcn: 'createShipment',
                args: [ asset_id, body]
               
};





//res.status(200).send(req.body).toString(); 
    // send the transaction proposal to the peers
    return channel.sendTransactionProposal(request);
}).then((results) => {
    console.log("result");
    console.log(results);
    var proposalResponses = results[0];
    var proposal = results[1];
    let isProposalGood = false;
    if (proposalResponses && proposalResponses[0].response &&
        proposalResponses[0].response.status === 200) {
            isProposalGood = true;
            console.log('Transaction proposal was good');
        } else {
            console.error('Transaction proposal was bad');
        }
    if (isProposalGood) {
        console.log(util.format(
            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
            proposalResponses[0].response.status, proposalResponses[0].response.message));

        // build up the request for the orderer to have the transaction committed
        var request = {
            proposalResponses: proposalResponses,
            proposal: proposal
        };

        // set the transaction listener and set a timeout of 30 sec
        // if the transaction did not get committed within the timeout period,
        // report a TIMEOUT status
        var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
        var promises = [];

        var sendPromise = channel.sendTransaction(request);
        promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

        // get an eventhub once the fabric client has a user assigned. The user
        // is required bacause the event registration must be signed
        let event_hub = channel.newChannelEventHub();
        event_hub.setPeerAddr('grpc://localhost:7053');

        // using resolve the promise so that result status may be processed
        // under the then clause rather than having the catch clause process
        // the status
        let txPromise = new Promise((resolve, reject) => {
            let handle = setTimeout(() => {
                event_hub.disconnect();
                resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
            }, 3000);
            event_hub.connect();
            event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                // this is the callback for transaction event status
                // first some clean up of event listener
                clearTimeout(handle);
                event_hub.unregisterTxEvent(transaction_id_string);
                event_hub.disconnect();

                // now let the application know what happened
                var return_status = {event_status : code, tx_id : transaction_id_string};
                if (code !== 'VALID') {
                    console.error('The transaction was invalid, code = ' + code);
                    resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                } else {
                    console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr())
                    resolve(return_status);
                }
            }, (err) => {
                //this is the callback if something goes wrong with the event registration or processing
                reject(new Error('There was a problem with the eventhub ::'+err));
            });
        });
        promises.push(txPromise);

        return Promise.all(promises);
    } else {
        test = 1;
        console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');   
    }
}).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
    // check the results in the order the promises were added to the promise all list
    if (results && results[0] && results[0].status === 'SUCCESS') {
        console.log('Successfully sent transaction to the orderer.');
    } else {
        console.error('Failed to order the transaction. Error code: ' + response.status);
    }

    if(results && results[1] && results[1].event_status === 'VALID') {
        console.log('Successfully committed the change to the ledger by the peer');
    } else {

        console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
    }next();
}).catch((err) => {
    //next();
    console.log("errrpppppppppppppp");
    console.log(err);
    //console.error('Failed to invoke successfully :: ' + err);
    if(test == 1){
        console.log(err);
        console.log("in if")
        test = 0;
        res.status(200).json({"error":1,"data":"Please contact admin.Blockchain server is down."});
    }
    else{
        console.log("in else");
        next();
    }
});
        // body...
    },
    make_data:function(shipments,asset_id,callback){

        var arr=[];
        var map={};

        for( var i=0;i<shipments.length;i++){
        var temp={};
        var product={};
            if(map[shipments[i].shipment_id]===undefined){
                temp=shipments[i];
                temp.product=[];
                temp.asset_id=asset_id;
                product.product_id=shipments[i].product_id;
                product.product_name=shipments[i].product_name;
                product.product_description=shipments[i].product_description;
                product.product_qty=shipments[i].product_qty;
                product.product_uom=shipments[i].product_uom;
                let cnt=map[shipments[i].shipment_id];
                temp.product.push(product)
                arr.push(temp);
                map[shipments[i].shipment_id]=arr.length-1;
                console.log(map[shipments[i].shipment_id])
            }else{
                //console.log(map);
                product.product_id=shipments[i].product_id;
                product.product_name=shipments[i].product_name;
                product.product_description=shipments[i].product_description;
                product.product_qty=shipments[i].product_qty;
                product.product_uom=shipments[i].product_uom;
                let cnt=map[shipments[i].shipment_id];
                //console.log(arr[cnt]);
                arr[cnt].product.push(product);
            }
            /*if(arr.length===shipments.length)
            callback(arr);*/
        }
        callback(arr)       
    },

    make_fibre_data:function(shipments,asset_id,callback){

        var arr=[];
        var map={};

        for( var i=0;i<shipments.length;i++){
        if (shipments[i].seller_company_type_name === "Birla Cellulose"){
        var temp={};
        var temp2 = {};
        var product={};
        var fibre = {};
            if(map[shipments[i].shipment_id]===undefined){
                temp2=fibre[i]
                temp=shipments[i];
                temp.product=[];

             //var type= shipments[i].product_name.slice(0, shipments[i].product_name.indexOf("Fibre")); 
             //console.log(type);
             console.log("in 1");

             console.log(shipments[i].product_name);
                arr.push(shipments[i].product_name);
                map[shipments[i].shipment_id]=arr.length-1;
               
            }else{
                console.log("in here ");
     //var type= shipments[i].product_name.slice(0, shipments[i].product_name.indexOf("Fibre")); 
             //console.log(type);
                
                arr.push(shipments[i].product_name);
                map[shipments[i].shipment_id]=arr.length-1;

            }
 
        }
      
    }
        callback(arr)       
    },


get_fibre_detail:function(req,res,next){
      let asset_id=(req.body.asset_id===undefined)?"":req.body.asset_id;
 database.connection.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
            res.status(200).json({"error":1,"data":resMsg.serverError});
        } else {
           console.log(req.body.shipment_id);
            console.log(req.body.company_name);
           var sql="CALL "+config.dbname+".usp_get_certificate_trace("+req.body.shipment_id+")";
            connection.query(sql,function(err, rows, fields) {
                if (err) {
                    console.log(err);
                    logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                    res.status(200).json({"error":1,"data":resMsg.sqlError});
                }else { 
                 /*   console.log("here6666");
                    //console.log(rows[0]);
                   console.log(asset_id);
                   console.log("__________________________________________________________________________________");
                   console.log(rows[0]);*/
                        module.exports.make_fibre_data(rows[0],asset_id,function(result){
                            console.log("::::::");
                            //let asset_id=req.body.asset_id
                            req.body.shipment=result;
                            req.body.isAudit=1;
                            //console.log(typeof(result));
                            //console.log("ttttttttttttttttttttttttttttttttttttttttttttt");
                            /*console.log(typeof(result))
                            console.log(result);*/
                            let data = []
                            result.map((val, index) => {
                                // console.log(val);
                                if(data.indexOf(val)<0){
                                    data.push(val)
                                }
                            })
                            console.log(data);
                                 res.status(200).send(data);                                  
                         

                        })
                  
                }
            });
            connection.release();
        }
    });
},


    audittrail:function(req,res,next){
        console.log(req.body.asset_id);
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
        }).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext('grasim_user', true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded grasim_user from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get grasim_user.... run registerUser.js');
    }

    
    const request = {
        //targets : --- letting this default to the peers assigned to the channel
        chaincodeId,
        fcn: 'auditquery',  
        args: [req.body.asset_id]
    };

    // send the query proposal to the peer
    return channel.queryByChaincode(request);
}).then((query_responses) => {
    console.log("Query has completed, checking results");
    // query_responses could have more than one  results if there multiple peers were used as targets
    if (query_responses && query_responses.length == 1) {
        if (query_responses[0] instanceof Error) {
            res.status(200).json({"error":0,"data":"Shipment has not started for this order"})
            
        } else {
                        let data=JSON.parse(query_responses[0]);
                    
                    //console.log(data[data.length-1].value.shipment);
                    /*let traderData=data[data.length-1].value.shipment;
                    traderData.map((data1,idx)=>{
                        //console.log(idx);
                        //console.log(data1);
                        //console.log(traderData[idx].buyer_company_type_name);
                        //console.log(traderData[idx].buyer_company_type_name=="Trader");
                        if(traderData[idx].seller_company_type_name == "Trader"){

                            if(traderData[idx].buyer_company_type_name == "Grey Fabric Manufacturer"){
                                traderData[idx].seller_company_type_name = "SPGTRGFM";
                                //console.log("in spinner");
                                console.log(traderData[idx].buyer_company_type_name);
                        }
                        else if(traderData[idx].buyer_company_type_name == "Finish Fabric Manufacturer"){
                            traderData[idx].seller_company_type_name = "GFMTRFFM";
                        }
                        else if(traderData[idx].buyer_company_type_name == "Garment Manufacturer"){
                            traderData[idx].seller_company_type_name = "FFMTRGM";
                        }
                    }
                    })
                    console.log("traderData");
                    data[data.length-1].value.shipment=traderData;
                        console.log(traderData);*/
                        var arr = data[data.length-1].value.shipment;
                        var revArray = arr.reverse();
                        data[data.length-1].value.shipment = revArray;
                        res.status(200).send(data[data.length-1])//.toString();
                        
                        //res.status(200).send(data);
            //res.send("Response is ", query_responses[0].toString());
        }
    } else {
        console.log("No payloads were returned from query");
    }
}).catch((err) => {

if (req.body.order_id == undefined){
    console.log("in public ");
    res.status(200).json({"error":1,"data":"Shipment has not started for this order"})

}
else{

    console.log(req.body.order_id);
var sql="CALL "+config.dbname+".usp_get_order_trace("+req.body.order_id+")";
database.connection.getConnection(function(err, connection) {
    if (err) {
        console.log('err in call order trace',err);
        res.status(500).json({"error":0,"data":"Internal server error"});
    } else {
        connection.query(sql,function(err, rows, fields) {
             if (err) {
                console.log(err);
                res.status(500).json({"error":0,"data":"Sql Error "});
            }else {
                 if(rows !=="undefined" && rows[0] !==undefined){

                    var dataObj={};
                    dataObj.orderDetail=rows[0];
                    dataObj.is_Order_Trace=1;

                    console.log(dataObj);
                    res.status(200).send(dataObj);   
                                        
                       }
                    else {
                        console.log("kkkkkkkkkkkkkk");
                        next();
                    }
                    
                                        
            }
        });
        connection.release();
    }
}); 

}


    console.error('Failed to query successfully :: ' + err);
});
    }
,


register_shipment_update: function (req,res,next) {
    
        console.log("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");

        console.log('here in register_shipment');
        /*console.log('body',req.body);*/
        Fabric_Client.newDefaultKeyValueStore({ path: store_path
         }).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext('grasim_user', true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded grasim_user from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get grasim_user.... run registerUser.js');
    }


    tx_id = fabric_client.newTransactionID();
       
 var newtype = "docType";
var newvalue = "Shipment" ;
req.body[newtype] = newvalue; 
 var newtype2 = "tx_id";
var newvalue2 =  tx_id._transaction_id;
req.body[newtype2] = newvalue2;
     
    var body = JSON.stringify(req.body);

    var asset_id = req.body.asset_id;
    console.log('req.body.asset_id;',req.body.asset_id);
  
    console.log("Assigning transaction_id: ", tx_id._transaction_id);
       
    var request = {     
                chaincodeId,
                txId: tx_id,
                fcn: 'createShipment',
                args: [ asset_id, body]
               
};





//res.status(200).send(req.body).toString(); 
    // send the transaction proposal to the peers
    return channel.sendTransactionProposal(request);
}).then((results) => {
    /*console.log("result");
    console.log(results);*/
    var proposalResponses = results[0];
    var proposal = results[1];
    let isProposalGood = false;
    if (proposalResponses && proposalResponses[0].response &&
        proposalResponses[0].response.status === 200) {
            isProposalGood = true;
            console.log('Transaction proposal was good');
        } else {
            console.error('Transaction proposal was bad');
        }
    if (isProposalGood) {
        console.log(util.format(
            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
            proposalResponses[0].response.status, proposalResponses[0].response.message));

        // build up the request for the orderer to have the transaction committed
        var request = {
            proposalResponses: proposalResponses,
            proposal: proposal
        };

        // set the transaction listener and set a timeout of 30 sec
        // if the transaction did not get committed within the timeout period,
        // report a TIMEOUT status
        var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
        var promises = [];

        var sendPromise = channel.sendTransaction(request);
        promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

        // get an eventhub once the fabric client has a user assigned. The user
        // is required bacause the event registration must be signed
        let event_hub = channel.newChannelEventHub();
        event_hub.setPeerAddr('grpc://localhost:7053');

        // using resolve the promise so that result status may be processed
        // under the then clause rather than having the catch clause process
        // the status
        let txPromise = new Promise((resolve, reject) => {
            let handle = setTimeout(() => {
                event_hub.disconnect();
                resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
            }, 3000);
            event_hub.connect();
            event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                // this is the callback for transaction event status
                // first some clean up of event listener
                clearTimeout(handle);
                event_hub.unregisterTxEvent(transaction_id_string);
                event_hub.disconnect();

                // now let the application know what happened
                var return_status = {event_status : code, tx_id : transaction_id_string};
                if (code !== 'VALID') {
                    console.error('The transaction was invalid, code = ' + code);
                    resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                } else {
                    console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr())
                    resolve(return_status);
                }
            }, (err) => {
                //this is the callback if something goes wrong with the event registration or processing
                reject(new Error('There was a problem with the eventhub ::'+err));
            });
        });
        promises.push(txPromise);

        return Promise.all(promises);
    } else {
        test = 1;
        console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');   
    }
}).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
    // check the results in the order the promises were added to the promise all list
    if (results && results[0] && results[0].status === 'SUCCESS') {
        console.log('Successfully sent transaction to the orderer.');
    } else {
        console.error('Failed to order the transaction. Error code: ' + response.status);
    }

    if(results && results[1] && results[1].event_status === 'VALID') {
        console.log('Successfully committed the change to the ledger by the peer');
    } else {

        console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
    }next();
}).catch((err) => {
    //next();
    console.log("errrpppppppppppppp");
    console.log(err);
    //console.error('Failed to invoke successfully :: ' + err);
    if(test == 1){
        console.log(err);
        console.log("in if")
        test = 0;
        res.status(200).json({"error":1,"data":"Please contact admin.Blockchain server is down."});
    }
    else{
        console.log("in else");
            res.status(200).json({"error":0,"data":"Shipment registered on Blockchain."});
    }
});
        // body...
    },

auditquery:function(asset_id, callback){
        console.log(typeof asset_id);
        //let asset = JSON.stringify(asset_id);
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
        }).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext('grasim_user', true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded grasim_user from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get grasim_user.... run registerUser.js');
    }

    
    const request = {
        //targets : --- letting this default to the peers assigned to the channel
        chaincodeId,
        fcn: 'auditquery',  
        args: [asset_id]
    };
    
    // send the query proposal to the peer
    return channel.queryByChaincode(request);
}).then((query_responses) => {
    console.log("Query has completed, checking results");
    //console.log(JSON.parse(query_responses));
    // query_responses could have more than one  results if there multiple peers were used as targets
    if (query_responses && query_responses.length == 1) {
        if (query_responses[0] instanceof Error) {
            res.status(200).json({"error":0,"data":"Shipment has not started for this order"})
            //console.error("error from query = ", query_responses[0]);
        } else {
                        let data=JSON.parse(query_responses[0]);
                    
                    //console.log(data[data.length-1].value.shipment);
                    /*let traderData=data[data.length-1].value.shipment;
                    traderData.map((data1,idx)=>{
                        //console.log(idx);
                        //console.log(data1);
                        //console.log(traderData[idx].buyer_company_type_name);
                        //console.log(traderData[idx].buyer_company_type_name=="Trader");
                        if(traderData[idx].seller_company_type_name == "Trader"){

                            if(traderData[idx].buyer_company_type_name == "Grey Fabric Manufacturer"){
                                traderData[idx].seller_company_type_name = "SPGTRGFM";
                                //console.log("in spinner");
                                console.log(traderData[idx].buyer_company_type_name);
                        }
                        else if(traderData[idx].buyer_company_type_name == "Finish Fabric Manufacturer"){
                            traderData[idx].seller_company_type_name = "GFMTRFFM";
                        }
                        else if(traderData[idx].buyer_company_type_name == "Garment Manufacturer"){
                            traderData[idx].seller_company_type_name = "FFMTRGM";
                        }
                    }
                    })
                    console.log("traderData");
                    data[data.length-1].value.shipment=traderData;
                        console.log(traderData);*/
                        var arr = data[data.length-1].value.shipment;
                        var revArray = arr.reverse();
                        data[data.length-1].value.shipment = revArray;
console.log("hhhhhhhhhhh");
                        callback(data[data.length-1])//.toString();
                        /*console.log("data sent");
                        console.log(data[data.length-1]);*/
                        //res.status(200).send(data);
            //res.send("Response is ", query_responses[0].toString());
        }
    } else {
        console.log("No payloads were returned from query");
    }
}).catch((err) => {
    console.log(err);
    callback("Shipment has not started for this order")
    console.error('Failed to query successfully :: ' + err);
});
    }



}


function makeDataforComapanyKey(value){
    console.log(value)
    var companyKeyId = '';

    async.waterfall([
        function(callback){
            value.map((val, index) => {
                if(val.seller_company_type_name === "Forest"){
                    companyKeyId = val.seller_company_key_id
                    callback(companyKeyId)
                }
                if(index === val.length-1 && companyKeyId === ''){
                    callback('')
                }
            })
        },
        function (arg, callback){
            console.log(arg);
            return arg;
        }
    ])
  
    
}


