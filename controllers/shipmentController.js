var database = require('../Database/database');
var config=require('../config/config');
var emailController=require('./emailController');
var async=require('async');
module.exports={
	send_email:function(req,res,next){
		async.waterfall([
			function(callback){
				let data={};
				data.user_action=true;
	        data.usrData=req.session.usrData;
	        data.company_key_id=req.session.usrData.company_key_id;
	        data.recv_company_key_id=(req.body.recv_company_key_id===undefined)?1:req.body.recv_company_key_id;
	        data.action="placed";
	        data.subject="Shipment placed : invoice number-"+req.body.invoice_number;
	        data.procedure=".usp_get_shipment_emailusers";
	        data.id=req.body.shipment_id;
	        data.invoice_number=req.body.invoice_number;
	        data.user_type="Buyers";
	        if(req.body.isEmail!==0){
	        	emailController.create_mail(data,function(result){
		          //  console.log('result',result);
		            /*if(req.body.isAudit)
		            	next();*/
		            if(req.body.with_alert==1)
		            	callback(null,result)
		            else
		            	callback(result,false)		
	        	})
	        }

			},function(arg,callback){
				let data={};
				data.ship_with_alert=true;
			data.usrData=req.session.usrData;
	        data.company_key_id=req.session.usrData.company_key_id;
	        data.subject="Shipment placed : invoice number-"+req.body.invoice_number;
	        data.companyData={};
	        data.user_type="Buyers";
	        data.companyData.recv_email="birlacoc@adityabirla.com";
	        module.exports.view_shipment(req.body.shipment_id,function(result){
	        	//console.log('at 41',result);
	        	if(!result.error){
	        		data.reciever_company=result.shipmentDetail.buyer_company_name;
		        	data.invoice_number=result.shipmentDetail.invoice_number;
		        	data.order_number=result.shipmentDetail.order_number;
		        	data.company_name=result.shipmentDetail.seller_company_name;
		        	//console.log('order_id',result.shipmentDetail.order_id);
		        	module.exports.validate_qty(req.body.shipment_id,result.shipmentDetail.order_id,req.session.usrData.company_key_id,function(msgResult){
		        	//	console.log('result for validation qty',msgResult.data.b_message);
		        		if(msgResult.data.b_message!==null && msgResult.data.b_message.indexOf('lower')!==-1)
		        			data.msg="lower quantity than expected";
		        		else
		        			data.msg="higher quantity than expected";
		        		emailController.send_email(data,function(rslt){
		        			//console.log(rslt);
		        			/*if(req.body.isAudit)
		        			next();*/
		        			callback(null,rslt)

		        		})
		        	})
	        	}	
	        })
			}
			],function(err,result){
				console.log("$$$$$$$$$$$$$$$$$$$$$");
				if(req.body.isAudit)
		        			next()
		})
		
	//	console.log('with_alert',req.body.with_alert);
		/*if(req.body.with_alert==1)
		{
			data.ship_with_alert=true;
			data.usrData=req.session.usrData;
	        data.company_key_id=req.session.usrData.company_key_id;
	        data.subject="Shipment placing : invoice number-"+req.body.invoice_number;
	        data.companyData={};
	        data.user_type="Buyers";
	        data.companyData.recv_email="birlacoc@adityabirla.com";
	        module.exports.view_shipment(req.body.shipment_id,function(result){
	        	if(!result.error){
	        		data.reciever_company=result.shipmentDetail.buyer_company_name;
		        	data.invoice_number=result.shipmentDetail.invoice_number;
		        	data.order_number=result.shipmentDetail.order_number;
		        	data.company_name=result.shipmentDetail.seller_company_name;
		        	console.log('order_id',result.shipmentDetail.order_id);
		        	module.exports.validate_qty(req.body.shipment_id,result.shipmentDetail.order_id,req.session.usrData.company_key_id,function(msgResult){
		        		console.log('result for validation qty',msgResult.data.b_message);
		        		if(msgResult.data.b_message.indexOf('lower')!==-1)
		        			data.msg="lower quantity than expected";
		        		else
		        			data.msg="higher quantity than expected";
		        		emailController.send_email(data,function(rslt){
		        			console.log(rslt);
		        			if(req.body.isAudit)
		        			next();
		        		})
		        	})
	        	}	
	        })

		}else{
			data.user_action=true;
	        data.usrData=req.session.usrData;
	        data.company_key_id=req.session.usrData.company_key_id;
	        data.recv_company_key_id=(req.body.recv_company_key_id===undefined)?1:req.body.recv_company_key_id;
	        data.action="placed";
	        data.subject="Shipment placing : invoice number-"+req.body.invoice_number;
	        data.procedure=".usp_get_shipment_emailusers";
	        data.id=req.body.shipment_id;
	        data.invoice_number=req.body.invoice_number;
	        data.user_type="Buyers";
	        if(req.body.isEmail!==0){
	        	emailController.create_mail(data,function(result){
		            console.log('result',result);
		            if(req.body.isAudit)
		            	next();
	        	})
	        }
	        
		}*/
	},
	validate_qty:function(shipment_id,order_id,company_key_id,callback){
		database.connection.getConnection(function(err, connection) {
	        if (err) {
	            console.log(err);
	            callback({"error":1,"data":"Internal server error"});
	        } else {
	            var sql="CALL "+config.dbname+".usp_validate_shipment_qty (?,?,?,@b_error,@b_message,@shipment_product_qty,@expected_consume_product_qty,@consume_productqty); SELECT @b_error as b_error,@b_message as b_message,@shipment_product_qty as shipment_product_qty,@expected_consume_product_qty as expected_consume_product_qty,@consume_productqty as consume_productqty";
	            //console.log(sql);
	            connection.query(sql,[shipment_id,order_id,company_key_id],function(err, rows) {          
	                if (err) {
	                    console.log(err);
	                    callback({"error":1,"data":"Sql error"});
	                } else 
	                {
	                    /*console.log(rows[1]);*/
	                    if(rows[1]!==undefined && rows[rows.length-1][0]['b_error']!==undefined)
	                    { 
	                       callback({"error":0,"data":rows[rows.length-1][0]});
	                    }
	                    else
	                    {
	                        callback({"error":1,"data":"Something went wrong"}); 
	                    }
	                }
	            });
	            connection.release();
	        }
	    });
	},
	add_product:function (shipment_id,product_lot_number,product_id,product_description,product_qty,product_uom,blendpercentage,created_by,callback) {
		database.connection.getConnection(function(err, connection) {
			if(err){
				console.log(err);
				callback({"error":1,"data":"Internal server error"});
			}else
			{	
				var sql="SET @shipment_product_id = 0; CALL "+config.dbname+".usp_add_shipment_product(?,?,?,?,?,?,?,?,@shipment_product_id); SELECT @shipment_product_id as shipment_product_id";
				/*console.log(sql);*/
				connection.query(sql,[shipment_id,product_lot_number,product_id,product_description,product_qty,product_uom,blendpercentage,created_by],function(err, rows) {
					/*console.log(rows);*/
					if(rows[2]!==undefined && rows[2][0]['shipment_product_id']===0){
						callback({"error":0,"data":"Product already added in shipment"});
					}else if (!err) {
                    	callback({"error":0,"data":"Product added successfully","shipment_product_id":rows[2][0]['shipment_product_id']});
                    	connection.release();
            		} else {
            			console.log(err)
                    	callback({"error":1,"data":"Sql error"});
            		}
        		});     
			}			
		});		
	},
	update_product:function (shipment_id,shipment_product_id,product_lot_number,product_id,product_description,product_qty,product_uom,blendpercentage,created_by,callback) {
		database.connection.getConnection(function(err, connection) {
			if(err){
				console.log(err);
				callback({"error":1,"data":"Internal server error"});
			}else
			{
						
				var sql="SET @b_result = 0; CALL "+config.dbname+".usp_update_shipment_product(?,?,?,?,?,?,?,?,?,@b_result); SELECT @b_result as b_result";
				/*console.log(sql);*/
				connection.query(sql,[shipment_id,shipment_product_id,product_lot_number,product_id,product_description,product_qty,product_uom,blendpercentage,created_by],function(err, rows) {
					/*console.log(rows);*/
					if(rows[2]!==undefined && rows[2][0]['b_result']===1){
						callback({"error":0,"data":"Product updated successfully"});
					}else if (!err) {
                    	callback({"error":0,"data":"Duplicate value"});
                    	connection.release();
            		} else {
            			console.log(err)
                    	callback({"error":1,"data":"Sql error"});
            		}
        		});     
			}			
		});		
	},
	update_product_lot:function(product_lot_id,consume_product_qty,user_id,callback){
		database.connection.getConnection(function(err, connection) {
			if(err){
				console.log(err);
				callback({"error":1,"data":"Internal server error"});
			}else
			{
						
				var sql="SET @b_result = 0; CALL "+config.dbname+".usp_update_shipment_product_lot(?,?,?,@b_result); SELECT @b_result as b_result";
				/*console.log(sql);*/
				connection.query(sql,[product_lot_id,consume_product_qty,user_id],function(err, rows) {
					/*console.log(rows);*/
					if (err)
						callback({"error":1,"data":"Sql error"});
					else if(rows[2]!==undefined && rows[2][0]['b_result']===1){
						callback({"error":0,"data":"Lot updated successfully"});
					}else  {
                    	callback({"error":1,"data":"Lot data not updated"});
                    	connection.release();
            		}
        		});     
			}			
		});	
	},

	add_lot:function (shipment_product_id,company_key_id,stock_lot_id,consume_product_qty,created_by,callback) {
		
    var appData={};
   
		database.connection.getConnection(function(err, connection) {
			if(err)
				console.log(err);
			else
				{
					
/*CALL `grasim`.`usp_add_shipment_product_lots`(<{IN shipment_productid INT}>, <{IN company_keyid INT}>, <{IN stock_lotid INT}>, <{IN consume_productqty DECIMAL(18,3)}>, <{IN user_id INT}>, <{OUT shipment_productlot_id INT}>);*/
var sql="SET @shipment_productlot_id = 0; CALL grasim.usp_add_shipment_product_lots('"+shipment_product_id+"',"+company_key_id+","+stock_lot_id+","+consume_product_qty+","+created_by+",@shipment_productlot_id); SELECT @shipment_productlot_id";
/*console.log(sql);*/
	connection.query(sql, function(err, rows) {
	                		if (!err) {
		                    	var tst=rows[2][0];
		                    	/*console.log("shipment_productlot_id ...");
		                    	console.log(tst['@shipment_productlot_id']);*/

		                   		appData.error = 0;
		                    	appData["data"] = "Shipment lot created successfully!";
		                    	callback(appData);
	                		} else {
	                			/*console.log("not adding the lot");
		                    console.log(err);*/
		                    appData["data"] = "Error Occured!";
		                    callback(appData);
	                		}
	            		});     
							
				}
				connection.release();
		});	
		},	
	check_duplicate_shipment:function(company_key_id,invoice_number,shipment_id,callback){
		var sql="SET @b_result = 0; CALL "+config.dbname+".usp_is_invoice_no_duplicate("+company_key_id+",'"+invoice_number+"','"+shipment_id+"',@b_result); SELECT @b_result";
		
	    database.connection.getConnection(function(err, connection) {
	        if (err) {
	            callback({"error":1,"data":"Internal server error"});
	        } else {
	            connection.query(sql, function(err, rows) {
	                if (!err) {
	                    callback({"error":0,"duplicate":rows[2][0]["@b_result"]});
	                } else {
	                    callback({"error":1,"data":"Sql error"});
	                }
	            });
	            connection.release();
	        }
	    }); 
	},
	get_shipment:function(company_key_id,data,procedure,callback){
		console.log('procedure',procedure);
		console.log('data',data);
	    var from_date=data.from_date===undefined?"":data.from_date;
	    var to_date=data.to_date===undefined?"":data.to_date;
	    var search_text=data.search_text===undefined?"":data.search_text;
	    var db_param = data.db_param===undefined?"":data.db_param;
	    var page_size=data.page_size===undefined?10:data.page_size;
	    var page_number=data.page_number===undefined?1:data.page_number;
	    var sql="CALL "+config.dbname+procedure+"(?,?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount";
	    var arr=[company_key_id,from_date,to_date,search_text,page_size,page_number];
	    console.log('arr',arr);
	    database.connection.getConnection(function(err, connection) {
	        if (err) {
	            callback({"error":1,"data":"Internal server error"});
	        } else {
	            connection.query(sql,[company_key_id,from_date,to_date,search_text,db_param,page_size,page_number], function(err, rows, fields) {
	                if (err) {
	                    console.log(err);
	                    callback({"error":1,"data":"Sql error"});
	                }else { 
	                    if(rows===undefined)
	                      {  
	                        callback({"error":1,"data":"No such records"});}
	                    else
	                    {
	                        callback({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
	                    }
	                }
	            });
	            connection.release();
	        }
	    }); 
	},
	check_duplicate_lot:function(shipment_id,product_lot_number,shipment_product_id,callback){
		var sql="SET @b_result = 0; CALL "+config.dbname+".usp_is_shipment_product_lot_no_duplicate(?,?,?,@b_result); SELECT @b_result";
	    database.connection.getConnection(function(err, connection) {
	        if (err) {
	        	console.log(err);
	            callback({"error":1,"data":"Internal server error"});
	        } else {
	            connection.query(sql,[shipment_id,product_lot_number,shipment_product_id], function(err, rows) {
	            	console.log('rows',rows);
	                if (!err) {
	                    callback({"error":0,"duplicate":rows[2][0]["@b_result"]});
	                } else {
	                	console.log(err);
	                    callback({"error":1,"data":"Sql error"});
	                }
	            });
	            connection.release();
	        }
	    });
	},
	place_shipment:function(shipment_id,user_id,callback){
		console.log("on place ");
		var sql="SET @b_result = 0; CALL "+config.dbname+".usp_place_shipment(?,?,@b_result); SELECT @b_result";
		database.connection.getConnection(function(err, connection) {
	        if (err) {
	        	console.log(err);
	            callback({"error":1,"data":"Internal server error"});
	        } else {
	            connection.query(sql,[shipment_id,user_id], function(err, rows) {
	            	console.log('in place_shipment controller',rows);
	                if (!err) {
	                    callback({"error":0,"data":"Shipment placed successfully"});
	                } else {
	                	console.log(err);
	                    callback({"error":1,"data":"Sql error"});
	                }
	            });
	            connection.release();
	        }
	    });
	},
	view_shipment:function(shipment_id,callback){

    var dataObj={};
    async.waterfall([
        function(callback){
        	console.log("in view");
        	console.log(shipment_id);
            let sql="CALL "+config.dbname+".usp_view_shipment("+shipment_id+")";
            database.connection.getConnection(function(err, connection) {
                if (err) {
                    callback("Internal server error",null);
                } else {

                    connection.query(sql, function(err, rows, fields) {
                        if (err) {
                            console.log(err);
                            callback("Sql error",null);
                        }else {
                            if(rows===undefined ||rows[0].length===0)
                            {  console.log("No such record");
                                callback("No such record",null);
                            }
                            else
                            { console.log("view_shipment");
                                dataObj.shipmentDetail=rows[0][0];
                                callback(null,dataObj)
                                connection.release();
                            }
                        }
                    });
                }
            }) 
        },function(arg1,callback){
        	console.log("view_shipment_product");
            let sql="CALL "+config.dbname+".usp_get_shipment_products_by_shipmentid("+arg1.shipmentDetail.shipment_id+")";
            database.connection.getConnection(function(err, connection) {
                if (err) {
                    callback("Internal server error",null);
                } else {
                    connection.query(sql, function(err, rows, fields) {
                        if (err) {
                            console.log(err);
                            callback("Sql error",null);
                        }else {
                            if(rows===undefined ||rows[0].length===0)
                            { console.log("products22222222222222");
                             console.log(rows);
                                arg1.products=[];
                                callback(null,arg1);
                            }
                            else

                            {	console.log("products");
                            	console.log(rows);
                                arg1.products=rows[0];
                                callback(null,arg1);
                                connection.release();
                            }
                        }
                    });
                }
            }) 
        },
        function(arg2,callback){
            let sql="CALL "+config.dbname+".usp_validate_shipment_lot_existence("+arg2.shipmentDetail.shipment_id+",@b_result); SELECT @b_result as b_result";
            database.connection.getConnection(function(err, connection) {
                if (err) {
                    callback("Internal server error",null);
                } else {
                    connection.query(sql, function(err, rows, fields) {
                        console.log(rows[1]);
                        if (err) {
                            console.log(err);
                            callback("Sql error",null);
                        }else {
                            if(rows===undefined ||rows[0].length===0)
                            {  
                                callback("No such record",null);
                            }
                            else
                            {
                                arg2.is_lot=rows[1][0]['b_result'];
                                callback(null,arg2);
                                connection.release();
                            }
                        }
                    });
                }
            }) 
        }
        ],function(err,result){
        	if(err)
        		callback({"error":1,"data":err});
        	else
            	callback(result);
        })
	},
	test:function(data){

		return new Promise(function(resolve, reject){
			console.log(typeof data);
			if(data==1)
				resolve({"working":1});
			else
				reject({"error":1})
		});
	}
}