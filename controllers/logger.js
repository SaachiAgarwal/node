const fs = require('fs');
require('winston-daily-rotate-file');
const logDir = 'log';
var path=require('path');
var winston = require('winston');
if (!fs.existsSync(logDir)) {
  //fs.mkdirSync(path.join(__dirname,'../controllers'+logDir));
  fs.mkdirSync(logDir);
}
winston.loggers.add('logger', {
  transports: [
      //new files will be generated each day, the date patter indicates the frequency of creating a file.
    new winston.transports.DailyRotateFile({
      name: 'debug-log',
      filename: 'debug-log',
      level: 'debug',
      dirname:logDir,
      prepend: true,
      datePattern: 'YYYY-MM-DD',
      maxFiles:'1d'
    }),
    new (winston.transports.DailyRotateFile)({
      name: 'error-log',
      filename: 'error-log',
      level: 'error',
      dirname:logDir,
      prepend: true,
      datePattern: 'YYYY-MM-DD',
      maxFiles:'2d'
    })
  ]
});
var logger = winston.loggers.get('logger');
module.exports=logger;