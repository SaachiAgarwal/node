var jwt=require('jsonwebtoken');
module.exports={
	token_auth:function(req,res,next){
		console.log(req.url);
		console.log('session detail ', req.session);
		if(req.session.usrData===undefined){
			res.status(200).json({"error":1,"data":"Please login to continue"});
		}else{
			var token = req.body.token || req.headers['token'];
		    if (token) {
		        jwt.verify(token, process.env.SECRET_KEY, function(err,dt) {
		            if (err) {
		                res.status(200).json({"data":"Invalid token"});
		            } else {
		                next();
		            }
		        });
		    } else {
		        res.status(200).json({"error":1,"data":"Please send a token"})
		    }
		}
	}	
}
