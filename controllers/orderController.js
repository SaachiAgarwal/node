var database = require('../Database/database');
var config=require('../config/config');

function updateProduct(order_id,company_key_id,modified_by,data){
	console.log('here');
	console.log(data.order_product_id);
	if(data.order_product_id===undefined)
	{
		var sql="SET @b_result = 0; CALL "+config.dbname+".usp_add_order_product('"+order_id+"',"+company_key_id+",'"+data.product_name+"',"+data.product_category_id+","+data.product_type_id+","+data.product_qty+",'"+data.product_uom+"','"+data.blendpercentage+"',"+modified_by+",@b_result); SELECT @b_result as b_result";
		console.log('in if');
	}else{	
		var sql="SET @b_result= 0; CALL "+config.dbname+".usp_update_order_product("+data.order_product_id+","+company_key_id+",'"+data.product_name+"',"+data.product_category_id+","+data.product_type_id+","+data.product_qty+",'"+data.product_uom+"','"+data.blendpercentage+"',"+modified_by+",@b_result); select @b_result as b_result";
		console.log('in else');
	}	
	console.log(sql);
	return new Promise(function(resolve, reject){
		database.connection.getConnection(function(err, connection) {
			if(err)
				reject("error")
			else
			{
				
				connection.query(sql, function(err, rows) {
            		if (err) {
            			console.log(err);
                    	resolve({"error":1,"data":data.product_name});
            		} else{
            			console.log('in call');
				console.log(sql);
            			console.log(rows);
            			console.log('in resolve');
            			console.log(rows[2][0]['b_result']);
            			if(rows[2][0]['b_result'])
            				resolve();
            			else
            				resolve({"error":1,"data":data.product_name})
            		}
        		}); 
				console.log(data);
			}
				connection.release();
		});

	});
}
function update_process_loss(id,value,name){
	return new Promise(function(resolve, reject){
		database.connection.getConnection(function(err, connection) {
	        if (err) {
	            res.status(200).json({"error":1,"data":resMsg.serverError});
	        } else {
	            var sql="CALL "+config.dbname+".usp_update_defined_process_loss(?,?)"; 
	            connection.query(sql,[id,value], function(err, rows, fields) {
	                if (err) {
	                    reject({"error":1,"data":name+" not updated"})
	                }else{
	                    resolve({"error":0,"data":"Process loss data updated"});
	                }
	            });
	            connection.release();
	        }
	    });
    });
}
module.exports={
	
	add_product:function (company_key_id,created_by,product,callback) {
		
		var product_description=product.product_description===undefined?'':product.product_description;
		var product_id=product.product_id===undefined?'':product.product_id;
		var blendpercentage=product.blend_percentage===undefined?100:product.blend_percentage;
		var glm=(product.glm===undefined)?'':product.glm;
		var mpg=(product.mpg===undefined)?'':product.mpg;
		
		database.connection.getConnection(function(err, connection) {
			if(err)
				reject("error")
			else
			{
				var sql="SET @b_result = 0; CALL "+config.dbname+".usp_add_order_product(?,?,?,?,?,?,?,?,?,?,?,@b_result); SELECT @b_result";
				console.log("%%%%%%%%%%%%%%%%%%%%");
				console.log(glm);
				console.log(product.glm);
				console.log(sql);
				connection.query(sql,[product.order_id,company_key_id,product.product_name,product_description,product_id,product.product_qty,product.product_uom,glm,mpg,blendpercentage,created_by],function(err, rows) {
            		if (err) {
            			console.log(sql);
            			console.log("------------------------");
            			console.log(blendpercentage);
            			console.log(err);
                    	callback({"error":1,"data":"Product data not saved"});
            		} else{
            			console.log(rows);
            			
            			if(rows[2][0]['@b_result']===1)
                        { 
                           callback({"error":0,"data":"Product saved successfully"});
                        }
                           else if(rows[2][0]['@b_result']===-1)
                        {
                             callback({"error":1,"data":"Product already added in this order "});  
                        }
                        else 
                        {
                             callback({"error":1,"data":"Product does not exist"});  
                        }
            		}
        		}); 
			}
			connection.release();
		});
	},
	update_product:function (order_id,company_key_id,modified_by,products,callback) {
		console.log(products);
		let promiseArr = [];
		products.map((data)=> {
     		console.log(data);
         		promiseArr.push(updateProduct(order_id,company_key_id,modified_by,data)); 
     		
		}); 
		Promise.all(promiseArr)
		.then((result) => {console.log(result);callback(result)})
		.catch((err) => callback(err));
	},
	check_duplicate_order:function(company_key_id,order_number,order_id,callback){
		var appData={};
		var sql="SET @b_result = 0; CALL "+config.dbname+".usp_is_order_no_duplicate(?,?,?,@b_result); SELECT @b_result";
		console.log(sql);
	    database.connection.getConnection(function(err, connection) {
	        if (err) {
	            callback({"error":1,"data":"Internal server error"});
	        } else {
	            connection.query(sql,[company_key_id,order_number,order_id],function(err, rows) {
	                if (!err) {
	                    callback({"error":0,"duplicate":rows[2][0]['@b_result']});
	                } else {
	                    callback({"error":1,"data":"Sql error"});
	                }
	            });
	            connection.release();
	        }
	    });
	},
	update_process_loss:function(process_data,callback){
		let promiseArr=[];
		process_data.map((data)=>{
			promiseArr.push(update_process_loss(data.process_loss_id,data.process_loss,data.process_name))
		});
		Promise.all(promiseArr).then((result)=>{callback({"error":0,"data":"Process loss data updated"})}).catch((err)=>{callback(err)});
	}
}