	
var mysql = require('mysql');
var config= require('../config/config');
console.log(config.dbname);
var connection = mysql.createPool({
    connectionLimit: config.connectionLimit,
    host:config.host,
    user:config.user,
    password:config.password,
    database:config.dbname,
    port: config.port,
    debug: false,
    multipleStatements: true,
    timezone:'UTC'
});


module.exports.connection = connection;