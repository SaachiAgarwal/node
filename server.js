var express = require('express');
var cors = require('cors');
var bodyParser = require("body-parser");
var session=require("express-session");
var async=require('async');
var app = express();
var http = require('http');
var path = require('path');
var jwt = require('jsonwebtoken');
var validator=require('express-validator');
var database = require('./Database/database');
var token;
var bcrypt = require('bcrypt');
var port = process.env.PORT || 3002;
var auth=require('./controllers/auth');
var config=require('./config/config');
var audit=require('./controllers/audit');
var logger=require('./controllers/logger');
const mysql      = require('mysql');
var moment = require('moment');
const https = require('https');
const fs = require('fs');
var emailController=require('./controllers/emailController');
const options = {
  key: fs.readFileSync('/home/digital/digitalrepo/localhost.key'),
  cert: fs.readFileSync('/home/digital/digitalrepo/localhost.crt')
};


app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(validator());
app.use(session({ secret: 'lostmymarbles', resave: true, saveUninitialized: true }));
var router = require('./routes/Users');
var orders = require('./routes/orders');
var masters = require('./routes/mastersRoutes');
var shipment = require('./routes/stocks');
var admin = require('./routes/admin');
var reports = require('./routes/reports');
var csv=require('./routes/csv_upload');
app.post('/users/authenticate', function(req, res) {
console.log("in auth")
	database.connection.getConnection(function(err, connection) {
		console.log(err);
        if(err)
        {
        	console.log(err);
            res.json({"error":1,"data":"Internal server error"});
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
        }    
        else{
            async.waterfall([
		        function(callback) {
		            connection.query('SELECT * FROM users WHERE email = ? && is_deleted != 1', [req.body.email], function(err, rows, fields){
		                if(err || rows.length===0 || rows.length===undefined)
		                    callback("User id not found",null);
		                else
		                    callback(null,rows[0])
		            })            
		        },
		        function(arg1,callback) {
		            if (bcrypt.compareSync(req.body.password, arg1.password)) {
		            	delete arg1['password'];
		                var token = jwt.sign(arg1, process.env.SECRET_KEY, {
		                    expiresIn: "4h"
		                });
		                callback(null,token,arg1);
		            } else {
		                callback('Wrong password',null)
		            }
		        },
		        function(arg2,arg3, callback) {
		            var sql='CALL usp_get_user_details ('+arg3.user_id+')';
		            
		            connection.query(sql,function(err,rows,fields){
		                if(err)
		                    callback('Unable to fetch user info',null);
		                else
		                { 
		                	req.session.usrData=rows[0][0];
		                	req.session.usrData.user_id=arg3.user_id;
		                	            	
		                    callback(null,{"token":arg2,"user_info":rows[0][0]});
		                }
		            });
		        }
	    	], function (err, result) {
	        	if(err)
	            	res.json({"error":1,"data":err});
	        	else
	            	res.json(result);
    		});
        }
    });   
});
app.post('/open_api/generate_token', function(req, res) {
	database.connection.getConnection(function(err, connection) {
		console.log(err);
        if(err)
            res.json({"error":1,"data":"Internal server error"});
        else{
            async.waterfall([
		        function(callback) {
		            connection.query('SELECT * FROM users WHERE email = ?', [req.body.email], function(err, rows, fields){
		                if(err || rows.length===0 || rows.length===undefined)
		                    callback("User id not found",null);
		                else
		                    callback(null,rows[0])
		            })            
		        },
		        function(arg1,callback) {
		            if (bcrypt.compareSync(req.body.password, arg1.password)) {
		            	delete arg1['password'];
		            	arg1.open_api=true;
		                var token = jwt.sign(arg1, process.env.SECRET_KEY, {
		                    expiresIn: 86400
		                });
		                callback(null,token);
		            } else {
		                callback('Wrong password',null)
		            }
		        }
	    	], function (err, result) {
	        	if(err)
	            	res.json({"error":1,"data":err});
	        	else
	            	res.json(result);
	            connection.release();
    		});
        }
    });   
});

app.post('/users/open_api', function(req, res) {
                database.connection.getConnection(function(err, connection) {
                                console.log(err);
        if(err)
        {
               console.log(err);
            res.json({"error":1,"data":"Internal server error"});
            logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
        }    
        else{
            async.waterfall([
                                        function(callback) {
                                               console.log(req.body.email);
                                            connection.query('SELECT * FROM users WHERE email = ? && is_deleted != 1', [req.body.email], function(err, rows, fields){
                                                if(err || rows.length===0 || rows.length===undefined)
                                                    callback("User id not found",null);
                                             else{
                                                                if (bcrypt.compareSync(req.body.password, rows[0].password)) {
                                                                               
                                                                               var pwd = JSON.parse('{ "password":"rows[0].password" }');
                                                               
                                                                                               var token = jwt.sign(pwd, process.env.SECRET_KEY, {
                                                                               expiresIn: "4h"
                                                                                                });
                                                                                              //req.body.token = token;
                                                                                callback(null,token , rows[0])
                                                                  } else {
                                                                                               callback('Wrong password',null)
                                            }
                                                }
                                            })            
                                        },
                                                        function(arg1,arg2,callback) {
                                            var sql='CALL usp_get_user_details ('+arg2.user_id+')';
                                            
                                            connection.query(sql,function(err,rows,fields){
                                                if(err)
                                                    callback('Unable to fetch user info',null);
                                                else
                                                { 
                                                              req.session.usrData=rows[0][0];
                                                              req.session.usrData.user_id=arg2.user_id;
                                                             callback(null,{"user_id":req.session.usrData.user_id,"company_key_id":req.session.usrData.company_key_id});                        
                                                }
                                            });
                                        },
function(arg1 ,arg2 , callback) {
  // const purchase_order = req.body.purchase_order;
  // const order_date = req.body.order_date;
  // const order_expected_date = req.body.order_expected_date;
  // const supplier_company_name = req.body.supplier_company_name;
  // const order_description = req.body.order_description;
  // const product = req.body.product;
  // const product_description = req.body.product_description;
  // const quantity = req.body.quantity;
  // const unit = req.body.unit;
  // const blend_percentage = req.body.blend_percentage;
  // const link_purchase_order = req.body.link_purchase_order;
  // const glm = req.body.glm;
  // const mpg = req.body.mpg

    var name;
  
    console.log('type', req.body.type);
    console.log('files', req.files);
     'use strict';
    const dbhost = config.host;
    const dbuser = config.user;
    const dbpass = config.password;
    const dbname = config.dbname;
    var tblnm, procedure, procedure2;
    tblnm = 'order_upload';
    procedure = 'usp_upload_order_data';
    msg = "Order uploaded successfully!"
    if(req.body.type==='Shipment' || req.body.type==='shipment'){
      console.log("shipment--------------------------------")
        tblnm= 'shipment_upload';
        procedure='usp_upload_shipment_data';
        msg="Order uploaded successfully!"
    }else if(req.body.type==='Order'||req.body.type==='order' ){
      console.log("__________________________________________");
        tblnm= 'order_upload';
        procedure='usp_upload_order_data';
        msg="Order uploaded successfully!"
    }
    //else if(req.body.type==='Stocks'){
    //     tblnm= 'stock_upload';
    //     procedure='usp_upload_stock_data';
    //     procedure2 ='usp_update_0_day_stock_status';
    //     msg="Stock uploaded successfully!"
    // }
    const created_by = req.session.usrData.user_id;
    // const csvfn = path.join( __dirname ,'../uploads/')+name;
  
  
  
    return new Promise((resolve, reject) => {
  
      const context = mysql.createConnection({
        host: dbhost,
        user: dbuser,
        password: dbpass,
        database: dbname
      });
  
      context.connect((err) => {
        if (err) {
          console.error('error connecting: ' + err.stack);
          console.log("here")
          reject(err);
        } else {
          resolve(context);
        }
      });
    })
      .then(context => {
        return new Promise((resolve, reject) => {
          var fields = '';
          var fieldnms = '';
          var qs = '';
          console.log(req.session.usrData.company_type_name);
         

  
          if(req.body.type==='Order' || req.body.type==='order'){

            if (req.session.usrData.company_type_name == 'Garment Manufacturer'){
                fieldnms = "purchase_order, order_date, order_expected_date, supplier_company_name, order_description, product, product_description, quantity, unit, glm, mpg ,blend_percentage, link_purchase_order"
                fields = "purchase_order TEXT, order_date TEXT, order_expected_date TEXT, supplier_company_name TEXT, order_description TEXT, product TEXT, product_description TEXT, quantity TEXT, unit TEXT, glm TEXT , mpg TEXT, blend_percentage TEXT, link_purchase_order TEXT"
                qs = "?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?"
            }
            else{
            
                fieldnms = "purchase_order, order_date, order_expected_date, supplier_company_name, order_description, product, product_description, quantity, unit, blend_percentage, link_purchase_order"
                fields = "purchase_order TEXT, order_date TEXT, order_expected_date TEXT, supplier_company_name TEXT, order_description TEXT, product TEXT, product_description TEXT, quantity TEXT, unit TEXT, blend_percentage TEXT, link_purchase_order TEXT"
                qs = "?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?"
                }
        }
        else if(req.body.type==='Shipment' || req.body.type==='shipment'){
	  if (req.session.usrData.company_type_name == 'Pulp'){
	  fieldnms = "invoice_number, invoice_date, buyer_name, product, product_description, product_quantity, product_unit, product_lot_number, seller_unit, buyer_unit,transport_mode, shipment_description, buyer_order_number, received_invoice_number, supplier_name, consumed_product, consumed_quantity, consumed_unit"
          fields = "invoice_number TEXT, invoice_date TEXT, buyer_name TEXT, product TEXT, product_description TEXT, product_quantity TEXT, product_unit TEXT, product_lot_number TEXT, seller_unit TEXT, buyer_unit TEXT,transport_mode TEXT,shipment_description TEXT, buyer_order_number TEXT"
          qs = "?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?"

		}

	else{

	  fieldnms = "invoice_number, invoice_date, buyer_name, product, product_description, product_quantity, product_unit, product_lot_number, seller_unit, buyer_unit,transport_mode, shipment_description, buyer_order_number, received_invoice_number, supplier_name, consumed_product, consumed_quantity, consumed_unit"
          fields = "invoice_number TEXT, invoice_date TEXT, buyer_name TEXT, product TEXT, product_description TEXT, product_quantity TEXT, product_unit TEXT, product_lot_number TEXT, seller_unit TEXT, buyer_unit TEXT,transport_mode TEXT,shipment_description TEXT, buyer_order_number TEXT, received_invoice_number TEXT, supplier_name TEXT, consumed_product TEXT, consumed_quantity TEXT, consumed_unit TEXT"
          qs = "?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?"
		}

          

        }
  
          fields += `, created_by TEXT`;
          fieldnms += `,  created_by`;
          qs += ',' + created_by;
  
          context.qs = qs;
          context.fieldnms = fieldnms;
          console.log("Data Enetred---------------");
       
          console.log('tblname', tblnm);
          console.log(`about to create CREATE TABLE IF NOT EXISTS ${tblnm} ( ${fields} )`);
          context.query(`CREATE TABLE IF NOT EXISTS ${tblnm} ( ${fields} )`,
            [],
            err => {
              console.log(err);
              if (err) reject(err);
              else resolve(context);
            })
        });
      })
      .then(context => {
        return new Promise((resolve, reject) => {
      
          var d = []
  
          for (var key in req.body) {
            if (key !== "token" && key !== "email" && key !== "password" && key !== "type" ) {
              let value = req.body[key];
              console.log(`value for ${key} is ${value}`)
              if(value===""){
                d.push(null)
              }
              else if(key === "order_date" || key === "order_expected_date" || key === "invoice_date"){
                value = value.trim();
                var date = moment(value, 'DD-MM-YYYY');//, 'DD/MM/YYYY');
                // var train_date = date.format('ll'); 
                if (date.isValid()) {
  
                  var train_date = date.format('DD-MM-YYYY');
                  //var new_train_date = moment(train_date);
                  //if (new_train_date.isValid()){
                  value = train_date;
                  // }
                  console.log(train_date);
                  d.push(value)
                }
              }
              else{
                d.push(value.trim())
              }
            }
          }
         
          console.log(d)
          if (d.length > 0) {
               console.log(context.fieldnms);
               console.log(context.qs);
            context.query(`INSERT INTO ${tblnm} ( ${context.fieldnms} ) VALUES ( ${context.qs} )`, d,
              err => {
                if (err) { console.error(err); reject(err); }
                else  resolve(context); 
              });
          } else { console.log(`empty row ${util.inspect(datum)} ${util.inspect(d)}`); next(); }
  
  
  
        });
      })
      .then(context => {
        console.log('test22222222222');
        context.end();
        console.log(req.session.usrData);
        let sql = "CALL " + config.dbname + "." + procedure + "(" + created_by + "," + req.session.usrData.company_key_id + ",+@err_message,@b_result);SELECT @err_message as err_message,@b_result as b_result;";
        console.log("upload ");
        console.log(sql);
        async.waterfall([
          function(callback){
            database.connection.getConnection(function (err, connection) {
              if (err) {
                console.log(err);
                res.status(500).json({ "error": 1, "data": "Internal server error" });
              } else {
                connection.query(sql, function (err, rows) {
                  console.log(rows[0]);
                  console.log(rows[1])
                  if (err) {
                    console.log(err);
                    res.status(200).json({ "error": 1, "data": "Sql error" });
      
                  } else if (rows[1] !== undefined && rows[1][0]['b_result'] === 0) {
                    console.log("PPPPPPPPPPPP")
                    res.status(200).json({ "error": 1, "data": rows[1][0]['err_message'] });
                  }
                  else {
                    if (req.body.type === 'Stocks' && req.session.usrData.company_type_name != 'Birla Cellulose') {
                      console.log("other than birla ");
                      var sql = "SET @b_result = 0; CALL " + config.dbname + ".usp_update_0_day_stock_status(?,?,@b_result); SELECT @b_result";
                      connection.query(sql, [req.session.usrData.user_id, req.session.usrData.company_key_id], function (err, rows) {
                        if (err) {
                          console.log(err);
                          logger.error(req.session.usrData.email + Date() + JSON.stringify(err));
                          res.status(200).json({ "error": 1, "data": "Sql error" })
                        } else {
                          res.status(200).json({ "error": 0, "data": "Count updated successfully" })
                        }
                      });
                    }
                    else {
                    console.log(rows);
        
                    if(req.body.type === "Shipment" ||req.body.type=== 'shipment'){
                      res.json({"error":0,"data":"Shipment Uploaded"});  

                    }
                    else if(req.body.type === "Order"||req.body.type ==="order" ){
                      callback(null, rows);

                    }
                    
                    }
                  }
                });
                connection.release();
              }
            });
          },
          function(result, callback){
            debugger
            console.log(result)
            
            database.connection.getConnection(function(err, connection) {
              if (err) {
                  logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                  appData["error"] = 1;
                  appData["data"] = "Internal server error";
                  res.status(500).json(appData);
              } else {
                  var sql="CALL "+config.dbname+".usp_get_outgoing_orders (?,?,?,?,?,?,?,@total_recordcount);SELECT @total_recordcount as total_recordcount;";
                 // console.log(sql);
                  connection.query(sql,[req.session.usrData.company_key_id,'','','','',10,1],function(err, rows, fields) {
                      if (err) {
                          logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                          res.status(500).json({"error":1,"data":"Sql error"});
                      }else {
                          if(rows==="undefined" || rows[0]===undefined)
                          {
                              res.status(200).json({"error":1,"data":"No such record"});
                          }
                          else
                          {
                              console.log(rows[0][0].order_id)
                              callback(null, rows[0][0].order_id )
                              //res.status(200).json({"error":0,"data":rows[0],"total_recordcount":rows[rows.length-1][0]['total_recordcount']});
                          }
                      }
                      connection.release();
                  });
                  
              }
          });

          },
          function(result1, callback){
            console.log(result1);
            var order_id = result1
            var sql="CALL "+config.dbname+".usp_place_order("+order_id+","+req.session.usrData.company_key_id+","+req.session.usrData.user_id+")";
            var appData={};
            database.connection.getConnection(function(err, connection) {
              if (err) {
                  res.status(500).json({"error":1,"data":"Internal server error"});
              } else {
                  connection.query(sql, function(err, rows, fields) {
                      if (err) {
                          console.log(err);
                          logger.error(req.session.usrData.email+Date()+JSON.stringify(err));
                          res.json({"error":1,"data":"Sql error"});
                      }else {
                          if(rows.affectedRows===1)
                          {
                              res.json({"error":0,"data":"Order placed successfully"});  
                           /*   let data={};
                              data.user_action=true;
                              data.usrData=req.session.usrData;
                              data.company_key_id=req.session.usrData.company_key_id;
                              data.recv_company_key_id=(req.body.recv_company_key_id===undefined)?1:req.body.recv_company_key_id;
                              data.action="placed";
                              data.subject="Order placed : order number-"+req.body.order_number;
                              data.procedure=".usp_get_order_emailusers"
                              data.id=req.body.order_id;
                              data.order_number=req.body.order_number;
                              data.user_type="Suppliers";
                              emailController.create_mail(data,function(result){
                                  console.log('result',result);
                              })*/
                              /*console.log('data in cancel_ordera',data);
                              console.log('data1',data);*/
                          }
                          else
                          {
                              res.json({"error":1,"data":"No such record"});
                          }
                      }
                  });
                  connection.release();
              }
          });      






          }
        ])


  
      })
      .catch(err => {
        console.error(err.stack);
        res.status(200).json({ "error": 1, "data": "Error occurred" });
      });





                                                                /////////////////////////////////////////
                                        }
                                ], function (err, result) {
                               if(err)
                               res.json({"error":1,"data":err});
                               else
                               res.json(result);
                                });
        }
    });   
});





app.get('/public/audittrail',audit.make_audit,audit.audittrail);
app.get('/public/geo_data',audit.make_audit,audit.getshipmentid, audit.tracegeo);
app.get('/public/get_audit_display',audit.get_companyid , audit.public_audit_display);
/*app.use('/users',router);
app.use('/orders',auth.token_auth,orders);
app.use('/masters',auth.token_auth,masters);
app.use('/csv',csv);*/
app.use('/users',router);
app.use('/orders',orders);
app.use('/masters',masters);
app.use('/shipment',shipment);
app.use('/csv',csv);
app.use('/admin',admin);
app.use('/reports',reports);
app.listen(port,function(){
    console.log("Server is running on port: "+port);
});

//added this new code below to start server on https
//https.createServer(options, app).listen(port);
//console.log("Server is running on port: "+port);
